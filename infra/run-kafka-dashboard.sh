###############################################################################
#
# The following script creates a kafka dashboard container and connects it to the
# ${NETWORK}
#
###############################################################################
NETWORK="infra_wp4-network"
EXISTING=$(docker ps -a | grep kafdrop | cut -d' ' -f1)

if ! [ -z "$EXISTING" ] ; then
  echo "Dashboard $EXISTING is already running on port 9000..."
else
  CONTAINER=$(docker run --rm  -d -p 9000:9000  -e KAFKA_BROKERCONNECT=broker:9092   obsidiandynamics/kafdrop)
  docker network connect $NETWORK $CONTAINER
  echo "Dashboard is running on port 9000..."
fi

