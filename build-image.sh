#!/usr/bin/env bash

set -e
BUILD="-build"

if [ $1 = $BUILD ]; then
	mvn -f ./analytics/streamsight/pom.xml clean antlr4:antlr4 package -DskipTests -Dsyntax-validation=True
	mv ./analytics/streamsight/target/rainbow-analytics-syntax-validation.jar ./rainbow-analytic-enabler/rest-api/src/rainbow-analytics-syntax-validation.jar
	mvn -f ./analytics/streamsight/pom.xml clean antlr4:antlr4 package -DskipTests
	mv ./analytics/streamsight/target/rainbow-analytics.jar ./rainbow-analytic-enabler/rest-api/src/rainbow-analytics.jar
fi
# docker build . -t rainbow-analytic-enabler
docker build . -t registry.gitlab.com/rainbow-project1/rainbow-integration/rainbow-analytic-enabler:latest
