# -------------------------------------------#
# Author: Michailidou Anna-Valentini
# Description:Class implementing a hybrid
#	solution that finds the trade-off 
#	between latency and data quality check 
#	in a geo-distributed, heterogeneous and
#	edge-computing environment. 
# -------------------------------------------#
import random
import gurobipy as gp
from gurobipy import GRB
from random import choices
from LPOptimizer import LPOptimizer
from DAGCreator import DAGCreator
from SpringRelaxation import SpringRelaxation
from EqualAssignment import EqualAssignment
import sys
from FindMetrics import findMetricsFun
import math


class Equality():

	global numberOfDevices

	# ARG1:path to topology file
	# ARG2:number of devices
	dagFilePath = sys.argv[1]
	beta = 1  # For F formula
	penalty = 10  # Penalty
	qThreshold = 0.9
	numberOfDevices = int(sys.argv[2])

	# Create DAG
	dag = DAGCreator(dagFilePath)
	(paths, pairs, source, sink, children, parents, numberOfOperators, noOfSources, order,idToName) = dag.run()

	# Set constraints
	# Communication cost between devices (random)
	meancomCost = 0
	sumComCost = 0
	comCost = [[0 for i in range(numberOfDevices)] for j in range(numberOfDevices)]

	for i in range(numberOfDevices):
		for j in range(numberOfDevices):
			cost = round(random.uniform(0.1, 10), 2)  # Bounds
			if (i == j):
				comCost[i][j] = 0
			if (i < j and i != j):
				sumComCost = sumComCost + cost
				comCost[i][j] = cost
				comCost[j][i] = cost
	meancomCost = round(sumComCost / ((numberOfDevices * (numberOfDevices - 1)) / 2), 2)  # Used for penalty

	# Requirements of operators and DQ
	RCpu = []
	RMem = []
	for i in range(numberOfOperators):
		RCpu.append(round(random.uniform(5, 50), 2))
		RMem.append(random.randint(50, 500))
	RCPUDQ = round(random.uniform(5, 50), 2)
	RRAMDQ = random.randint(50, 500)

	# Characteristics of devices
	CCpu = []
	CMem = []
	UsedCpu = []
	UsedMem = []
	for i in range(numberOfDevices):
		CCpu.append(round(random.uniform(10, 500), 2))
		CMem.append(random.randint(100, 5000))
		UsedCpu.append(0)
		UsedMem.append(0)

	alpha = meancomCost / penalty  # Penalty

	# Availability of devices per operator
	available = []
	population = [1, 0]
	weights = [0.85, 0.15]  # 85% available, 15% not available
	for i in range(numberOfOperators):
		available.append([])
		if (i not in source):
			for j in range(numberOfDevices):
				available[i].append(choices(population, weights)[0])

	fractions = []
	communicationCost = []
	for n in range(numberOfOperators):
		fractions.append([])
		communicationCost.append(-1)

	# Equal distribution of sources to devices
	sourceDividedFraction = 1 / numberOfDevices
	for op in source:
		for dev in range(numberOfDevices):
			fractions[op].append(sourceDividedFraction)
			available[op].append(1)
	# UsedCpu[dev]=UsedCpu[dev]+fractions[op][dev]*RCpu[op]
	# UsedMem[dev]=UsedMem[dev]+fractions[op][dev]*RMem[op]

	# Spring relaxation
	sr = SpringRelaxation()
	(DQfractionSpring, totalTransferTimeSrping, FSpring, fractionsSpring) = sr.run(numberOfDevices, comCost,
																				   numberOfOperators, paths, pairs,
																				   source, children, parents, RCpu,
																				   RMem, CCpu, CMem, available,
																				   fractions, RCPUDQ, RRAMDQ,
																				   noOfSources, beta, alpha, order)

	# Εqual distribution
	eq = EqualAssignment()
	(DQfractionEQ, totalTransferTimeEQ, FEQ, UsedCpuEQ, UsedMemEQ, fractionsEQ, transferTimesEQ, slowestDevicesEQ,
	 DQfractionsEQ, slowestPathEQ) = eq.run(numberOfOperators, numberOfDevices, RCpu, RMem, CCpu, CMem, RCPUDQ, RRAMDQ,
											available, comCost, pairs, parents, paths, source, noOfSources, fractions,
											beta, alpha, order)

	# LP initial solution
	transferTimes = {}
	slowestDevices = {}
	for op in order:
		if (op not in source):  # For each non source
			modelN = gp.Model("lp")
			modelN.Params.OutputFlag = 0
			transferTime = modelN.addVar(vtype=GRB.CONTINUOUS, lb=0, name="transferTime")  # The objective
			fList = list(range(numberOfDevices))
			fraction = modelN.addVars(fList, lb=0, ub=100, vtype=GRB.INTEGER, name="x")
			binZerol = list(range(numberOfDevices))
			binSelfl = list(range(numberOfDevices))
			# 0 if the destination device will not hold data, 1 otherwise
			binZero = modelN.addVars(binZerol, lb=0, ub=1, vtype=GRB.INTEGER, name="bz")
			# 1 if the destination device is the same as the starting one, 0 otherwise
			binSelf = modelN.addVars(binSelfl, lb=0, ub=1, vtype=GRB.INTEGER, name="bs")

			c1 = 0
			for i in range(numberOfDevices):  # CPU, RAM constraints
				modelN.addConstr((fraction[i] * RCpu[op] / 100) <= (CCpu[i] - UsedCpu[i]))
				modelN.addConstr((fraction[i] * RMem[op] / 100) <= (CMem[i] - UsedMem[i]))
				c1 += fraction[i]  # Sum of fractions equal to 100
				if available[op][i] == 0:  # Availability constraint
					modelN.addConstr(fraction[i] == 0)
				modelN.addConstr(binSelf[i] >= fraction[i] / 100)
				modelN.addConstr(binSelf[i] <= fraction[i] / 100 + 0.99)
				modelN.addConstr(binZero[i] >= fraction[i] / 100)
				modelN.addConstr(binZero[i] <= fraction[i] / 100 + 0.99)
			modelN.addConstr(c1 == 100)

			for op1 in parents[op]:
				for dev1 in range(numberOfDevices):
					c2 = 0
					for dev2 in range(numberOfDevices):
						c2 = c2 + (fractions[op1][dev1] * pairs[op1, op] * comCost[dev1][dev2] * fraction[dev2]) / 100
						if (fractions[op1][dev1] != 0):  # If the device holds data
							# Penalty for each enabled link to other device
							c2 = c2 + alpha * (binZero.sum() - binSelf[dev1])
					modelN.addConstr(transferTime >= c2)  # Minimize the max
			modelN.setObjective(transferTime, GRB.MINIMIZE)

			modelN.optimize()  # Solve LP
			if (modelN.STATUS != GRB.OPTIMAL):
				solved = -1
			else:
				solved = 1
			if (solved == -1):
				break;
			# Enforce solution
			for i in range(numberOfDevices):
				fractions[op].append(round(fraction[i].x) / 100)
				UsedCpu[i] = UsedCpu[i] + (fraction[i].x * RCpu[op] / 100)
				UsedMem[i] = UsedMem[i] + (fraction[i].x * RMem[op] / 100)
	lpopt = LPOptimizer()

	if (solved != -1):  # If a solution was found by LP
		# Find metrics (latency, dq fraction, F)
		(F, totalTransferTime, DQfraction, DQfractions, usedCpu, usedMem, transferTimes, slowestDevices,
		 slowestPath) = findMetricsFun(numberOfDevices, comCost, paths, pairs, noOfSources, beta, alpha, available,
									   RCPUDQ, RRAMDQ,CCpu,CMem, source, fractions, UsedCpu, UsedMem)

		# Call optimizer and find best solution
		if (FEQ < F and FEQ != 0):  # If equal found a better solution than LP
			# Call optimizer using equal solution
			(DQfractionLat, totalTransferTimeLat, FLat, fractionsLat, DQfractionQual, totalTransferTimeQual, FQual,
			 fractionsQual) = lpopt.run(numberOfOperators, numberOfDevices, RCpu, RMem, CCpu, CMem, UsedCpuEQ,
										UsedMemEQ, RCPUDQ, RRAMDQ, available, comCost, pairs, parents
										, paths, source, noOfSources, fractionsEQ, transferTimesEQ, slowestDevicesEQ,
										DQfractionsEQ, DQfractionEQ, totalTransferTimeEQ, FEQ, beta, alpha,
										slowestPathEQ, qThreshold, order)
			# If Spring Relaxation found a better solution than qualOpt and latOpt the problem is solved by Spring Relaxation
			if ( FSpring < FLat and FSpring < FQual and FSpring != 0):
				solvedBy = "Spring"
				fractions=fractionsSpring
			else:  # Else the problem is solved by Equal assignment
				solvedBy = "Equal"
				fractions=fractionsEQ
				if (FLat <= FQual and FLat != 0):
					fractions = fractionsLat
				elif (FQual != 0):
					fractions = fractionsQual
		else:  # If LP is better than equal
			# Call optimizer using LP solution
			(DQfractionLat, totalTransferTimeLat, FLat, fractionsLat, DQfractionQual, totalTransferTimeQual, FQual,
			 fractionsQual) = lpopt.run(numberOfOperators, numberOfDevices, RCpu, RMem, CCpu, CMem, UsedCpu, UsedMem,
										RCPUDQ, RRAMDQ, available, comCost, pairs, parents
										, paths, source, noOfSources, fractions, transferTimes, slowestDevices,
										DQfractions, DQfraction, totalTransferTime, F, beta, alpha, slowestPath,
										qThreshold, order)
			# If Spring Relaxation found a better solution than qualOpt and latOpt the problem is solved by Spring Relaxation
			if (FSpring < FLat and FSpring < FQual and FSpring != 0):
				solvedBy = "Spring"
				fractions=fractionsSpring
			else:  # Else the problem is solved by LP
				solvedBy = "LP"
				if(FLat<=FQual and FLat!=0):
					fractions=fractionsLat
				elif(FQual!=0):
					fractions=fractionsQual



	else:  # Id LP found no solution
		if (FEQ != 0):  # If Equal found a solution
			# Call optimizer using equal solution
			(DQfractionLat, totalTransferTimeLat, FLat, fractionsLat, DQfractionQual, totalTransferTimeQual, FQual,
			 fractionsQual) = lpopt.run(numberOfOperators, numberOfDevices, RCpu, RMem, CCpu, CMem, UsedCpuEQ,
										UsedMemEQ, RCPUDQ, RRAMDQ, available, comCost, pairs, parents
										, paths, source, noOfSources, fractionsEQ, transferTimesEQ, slowestDevicesEQ,
										DQfractionsEQ, DQfractionEQ, totalTransferTimeEQ, FEQ, beta, alpha,
										slowestPathEQ, qThreshold, order)
			# If Spring Relaxation found a better solution than qualOpt and latOpt the problem is solved by Spring Relaxation
			if (FSpring < FLat and FSpring < FQual and FSpring != 0):
				solvedBy = "Spring"
				fractions=fractionsSpring
			else:  # Else the problem is solved by Equal assignment
				solvedBy = "Equal"
				fractions=fractionsEQ
		else:  # If Equal found no solution
			FLat = 0
			FQual = 0
			DQfractionLat = 0
			DQfractionQual = 0
			totalTransferTimeLat = 0
			totalTransferTimeQual = 0
			if (FSpring != 0):  # If Spring Relaxation found a solution the problem is solved by Spring Relaxation
				solvedBy = "Spring"
				fractions=fractionsSpring
			else:  # Else the problem is not solved
				solvedBy = "None"
				fractions=[]

		#F = 0
		#DQfraction = 0
		#totalTransferTime = 0
		#fractions=[]
		#fractions=[]

	operatorId = 0
	fractions = [[round(val, 2) for val in sublst] for sublst in fractions]
	for line in fractions:
		if(operatorId!=0):
			id
			f = open(idToName[operatorId] + ".txt", "w")
			f.write(', '.join(map(str, line)))
			f.close()
		operatorId += 1

