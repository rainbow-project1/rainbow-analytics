# -------------------------------------------#
# Author: Michailidou Anna-Valentini
# Description:Class that creates DAGs.
# -------------------------------------------#

from Graph import Graph
import csv
from collections import defaultdict
import re

class DAGCreator():
    def __init__(self, dagFilePath):
        self.dagFilePath = dagFilePath

    def run(self):
        paths = []
        pairs = {}
        graph = defaultdict(list)
        parents = []
        children = []
        noOfSources = 1
        nameToId = {}
        idToName={}

        id = 1
        with open(self.dagFilePath, 'r') as f:
            reader = csv.reader(f, dialect='excel', delimiter='\t')
            for row in reader:
                op1 = row[0]#.split("[")[0]
                op2 = row[1]#.split("[")[0]
                if (op1 not in nameToId.keys()):
                    nameToId[op1] = id
                    idToName[id]=re.sub("\*", 'ASTERISK', op1)
                    id += 1
                if (op2 not in nameToId.keys()):
                    nameToId[op2] = id
                    idToName[id] = re.sub("\*", 'ASTERISK', op2)
                    id += 1
        numberOfOperators = len(nameToId) + 1
        g = Graph(numberOfOperators, paths, pairs, graph)
        with open(self.dagFilePath, 'r') as f:
            reader = csv.reader(f, dialect='excel', delimiter='\t')
            for row in reader:
                op1 = row[0]#.split("[")[0]
                op2 = row[1]#.split("[")[0]
                g.addEdge(nameToId[op1], nameToId[op2], 1)

        source = [0]  # Source nodes
        sink = []


        for i in range(1, len(nameToId) + 1):
            isSource = True
            isSink = True
            for pair in pairs:
                if (pair[1] == i):
                    isSource = False
                if (pair[0] == i):
                    isSink = False
            if (isSource == True):
                g.addEdge(0, i, 1)
            if (isSink == True):
                sink.append(i)

        for i in range(numberOfOperators):
            (c, p) = g.findParentsAndChildren(i)
            children.append(c)
            parents.append(p)

        # Find all paths from source to sink
        for s in source:
            for d in sink:
                paths = g.printAllPaths(s, d)
        order = g.topologicalOrder(source, paths)
        return (paths, pairs, source, sink, children, parents, numberOfOperators, noOfSources, order,idToName)
