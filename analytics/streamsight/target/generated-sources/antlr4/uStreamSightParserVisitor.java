// Generated from uStreamSightParser.g4 by ANTLR 4.7.1

    package eu.rainbowh2020;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link uStreamSightParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface uStreamSightParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Visit a parse tree produced by the {@code streamDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamDefinitionExpr(uStreamSightParser.StreamDefinitionExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code insightDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsightDefinitionExpr(uStreamSightParser.InsightDefinitionExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#streamId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamId(uStreamSightParser.StreamIdContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(uStreamSightParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#insightDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsightDefinition(uStreamSightParser.InsightDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#insightId}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInsightId(uStreamSightParser.InsightIdContext ctx);
	/**
	 * Visit a parse tree produced by the {@code interExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterExpr(uStreamSightParser.InterExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code filterExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilterExpr(uStreamSightParser.FilterExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code groupExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupExpr(uStreamSightParser.GroupExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code basecaseExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBasecaseExpr(uStreamSightParser.BasecaseExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#intervalComposition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntervalComposition(uStreamSightParser.IntervalCompositionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#filteredComposition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFilteredComposition(uStreamSightParser.FilteredCompositionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#groupingComposition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupingComposition(uStreamSightParser.GroupingCompositionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOpExpr(uStreamSightParser.OpExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code mulopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulopExpr(uStreamSightParser.MulopExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code DOUBLE}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDOUBLE(uStreamSightParser.DOUBLEContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddopExpr(uStreamSightParser.AddopExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code aggregateExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAggregateExpr(uStreamSightParser.AggregateExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code metricStreamExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMetricStreamExpr(uStreamSightParser.MetricStreamExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code cumulativeExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCumulativeExpr(uStreamSightParser.CumulativeExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#metric}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMetric(uStreamSightParser.MetricContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#membership}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMembership(uStreamSightParser.MembershipContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#member}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMember(uStreamSightParser.MemberContext ctx);
	/**
	 * Visit a parse tree produced by the {@code windowedAggrExpr}
	 * labeled alternative in {@link uStreamSightParser#aggregate}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowedAggrExpr(uStreamSightParser.WindowedAggrExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#cumulative}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCumulative(uStreamSightParser.CumulativeContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#metricStream}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMetricStream(uStreamSightParser.MetricStreamContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(uStreamSightParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#windowedFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindowedFunction(uStreamSightParser.WindowedFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#window}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWindow(uStreamSightParser.WindowContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#interval}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInterval(uStreamSightParser.IntervalContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#grouping}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGrouping(uStreamSightParser.GroupingContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#timeperiod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTimeperiod(uStreamSightParser.TimeperiodContext ctx);
}