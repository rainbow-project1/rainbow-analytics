// Generated from uStreamSightParser.g4 by ANTLR 4.7.1

    package eu.rainbowh2020;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link uStreamSightParser}.
 */
public interface uStreamSightParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code streamDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStreamDefinitionExpr(uStreamSightParser.StreamDefinitionExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code streamDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStreamDefinitionExpr(uStreamSightParser.StreamDefinitionExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code insightDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterInsightDefinitionExpr(uStreamSightParser.InsightDefinitionExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code insightDefinitionExpr}
	 * labeled alternative in {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitInsightDefinitionExpr(uStreamSightParser.InsightDefinitionExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 */
	void enterStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 */
	void exitStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#streamId}.
	 * @param ctx the parse tree
	 */
	void enterStreamId(uStreamSightParser.StreamIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#streamId}.
	 * @param ctx the parse tree
	 */
	void exitStreamId(uStreamSightParser.StreamIdContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 */
	void enterStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 */
	void exitStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 */
	void enterKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 */
	void exitKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 */
	void enterKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 */
	void exitKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(uStreamSightParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(uStreamSightParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#insightDefinition}.
	 * @param ctx the parse tree
	 */
	void enterInsightDefinition(uStreamSightParser.InsightDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#insightDefinition}.
	 * @param ctx the parse tree
	 */
	void exitInsightDefinition(uStreamSightParser.InsightDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#insightId}.
	 * @param ctx the parse tree
	 */
	void enterInsightId(uStreamSightParser.InsightIdContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#insightId}.
	 * @param ctx the parse tree
	 */
	void exitInsightId(uStreamSightParser.InsightIdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code interExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void enterInterExpr(uStreamSightParser.InterExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code interExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void exitInterExpr(uStreamSightParser.InterExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code filterExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void enterFilterExpr(uStreamSightParser.FilterExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code filterExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void exitFilterExpr(uStreamSightParser.FilterExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code groupExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void enterGroupExpr(uStreamSightParser.GroupExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code groupExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void exitGroupExpr(uStreamSightParser.GroupExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code basecaseExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void enterBasecaseExpr(uStreamSightParser.BasecaseExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code basecaseExpr}
	 * labeled alternative in {@link uStreamSightParser#composition}.
	 * @param ctx the parse tree
	 */
	void exitBasecaseExpr(uStreamSightParser.BasecaseExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#intervalComposition}.
	 * @param ctx the parse tree
	 */
	void enterIntervalComposition(uStreamSightParser.IntervalCompositionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#intervalComposition}.
	 * @param ctx the parse tree
	 */
	void exitIntervalComposition(uStreamSightParser.IntervalCompositionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#filteredComposition}.
	 * @param ctx the parse tree
	 */
	void enterFilteredComposition(uStreamSightParser.FilteredCompositionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#filteredComposition}.
	 * @param ctx the parse tree
	 */
	void exitFilteredComposition(uStreamSightParser.FilteredCompositionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#groupingComposition}.
	 * @param ctx the parse tree
	 */
	void enterGroupingComposition(uStreamSightParser.GroupingCompositionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#groupingComposition}.
	 * @param ctx the parse tree
	 */
	void exitGroupingComposition(uStreamSightParser.GroupingCompositionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOpExpr(uStreamSightParser.OpExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code opExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOpExpr(uStreamSightParser.OpExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mulopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMulopExpr(uStreamSightParser.MulopExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mulopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMulopExpr(uStreamSightParser.MulopExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code DOUBLE}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDOUBLE(uStreamSightParser.DOUBLEContext ctx);
	/**
	 * Exit a parse tree produced by the {@code DOUBLE}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDOUBLE(uStreamSightParser.DOUBLEContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddopExpr(uStreamSightParser.AddopExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addopExpr}
	 * labeled alternative in {@link uStreamSightParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddopExpr(uStreamSightParser.AddopExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code aggregateExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterAggregateExpr(uStreamSightParser.AggregateExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code aggregateExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitAggregateExpr(uStreamSightParser.AggregateExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code metricStreamExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterMetricStreamExpr(uStreamSightParser.MetricStreamExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code metricStreamExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitMetricStreamExpr(uStreamSightParser.MetricStreamExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code cumulativeExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterCumulativeExpr(uStreamSightParser.CumulativeExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code cumulativeExpr}
	 * labeled alternative in {@link uStreamSightParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitCumulativeExpr(uStreamSightParser.CumulativeExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#metric}.
	 * @param ctx the parse tree
	 */
	void enterMetric(uStreamSightParser.MetricContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#metric}.
	 * @param ctx the parse tree
	 */
	void exitMetric(uStreamSightParser.MetricContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#membership}.
	 * @param ctx the parse tree
	 */
	void enterMembership(uStreamSightParser.MembershipContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#membership}.
	 * @param ctx the parse tree
	 */
	void exitMembership(uStreamSightParser.MembershipContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#member}.
	 * @param ctx the parse tree
	 */
	void enterMember(uStreamSightParser.MemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#member}.
	 * @param ctx the parse tree
	 */
	void exitMember(uStreamSightParser.MemberContext ctx);
	/**
	 * Enter a parse tree produced by the {@code windowedAggrExpr}
	 * labeled alternative in {@link uStreamSightParser#aggregate}.
	 * @param ctx the parse tree
	 */
	void enterWindowedAggrExpr(uStreamSightParser.WindowedAggrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code windowedAggrExpr}
	 * labeled alternative in {@link uStreamSightParser#aggregate}.
	 * @param ctx the parse tree
	 */
	void exitWindowedAggrExpr(uStreamSightParser.WindowedAggrExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#cumulative}.
	 * @param ctx the parse tree
	 */
	void enterCumulative(uStreamSightParser.CumulativeContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#cumulative}.
	 * @param ctx the parse tree
	 */
	void exitCumulative(uStreamSightParser.CumulativeContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#metricStream}.
	 * @param ctx the parse tree
	 */
	void enterMetricStream(uStreamSightParser.MetricStreamContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#metricStream}.
	 * @param ctx the parse tree
	 */
	void exitMetricStream(uStreamSightParser.MetricStreamContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(uStreamSightParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(uStreamSightParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#windowedFunction}.
	 * @param ctx the parse tree
	 */
	void enterWindowedFunction(uStreamSightParser.WindowedFunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#windowedFunction}.
	 * @param ctx the parse tree
	 */
	void exitWindowedFunction(uStreamSightParser.WindowedFunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#window}.
	 * @param ctx the parse tree
	 */
	void enterWindow(uStreamSightParser.WindowContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#window}.
	 * @param ctx the parse tree
	 */
	void exitWindow(uStreamSightParser.WindowContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#interval}.
	 * @param ctx the parse tree
	 */
	void enterInterval(uStreamSightParser.IntervalContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#interval}.
	 * @param ctx the parse tree
	 */
	void exitInterval(uStreamSightParser.IntervalContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#grouping}.
	 * @param ctx the parse tree
	 */
	void enterGrouping(uStreamSightParser.GroupingContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#grouping}.
	 * @param ctx the parse tree
	 */
	void exitGrouping(uStreamSightParser.GroupingContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#timeperiod}.
	 * @param ctx the parse tree
	 */
	void enterTimeperiod(uStreamSightParser.TimeperiodContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#timeperiod}.
	 * @param ctx the parse tree
	 */
	void exitTimeperiod(uStreamSightParser.TimeperiodContext ctx);
}