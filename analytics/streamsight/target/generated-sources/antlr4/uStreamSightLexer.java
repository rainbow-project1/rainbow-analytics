// Generated from uStreamSightLexer.g4 by ANTLR 4.7.1

    package eu.rainbowh2020;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class uStreamSightLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMPUTE=1, STREAM=2, FROM=3, BY=4, EVERY=5, WHEN=6, TIME_PERIOD=7, ID=8, 
		STREAM_PROVIDER=9, SEMI_COLON=10, COLON=11, EQUAL=12, LPAR=13, RPAR=14, 
		LBR=15, RBR=16, COMMA=17, ADDOP=18, MULOP=19, OPERATOR=20, STRING=21, 
		INTEGER=22, FLOAT=23, WS=24, BlockComment=25, LineComment=26;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"COMPUTE", "STREAM", "FROM", "BY", "EVERY", "WHEN", "TIME_PERIOD", "ID", 
		"STREAM_PROVIDER", "SEMI_COLON", "COLON", "EQUAL", "LPAR", "RPAR", "LBR", 
		"RBR", "COMMA", "ADDOP", "MULOP", "OPERATOR", "STRING", "INTEGER", "FLOAT", 
		"WS", "BlockComment", "LineComment"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "';'", "':'", 
		"'='", "'('", "')'", "'['", "']'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMPUTE", "STREAM", "FROM", "BY", "EVERY", "WHEN", "TIME_PERIOD", 
		"ID", "STREAM_PROVIDER", "SEMI_COLON", "COLON", "EQUAL", "LPAR", "RPAR", 
		"LBR", "RBR", "COMMA", "ADDOP", "MULOP", "OPERATOR", "STRING", "INTEGER", 
		"FLOAT", "WS", "BlockComment", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public uStreamSightLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "uStreamSightLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 24:
			BlockComment_action((RuleContext)_localctx, actionIndex);
			break;
		case 25:
			LineComment_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void BlockComment_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			 // System.out.println("BC> " + getText());
			        skip();
			break;
		}
	}
	private void LineComment_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			 //System.out.println("LC> " + getText());
			        skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\34\u0106\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\2\5\2F\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\5\3T\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4^\n\4\3\5\3\5\3\5\3\5\5\5"+
		"d\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6p\n\6\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\5\7z\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\5\b\u009a\n\b\3\t\6\t\u009d\n\t\r\t\16\t\u009e\3\t\7\t\u00a2\n"+
		"\t\f\t\16\t\u00a5\13\t\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17"+
		"\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00c6\n\25\3\26\3\26\7\26\u00ca\n"+
		"\26\f\26\16\26\u00cd\13\26\3\26\3\26\3\27\5\27\u00d2\n\27\3\27\6\27\u00d5"+
		"\n\27\r\27\16\27\u00d6\3\30\5\30\u00da\n\30\3\30\6\30\u00dd\n\30\r\30"+
		"\16\30\u00de\3\30\3\30\6\30\u00e3\n\30\r\30\16\30\u00e4\5\30\u00e7\n\30"+
		"\3\31\6\31\u00ea\n\31\r\31\16\31\u00eb\3\31\3\31\3\32\3\32\3\32\3\32\7"+
		"\32\u00f4\n\32\f\32\16\32\u00f7\13\32\3\32\3\32\3\32\3\32\3\32\3\33\3"+
		"\33\7\33\u0100\n\33\f\33\16\33\u0103\13\33\3\33\3\33\4\u00cb\u00f5\2\34"+
		"\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20"+
		"\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\3\2\n\5\2C\\"+
		"aac|\6\2\62;C\\aac|\4\2--//\5\2\'\',,\61\61\4\2>>@@\3\2\62;\5\2\13\f\16"+
		"\17\"\"\4\2\f\f\17\17\2\u0122\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2"+
		"+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2"+
		"\3E\3\2\2\2\5S\3\2\2\2\7]\3\2\2\2\tc\3\2\2\2\13o\3\2\2\2\ry\3\2\2\2\17"+
		"\u0099\3\2\2\2\21\u009c\3\2\2\2\23\u00a6\3\2\2\2\25\u00a8\3\2\2\2\27\u00aa"+
		"\3\2\2\2\31\u00ac\3\2\2\2\33\u00ae\3\2\2\2\35\u00b0\3\2\2\2\37\u00b2\3"+
		"\2\2\2!\u00b4\3\2\2\2#\u00b6\3\2\2\2%\u00b8\3\2\2\2\'\u00ba\3\2\2\2)\u00c5"+
		"\3\2\2\2+\u00c7\3\2\2\2-\u00d1\3\2\2\2/\u00d9\3\2\2\2\61\u00e9\3\2\2\2"+
		"\63\u00ef\3\2\2\2\65\u00fd\3\2\2\2\678\7E\2\289\7Q\2\29:\7O\2\2:;\7R\2"+
		"\2;<\7W\2\2<=\7V\2\2=F\7G\2\2>?\7e\2\2?@\7q\2\2@A\7o\2\2AB\7r\2\2BC\7"+
		"w\2\2CD\7v\2\2DF\7g\2\2E\67\3\2\2\2E>\3\2\2\2F\4\3\2\2\2GH\7U\2\2HI\7"+
		"V\2\2IJ\7T\2\2JK\7G\2\2KL\7C\2\2LT\7O\2\2MN\7u\2\2NO\7v\2\2OP\7t\2\2P"+
		"Q\7g\2\2QR\7c\2\2RT\7o\2\2SG\3\2\2\2SM\3\2\2\2T\6\3\2\2\2UV\7H\2\2VW\7"+
		"T\2\2WX\7Q\2\2X^\7O\2\2YZ\7h\2\2Z[\7t\2\2[\\\7q\2\2\\^\7o\2\2]U\3\2\2"+
		"\2]Y\3\2\2\2^\b\3\2\2\2_`\7D\2\2`d\7[\2\2ab\7d\2\2bd\7{\2\2c_\3\2\2\2"+
		"ca\3\2\2\2d\n\3\2\2\2ef\7G\2\2fg\7X\2\2gh\7G\2\2hi\7T\2\2ip\7[\2\2jk\7"+
		"g\2\2kl\7x\2\2lm\7g\2\2mn\7t\2\2np\7{\2\2oe\3\2\2\2oj\3\2\2\2p\f\3\2\2"+
		"\2qr\7Y\2\2rs\7J\2\2st\7G\2\2tz\7P\2\2uv\7y\2\2vw\7j\2\2wx\7g\2\2xz\7"+
		"p\2\2yq\3\2\2\2yu\3\2\2\2z\16\3\2\2\2{|\7O\2\2|}\7K\2\2}~\7N\2\2~\177"+
		"\7N\2\2\177\u0080\7K\2\2\u0080\u009a\7U\2\2\u0081\u0082\7o\2\2\u0082\u009a"+
		"\7u\2\2\u0083\u0084\7U\2\2\u0084\u0085\7G\2\2\u0085\u0086\7E\2\2\u0086"+
		"\u0087\7Q\2\2\u0087\u0088\7P\2\2\u0088\u0089\7F\2\2\u0089\u009a\7U\2\2"+
		"\u008a\u009a\7u\2\2\u008b\u008c\7O\2\2\u008c\u008d\7K\2\2\u008d\u008e"+
		"\7P\2\2\u008e\u008f\7W\2\2\u008f\u0090\7V\2\2\u0090\u0091\7G\2\2\u0091"+
		"\u009a\7U\2\2\u0092\u009a\7o\2\2\u0093\u0094\7J\2\2\u0094\u0095\7Q\2\2"+
		"\u0095\u0096\7W\2\2\u0096\u0097\7T\2\2\u0097\u009a\7U\2\2\u0098\u009a"+
		"\7j\2\2\u0099{\3\2\2\2\u0099\u0081\3\2\2\2\u0099\u0083\3\2\2\2\u0099\u008a"+
		"\3\2\2\2\u0099\u008b\3\2\2\2\u0099\u0092\3\2\2\2\u0099\u0093\3\2\2\2\u0099"+
		"\u0098\3\2\2\2\u009a\20\3\2\2\2\u009b\u009d\t\2\2\2\u009c\u009b\3\2\2"+
		"\2\u009d\u009e\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u00a3"+
		"\3\2\2\2\u00a0\u00a2\t\3\2\2\u00a1\u00a0\3\2\2\2\u00a2\u00a5\3\2\2\2\u00a3"+
		"\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\22\3\2\2\2\u00a5\u00a3\3\2\2"+
		"\2\u00a6\u00a7\5\21\t\2\u00a7\24\3\2\2\2\u00a8\u00a9\7=\2\2\u00a9\26\3"+
		"\2\2\2\u00aa\u00ab\7<\2\2\u00ab\30\3\2\2\2\u00ac\u00ad\7?\2\2\u00ad\32"+
		"\3\2\2\2\u00ae\u00af\7*\2\2\u00af\34\3\2\2\2\u00b0\u00b1\7+\2\2\u00b1"+
		"\36\3\2\2\2\u00b2\u00b3\7]\2\2\u00b3 \3\2\2\2\u00b4\u00b5\7_\2\2\u00b5"+
		"\"\3\2\2\2\u00b6\u00b7\7.\2\2\u00b7$\3\2\2\2\u00b8\u00b9\t\4\2\2\u00b9"+
		"&\3\2\2\2\u00ba\u00bb\t\5\2\2\u00bb(\3\2\2\2\u00bc\u00c6\t\6\2\2\u00bd"+
		"\u00be\7?\2\2\u00be\u00c6\7?\2\2\u00bf\u00c0\7#\2\2\u00c0\u00c6\7?\2\2"+
		"\u00c1\u00c2\7@\2\2\u00c2\u00c6\7?\2\2\u00c3\u00c4\7>\2\2\u00c4\u00c6"+
		"\7?\2\2\u00c5\u00bc\3\2\2\2\u00c5\u00bd\3\2\2\2\u00c5\u00bf\3\2\2\2\u00c5"+
		"\u00c1\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6*\3\2\2\2\u00c7\u00cb\7$\2\2\u00c8"+
		"\u00ca\13\2\2\2\u00c9\u00c8\3\2\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00cc\3"+
		"\2\2\2\u00cb\u00c9\3\2\2\2\u00cc\u00ce\3\2\2\2\u00cd\u00cb\3\2\2\2\u00ce"+
		"\u00cf\7$\2\2\u00cf,\3\2\2\2\u00d0\u00d2\7/\2\2\u00d1\u00d0\3\2\2\2\u00d1"+
		"\u00d2\3\2\2\2\u00d2\u00d4\3\2\2\2\u00d3\u00d5\t\7\2\2\u00d4\u00d3\3\2"+
		"\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7"+
		".\3\2\2\2\u00d8\u00da\t\4\2\2\u00d9\u00d8\3\2\2\2\u00d9\u00da\3\2\2\2"+
		"\u00da\u00dc\3\2\2\2\u00db\u00dd\t\7\2\2\u00dc\u00db\3\2\2\2\u00dd\u00de"+
		"\3\2\2\2\u00de\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df\u00e6\3\2\2\2\u00e0"+
		"\u00e2\7\60\2\2\u00e1\u00e3\t\7\2\2\u00e2\u00e1\3\2\2\2\u00e3\u00e4\3"+
		"\2\2\2\u00e4\u00e2\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00e7\3\2\2\2\u00e6"+
		"\u00e0\3\2\2\2\u00e6\u00e7\3\2\2\2\u00e7\60\3\2\2\2\u00e8\u00ea\t\b\2"+
		"\2\u00e9\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00e9\3\2\2\2\u00eb\u00ec"+
		"\3\2\2\2\u00ec\u00ed\3\2\2\2\u00ed\u00ee\b\31\2\2\u00ee\62\3\2\2\2\u00ef"+
		"\u00f0\7\61\2\2\u00f0\u00f1\7,\2\2\u00f1\u00f5\3\2\2\2\u00f2\u00f4\13"+
		"\2\2\2\u00f3\u00f2\3\2\2\2\u00f4\u00f7\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f5"+
		"\u00f3\3\2\2\2\u00f6\u00f8\3\2\2\2\u00f7\u00f5\3\2\2\2\u00f8\u00f9\7,"+
		"\2\2\u00f9\u00fa\7\61\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fc\b\32\3\2\u00fc"+
		"\64\3\2\2\2\u00fd\u0101\7%\2\2\u00fe\u0100\n\t\2\2\u00ff\u00fe\3\2\2\2"+
		"\u0100\u0103\3\2\2\2\u0101\u00ff\3\2\2\2\u0101\u0102\3\2\2\2\u0102\u0104"+
		"\3\2\2\2\u0103\u0101\3\2\2\2\u0104\u0105\b\33\4\2\u0105\66\3\2\2\2\27"+
		"\2ES]coy\u0099\u009e\u00a3\u00c5\u00cb\u00d1\u00d6\u00d9\u00de\u00e4\u00e6"+
		"\u00eb\u00f5\u0101\5\b\2\2\3\32\2\3\33\3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}