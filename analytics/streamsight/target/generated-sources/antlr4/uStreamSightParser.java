// Generated from uStreamSightParser.g4 by ANTLR 4.7.1

    package eu.rainbowh2020;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class uStreamSightParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMPUTE=1, STREAM=2, FROM=3, BY=4, EVERY=5, WHEN=6, TIME_PERIOD=7, ID=8, 
		STREAM_PROVIDER=9, SEMI_COLON=10, COLON=11, EQUAL=12, LPAR=13, RPAR=14, 
		LBR=15, RBR=16, COMMA=17, ADDOP=18, MULOP=19, OPERATOR=20, STRING=21, 
		INTEGER=22, FLOAT=23, WS=24, BlockComment=25, LineComment=26;
	public static final int
		RULE_statements = 0, RULE_statement = 1, RULE_streamDefinition = 2, RULE_streamId = 3, 
		RULE_streamProvider = 4, RULE_keyValueParams = 5, RULE_keyValuePair = 6, 
		RULE_value = 7, RULE_insightDefinition = 8, RULE_insightId = 9, RULE_composition = 10, 
		RULE_intervalComposition = 11, RULE_filteredComposition = 12, RULE_groupingComposition = 13, 
		RULE_expression = 14, RULE_operation = 15, RULE_metric = 16, RULE_membership = 17, 
		RULE_member = 18, RULE_aggregate = 19, RULE_cumulative = 20, RULE_metricStream = 21, 
		RULE_function = 22, RULE_windowedFunction = 23, RULE_window = 24, RULE_interval = 25, 
		RULE_grouping = 26, RULE_timeperiod = 27;
	public static final String[] ruleNames = {
		"statements", "statement", "streamDefinition", "streamId", "streamProvider", 
		"keyValueParams", "keyValuePair", "value", "insightDefinition", "insightId", 
		"composition", "intervalComposition", "filteredComposition", "groupingComposition", 
		"expression", "operation", "metric", "membership", "member", "aggregate", 
		"cumulative", "metricStream", "function", "windowedFunction", "window", 
		"interval", "grouping", "timeperiod"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, null, null, null, null, null, null, "';'", "':'", 
		"'='", "'('", "')'", "'['", "']'", "','"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "COMPUTE", "STREAM", "FROM", "BY", "EVERY", "WHEN", "TIME_PERIOD", 
		"ID", "STREAM_PROVIDER", "SEMI_COLON", "COLON", "EQUAL", "LPAR", "RPAR", 
		"LBR", "RBR", "COMMA", "ADDOP", "MULOP", "OPERATOR", "STRING", "INTEGER", 
		"FLOAT", "WS", "BlockComment", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "uStreamSightParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public uStreamSightParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StatementsContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode EOF() { return getToken(uStreamSightParser.EOF, 0); }
		public List<TerminalNode> SEMI_COLON() { return getTokens(uStreamSightParser.SEMI_COLON); }
		public TerminalNode SEMI_COLON(int i) {
			return getToken(uStreamSightParser.SEMI_COLON, i);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(56);
			statement();
			setState(61);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(57);
					match(SEMI_COLON);
					setState(58);
					statement();
					}
					} 
				}
				setState(63);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(65);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI_COLON) {
				{
				setState(64);
				match(SEMI_COLON);
				}
			}

			setState(67);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class StreamDefinitionExprContext extends StatementContext {
		public StreamDefinitionContext streamDefinition() {
			return getRuleContext(StreamDefinitionContext.class,0);
		}
		public StreamDefinitionExprContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamDefinitionExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamDefinitionExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamDefinitionExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InsightDefinitionExprContext extends StatementContext {
		public InsightDefinitionContext insightDefinition() {
			return getRuleContext(InsightDefinitionContext.class,0);
		}
		public InsightDefinitionExprContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterInsightDefinitionExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitInsightDefinitionExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitInsightDefinitionExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			setState(71);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				_localctx = new StreamDefinitionExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(69);
				streamDefinition();
				}
				break;
			case 2:
				_localctx = new InsightDefinitionExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(70);
				insightDefinition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StreamDefinitionContext extends ParserRuleContext {
		public StreamIdContext streamId() {
			return getRuleContext(StreamIdContext.class,0);
		}
		public TerminalNode COLON() { return getToken(uStreamSightParser.COLON, 0); }
		public TerminalNode STREAM() { return getToken(uStreamSightParser.STREAM, 0); }
		public TerminalNode FROM() { return getToken(uStreamSightParser.FROM, 0); }
		public StreamProviderContext streamProvider() {
			return getRuleContext(StreamProviderContext.class,0);
		}
		public StreamDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_streamDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StreamDefinitionContext streamDefinition() throws RecognitionException {
		StreamDefinitionContext _localctx = new StreamDefinitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_streamDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			streamId();
			setState(74);
			match(COLON);
			setState(75);
			match(STREAM);
			setState(76);
			match(FROM);
			setState(77);
			streamProvider();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StreamIdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public StreamIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_streamId; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StreamIdContext streamId() throws RecognitionException {
		StreamIdContext _localctx = new StreamIdContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_streamId);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(79);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StreamProviderContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public TerminalNode LPAR() { return getToken(uStreamSightParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(uStreamSightParser.RPAR, 0); }
		public KeyValueParamsContext keyValueParams() {
			return getRuleContext(KeyValueParamsContext.class,0);
		}
		public StreamProviderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_streamProvider; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamProvider(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamProvider(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamProvider(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StreamProviderContext streamProvider() throws RecognitionException {
		StreamProviderContext _localctx = new StreamProviderContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_streamProvider);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(81);
			match(ID);
			setState(82);
			match(LPAR);
			setState(84);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(83);
				keyValueParams();
				}
			}

			setState(86);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyValueParamsContext extends ParserRuleContext {
		public List<KeyValuePairContext> keyValuePair() {
			return getRuleContexts(KeyValuePairContext.class);
		}
		public KeyValuePairContext keyValuePair(int i) {
			return getRuleContext(KeyValuePairContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(uStreamSightParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(uStreamSightParser.COMMA, i);
		}
		public KeyValueParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyValueParams; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterKeyValueParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitKeyValueParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitKeyValueParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyValueParamsContext keyValueParams() throws RecognitionException {
		KeyValueParamsContext _localctx = new KeyValueParamsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_keyValueParams);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88);
			keyValuePair();
			setState(93);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(89);
				match(COMMA);
				setState(90);
				keyValuePair();
				}
				}
				setState(95);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyValuePairContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(uStreamSightParser.EQUAL, 0); }
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public KeyValuePairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyValuePair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterKeyValuePair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitKeyValuePair(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitKeyValuePair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyValuePairContext keyValuePair() throws RecognitionException {
		KeyValuePairContext _localctx = new KeyValuePairContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_keyValuePair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			match(ID);
			setState(97);
			match(EQUAL);
			setState(98);
			value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(uStreamSightParser.STRING, 0); }
		public TerminalNode INTEGER() { return getToken(uStreamSightParser.INTEGER, 0); }
		public TerminalNode FLOAT() { return getToken(uStreamSightParser.FLOAT, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(100);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STRING) | (1L << INTEGER) | (1L << FLOAT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsightDefinitionContext extends ParserRuleContext {
		public InsightIdContext insightId() {
			return getRuleContext(InsightIdContext.class,0);
		}
		public TerminalNode EQUAL() { return getToken(uStreamSightParser.EQUAL, 0); }
		public TerminalNode COMPUTE() { return getToken(uStreamSightParser.COMPUTE, 0); }
		public CompositionContext composition() {
			return getRuleContext(CompositionContext.class,0);
		}
		public InsightDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insightDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterInsightDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitInsightDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitInsightDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsightDefinitionContext insightDefinition() throws RecognitionException {
		InsightDefinitionContext _localctx = new InsightDefinitionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_insightDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			insightId();
			setState(103);
			match(EQUAL);
			setState(104);
			match(COMPUTE);
			setState(105);
			composition();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsightIdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public InsightIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insightId; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterInsightId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitInsightId(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitInsightId(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InsightIdContext insightId() throws RecognitionException {
		InsightIdContext _localctx = new InsightIdContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_insightId);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(107);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompositionContext extends ParserRuleContext {
		public CompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_composition; }
	 
		public CompositionContext() { }
		public void copyFrom(CompositionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class GroupExprContext extends CompositionContext {
		public GroupingCompositionContext groupingComposition() {
			return getRuleContext(GroupingCompositionContext.class,0);
		}
		public GroupExprContext(CompositionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterGroupExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitGroupExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitGroupExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BasecaseExprContext extends CompositionContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BasecaseExprContext(CompositionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterBasecaseExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitBasecaseExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitBasecaseExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class InterExprContext extends CompositionContext {
		public IntervalCompositionContext intervalComposition() {
			return getRuleContext(IntervalCompositionContext.class,0);
		}
		public InterExprContext(CompositionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterInterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitInterExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitInterExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FilterExprContext extends CompositionContext {
		public FilteredCompositionContext filteredComposition() {
			return getRuleContext(FilteredCompositionContext.class,0);
		}
		public FilterExprContext(CompositionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterFilterExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitFilterExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitFilterExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompositionContext composition() throws RecognitionException {
		CompositionContext _localctx = new CompositionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_composition);
		try {
			setState(113);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				_localctx = new InterExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(109);
				intervalComposition();
				}
				break;
			case 2:
				_localctx = new FilterExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(110);
				filteredComposition();
				}
				break;
			case 3:
				_localctx = new GroupExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(111);
				groupingComposition();
				}
				break;
			case 4:
				_localctx = new BasecaseExprContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(112);
				expression(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalCompositionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode EVERY() { return getToken(uStreamSightParser.EVERY, 0); }
		public IntervalContext interval() {
			return getRuleContext(IntervalContext.class,0);
		}
		public IntervalCompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intervalComposition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterIntervalComposition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitIntervalComposition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitIntervalComposition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalCompositionContext intervalComposition() throws RecognitionException {
		IntervalCompositionContext _localctx = new IntervalCompositionContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_intervalComposition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			expression(0);
			setState(116);
			match(EVERY);
			setState(117);
			interval();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilteredCompositionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode WHEN() { return getToken(uStreamSightParser.WHEN, 0); }
		public TerminalNode OPERATOR() { return getToken(uStreamSightParser.OPERATOR, 0); }
		public FilteredCompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filteredComposition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterFilteredComposition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitFilteredComposition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitFilteredComposition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FilteredCompositionContext filteredComposition() throws RecognitionException {
		FilteredCompositionContext _localctx = new FilteredCompositionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_filteredComposition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(119);
			expression(0);
			setState(120);
			match(WHEN);
			setState(121);
			match(OPERATOR);
			setState(122);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupingCompositionContext extends ParserRuleContext {
		public AggregateContext aggregate() {
			return getRuleContext(AggregateContext.class,0);
		}
		public TerminalNode BY() { return getToken(uStreamSightParser.BY, 0); }
		public GroupingContext grouping() {
			return getRuleContext(GroupingContext.class,0);
		}
		public TerminalNode EVERY() { return getToken(uStreamSightParser.EVERY, 0); }
		public IntervalContext interval() {
			return getRuleContext(IntervalContext.class,0);
		}
		public GroupingCompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupingComposition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterGroupingComposition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitGroupingComposition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitGroupingComposition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupingCompositionContext groupingComposition() throws RecognitionException {
		GroupingCompositionContext _localctx = new GroupingCompositionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_groupingComposition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(124);
			aggregate();
			setState(125);
			match(BY);
			setState(126);
			grouping();
			setState(129);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EVERY) {
				{
				setState(127);
				match(EVERY);
				setState(128);
				interval();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class OpExprContext extends ExpressionContext {
		public OperationContext operation() {
			return getRuleContext(OperationContext.class,0);
		}
		public OpExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterOpExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitOpExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitOpExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MulopExprContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode MULOP() { return getToken(uStreamSightParser.MULOP, 0); }
		public MulopExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMulopExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMulopExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMulopExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DOUBLEContext extends ExpressionContext {
		public TerminalNode FLOAT() { return getToken(uStreamSightParser.FLOAT, 0); }
		public DOUBLEContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterDOUBLE(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitDOUBLE(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitDOUBLE(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddopExprContext extends ExpressionContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ADDOP() { return getToken(uStreamSightParser.ADDOP, 0); }
		public AddopExprContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterAddopExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitAddopExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitAddopExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 28;
		enterRecursionRule(_localctx, 28, RULE_expression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(134);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ID:
			case STRING:
				{
				_localctx = new OpExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(132);
				operation();
				}
				break;
			case FLOAT:
				{
				_localctx = new DOUBLEContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(133);
				match(FLOAT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(144);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(142);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
					case 1:
						{
						_localctx = new MulopExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(136);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(137);
						match(MULOP);
						setState(138);
						expression(5);
						}
						break;
					case 2:
						{
						_localctx = new AddopExprContext(new ExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(139);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(140);
						match(ADDOP);
						setState(141);
						expression(4);
						}
						break;
					}
					} 
				}
				setState(146);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class OperationContext extends ParserRuleContext {
		public OperationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_operation; }
	 
		public OperationContext() { }
		public void copyFrom(OperationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MetricStreamExprContext extends OperationContext {
		public MetricStreamContext metricStream() {
			return getRuleContext(MetricStreamContext.class,0);
		}
		public MetricStreamExprContext(OperationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMetricStreamExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMetricStreamExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMetricStreamExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CumulativeExprContext extends OperationContext {
		public CumulativeContext cumulative() {
			return getRuleContext(CumulativeContext.class,0);
		}
		public CumulativeExprContext(OperationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterCumulativeExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitCumulativeExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitCumulativeExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AggregateExprContext extends OperationContext {
		public AggregateContext aggregate() {
			return getRuleContext(AggregateContext.class,0);
		}
		public AggregateExprContext(OperationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterAggregateExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitAggregateExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitAggregateExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OperationContext operation() throws RecognitionException {
		OperationContext _localctx = new OperationContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_operation);
		try {
			setState(150);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new AggregateExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(147);
				aggregate();
				}
				break;
			case 2:
				_localctx = new MetricStreamExprContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(148);
				metricStream();
				}
				break;
			case 3:
				_localctx = new CumulativeExprContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(149);
				cumulative();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetricContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(uStreamSightParser.STRING, 0); }
		public MetricContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_metric; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMetric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMetric(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMetric(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MetricContext metric() throws RecognitionException {
		MetricContext _localctx = new MetricContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_metric);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(152);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MembershipContext extends ParserRuleContext {
		public TerminalNode LPAR() { return getToken(uStreamSightParser.LPAR, 0); }
		public List<MemberContext> member() {
			return getRuleContexts(MemberContext.class);
		}
		public MemberContext member(int i) {
			return getRuleContext(MemberContext.class,i);
		}
		public TerminalNode RPAR() { return getToken(uStreamSightParser.RPAR, 0); }
		public List<TerminalNode> COMMA() { return getTokens(uStreamSightParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(uStreamSightParser.COMMA, i);
		}
		public MembershipContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_membership; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMembership(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMembership(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMembership(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MembershipContext membership() throws RecognitionException {
		MembershipContext _localctx = new MembershipContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_membership);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(LPAR);
			setState(155);
			member();
			setState(160);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(156);
				match(COMMA);
				setState(157);
				member();
				}
				}
				setState(162);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(163);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemberContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public MemberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_member; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMember(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMember(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMember(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemberContext member() throws RecognitionException {
		MemberContext _localctx = new MemberContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_member);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(165);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregateContext extends ParserRuleContext {
		public AggregateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregate; }
	 
		public AggregateContext() { }
		public void copyFrom(AggregateContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class WindowedAggrExprContext extends AggregateContext {
		public WindowedFunctionContext windowedFunction() {
			return getRuleContext(WindowedFunctionContext.class,0);
		}
		public TerminalNode LPAR() { return getToken(uStreamSightParser.LPAR, 0); }
		public MetricStreamContext metricStream() {
			return getRuleContext(MetricStreamContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(uStreamSightParser.COMMA, 0); }
		public WindowContext window() {
			return getRuleContext(WindowContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(uStreamSightParser.RPAR, 0); }
		public WindowedAggrExprContext(AggregateContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterWindowedAggrExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitWindowedAggrExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitWindowedAggrExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AggregateContext aggregate() throws RecognitionException {
		AggregateContext _localctx = new AggregateContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_aggregate);
		try {
			_localctx = new WindowedAggrExprContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(167);
			windowedFunction();
			setState(168);
			match(LPAR);
			setState(169);
			metricStream();
			setState(170);
			match(COMMA);
			setState(171);
			window();
			setState(172);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CumulativeContext extends ParserRuleContext {
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public TerminalNode LPAR() { return getToken(uStreamSightParser.LPAR, 0); }
		public MetricStreamContext metricStream() {
			return getRuleContext(MetricStreamContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(uStreamSightParser.RPAR, 0); }
		public CumulativeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cumulative; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterCumulative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitCumulative(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitCumulative(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CumulativeContext cumulative() throws RecognitionException {
		CumulativeContext _localctx = new CumulativeContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_cumulative);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			function();
			setState(175);
			match(LPAR);
			setState(176);
			metricStream();
			setState(177);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MetricStreamContext extends ParserRuleContext {
		public MetricContext metric() {
			return getRuleContext(MetricContext.class,0);
		}
		public TerminalNode FROM() { return getToken(uStreamSightParser.FROM, 0); }
		public MembershipContext membership() {
			return getRuleContext(MembershipContext.class,0);
		}
		public MetricStreamContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_metricStream; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterMetricStream(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitMetricStream(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitMetricStream(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MetricStreamContext metricStream() throws RecognitionException {
		MetricStreamContext _localctx = new MetricStreamContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_metricStream);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179);
			metric();
			setState(182);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				setState(180);
				match(FROM);
				setState(181);
				membership();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_function);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowedFunctionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public WindowedFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_windowedFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterWindowedFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitWindowedFunction(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitWindowedFunction(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowedFunctionContext windowedFunction() throws RecognitionException {
		WindowedFunctionContext _localctx = new WindowedFunctionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_windowedFunction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WindowContext extends ParserRuleContext {
		public TimeperiodContext timeperiod() {
			return getRuleContext(TimeperiodContext.class,0);
		}
		public WindowContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_window; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterWindow(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitWindow(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitWindow(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WindowContext window() throws RecognitionException {
		WindowContext _localctx = new WindowContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_window);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			timeperiod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntervalContext extends ParserRuleContext {
		public TimeperiodContext timeperiod() {
			return getRuleContext(TimeperiodContext.class,0);
		}
		public IntervalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interval; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterInterval(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitInterval(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitInterval(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IntervalContext interval() throws RecognitionException {
		IntervalContext _localctx = new IntervalContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_interval);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			timeperiod();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupingContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(uStreamSightParser.STRING, 0); }
		public GroupingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_grouping; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterGrouping(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitGrouping(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitGrouping(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GroupingContext grouping() throws RecognitionException {
		GroupingContext _localctx = new GroupingContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_grouping);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TimeperiodContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(uStreamSightParser.INTEGER, 0); }
		public TerminalNode TIME_PERIOD() { return getToken(uStreamSightParser.TIME_PERIOD, 0); }
		public TimeperiodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_timeperiod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterTimeperiod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitTimeperiod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitTimeperiod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TimeperiodContext timeperiod() throws RecognitionException {
		TimeperiodContext _localctx = new TimeperiodContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_timeperiod);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(INTEGER);
			setState(195);
			match(TIME_PERIOD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 14:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		case 1:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\34\u00c8\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\3\2\3\2\3\2\7\2>\n\2\f\2\16\2"+
		"A\13\2\3\2\5\2D\n\2\3\2\3\2\3\3\3\3\5\3J\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\5\3\5\3\6\3\6\3\6\5\6W\n\6\3\6\3\6\3\7\3\7\3\7\7\7^\n\7\f\7\16\7a\13"+
		"\7\3\b\3\b\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3"+
		"\f\5\ft\n\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3"+
		"\17\3\17\5\17\u0084\n\17\3\20\3\20\3\20\5\20\u0089\n\20\3\20\3\20\3\20"+
		"\3\20\3\20\3\20\7\20\u0091\n\20\f\20\16\20\u0094\13\20\3\21\3\21\3\21"+
		"\5\21\u0099\n\21\3\22\3\22\3\23\3\23\3\23\3\23\7\23\u00a1\n\23\f\23\16"+
		"\23\u00a4\13\23\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\5\27\u00b9\n\27\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\35\3\35\2\3\36\36\2\4"+
		"\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668\2\3\3\2\27\31"+
		"\2\u00bb\2:\3\2\2\2\4I\3\2\2\2\6K\3\2\2\2\bQ\3\2\2\2\nS\3\2\2\2\fZ\3\2"+
		"\2\2\16b\3\2\2\2\20f\3\2\2\2\22h\3\2\2\2\24m\3\2\2\2\26s\3\2\2\2\30u\3"+
		"\2\2\2\32y\3\2\2\2\34~\3\2\2\2\36\u0088\3\2\2\2 \u0098\3\2\2\2\"\u009a"+
		"\3\2\2\2$\u009c\3\2\2\2&\u00a7\3\2\2\2(\u00a9\3\2\2\2*\u00b0\3\2\2\2,"+
		"\u00b5\3\2\2\2.\u00ba\3\2\2\2\60\u00bc\3\2\2\2\62\u00be\3\2\2\2\64\u00c0"+
		"\3\2\2\2\66\u00c2\3\2\2\28\u00c4\3\2\2\2:?\5\4\3\2;<\7\f\2\2<>\5\4\3\2"+
		"=;\3\2\2\2>A\3\2\2\2?=\3\2\2\2?@\3\2\2\2@C\3\2\2\2A?\3\2\2\2BD\7\f\2\2"+
		"CB\3\2\2\2CD\3\2\2\2DE\3\2\2\2EF\7\2\2\3F\3\3\2\2\2GJ\5\6\4\2HJ\5\22\n"+
		"\2IG\3\2\2\2IH\3\2\2\2J\5\3\2\2\2KL\5\b\5\2LM\7\r\2\2MN\7\4\2\2NO\7\5"+
		"\2\2OP\5\n\6\2P\7\3\2\2\2QR\7\n\2\2R\t\3\2\2\2ST\7\n\2\2TV\7\17\2\2UW"+
		"\5\f\7\2VU\3\2\2\2VW\3\2\2\2WX\3\2\2\2XY\7\20\2\2Y\13\3\2\2\2Z_\5\16\b"+
		"\2[\\\7\23\2\2\\^\5\16\b\2][\3\2\2\2^a\3\2\2\2_]\3\2\2\2_`\3\2\2\2`\r"+
		"\3\2\2\2a_\3\2\2\2bc\7\n\2\2cd\7\16\2\2de\5\20\t\2e\17\3\2\2\2fg\t\2\2"+
		"\2g\21\3\2\2\2hi\5\24\13\2ij\7\16\2\2jk\7\3\2\2kl\5\26\f\2l\23\3\2\2\2"+
		"mn\7\n\2\2n\25\3\2\2\2ot\5\30\r\2pt\5\32\16\2qt\5\34\17\2rt\5\36\20\2"+
		"so\3\2\2\2sp\3\2\2\2sq\3\2\2\2sr\3\2\2\2t\27\3\2\2\2uv\5\36\20\2vw\7\7"+
		"\2\2wx\5\64\33\2x\31\3\2\2\2yz\5\36\20\2z{\7\b\2\2{|\7\26\2\2|}\5\36\20"+
		"\2}\33\3\2\2\2~\177\5(\25\2\177\u0080\7\6\2\2\u0080\u0083\5\66\34\2\u0081"+
		"\u0082\7\7\2\2\u0082\u0084\5\64\33\2\u0083\u0081\3\2\2\2\u0083\u0084\3"+
		"\2\2\2\u0084\35\3\2\2\2\u0085\u0086\b\20\1\2\u0086\u0089\5 \21\2\u0087"+
		"\u0089\7\31\2\2\u0088\u0085\3\2\2\2\u0088\u0087\3\2\2\2\u0089\u0092\3"+
		"\2\2\2\u008a\u008b\f\6\2\2\u008b\u008c\7\25\2\2\u008c\u0091\5\36\20\7"+
		"\u008d\u008e\f\5\2\2\u008e\u008f\7\24\2\2\u008f\u0091\5\36\20\6\u0090"+
		"\u008a\3\2\2\2\u0090\u008d\3\2\2\2\u0091\u0094\3\2\2\2\u0092\u0090\3\2"+
		"\2\2\u0092\u0093\3\2\2\2\u0093\37\3\2\2\2\u0094\u0092\3\2\2\2\u0095\u0099"+
		"\5(\25\2\u0096\u0099\5,\27\2\u0097\u0099\5*\26\2\u0098\u0095\3\2\2\2\u0098"+
		"\u0096\3\2\2\2\u0098\u0097\3\2\2\2\u0099!\3\2\2\2\u009a\u009b\7\27\2\2"+
		"\u009b#\3\2\2\2\u009c\u009d\7\17\2\2\u009d\u00a2\5&\24\2\u009e\u009f\7"+
		"\23\2\2\u009f\u00a1\5&\24\2\u00a0\u009e\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2"+
		"\u00a0\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\u00a5\3\2\2\2\u00a4\u00a2\3\2"+
		"\2\2\u00a5\u00a6\7\20\2\2\u00a6%\3\2\2\2\u00a7\u00a8\7\n\2\2\u00a8\'\3"+
		"\2\2\2\u00a9\u00aa\5\60\31\2\u00aa\u00ab\7\17\2\2\u00ab\u00ac\5,\27\2"+
		"\u00ac\u00ad\7\23\2\2\u00ad\u00ae\5\62\32\2\u00ae\u00af\7\20\2\2\u00af"+
		")\3\2\2\2\u00b0\u00b1\5.\30\2\u00b1\u00b2\7\17\2\2\u00b2\u00b3\5,\27\2"+
		"\u00b3\u00b4\7\20\2\2\u00b4+\3\2\2\2\u00b5\u00b8\5\"\22\2\u00b6\u00b7"+
		"\7\5\2\2\u00b7\u00b9\5$\23\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9"+
		"-\3\2\2\2\u00ba\u00bb\7\n\2\2\u00bb/\3\2\2\2\u00bc\u00bd\7\n\2\2\u00bd"+
		"\61\3\2\2\2\u00be\u00bf\58\35\2\u00bf\63\3\2\2\2\u00c0\u00c1\58\35\2\u00c1"+
		"\65\3\2\2\2\u00c2\u00c3\7\27\2\2\u00c3\67\3\2\2\2\u00c4\u00c5\7\30\2\2"+
		"\u00c5\u00c6\7\t\2\2\u00c69\3\2\2\2\17?CIV_s\u0083\u0088\u0090\u0092\u0098"+
		"\u00a2\u00b8";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}