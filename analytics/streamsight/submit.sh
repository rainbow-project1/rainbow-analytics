MAIN_CLASS="eu.rainbowh2020.Main"
TOPOLOGY="streamsight-topology"
NETWORK="demo_rainbow-net"
STREAMSIGHT_FILE="scenario1.insights"
OUTPUT_MODE="RainbowStorage"
PROVIDER_HOSTS="ignite-server"
PROVIDER_PORT="50000"
CONSUMER_HOST="ignite-server"
CONSUMER_PORT="50000"

mvn clean antlr4:antlr4 package -DskipTests

docker run -it --rm --network $NETWORK storm storm kill $TOPOLOGY 2>&1  > /dev/null
if [ "$?" -ne "1" ]
then
  echo "Existing topology deleted, waiting 15s..."
  sleep 15
fi
docker run -it --rm --network $NETWORK  \
  -v $(pwd)/target/analytics-1.0-SNAPSHOT.jar:/analytics-1.0-SNAPSHOT.jar \
  -v $(pwd)/$STREAMSIGHT_FILE:/insights.insights \
    storm storm jar /analytics-1.0-SNAPSHOT.jar $MAIN_CLASS $TOPOLOGY $OUTPUT_MODE $PROVIDER_HOSTS $PROVIDER_PORT $CONSUMER_HOST $CONSUMER_PORT


