package eu.rainbowh2020.grammar;

import org.junit.Test;

public class TestGrammar {
    @Test
    public void test() throws Exception {

        GrammarTranslator translator =new GrammarTranslator();
        translator.parse(Helpers.loadFileStreamFromResources("query-test.ustream"));

        translator.getStreamDefinitions().values().forEach((stream)-> System.out.println(stream.pretty()) );
        translator.getInsightDefinitions().values().forEach((stream)-> System.out.println(stream.pretty()) );

    }

}

