package eu.rainbowh2020.storm;

import eu.rainbowh2020.grammar.compiler.bolts.PrintBolt;
import eu.rainbowh2020.grammar.compiler.bolts.joins.SimpleJoinStreams;
import eu.rainbowh2020.grammar.compiler.operations.Operation;
import eu.rainbowh2020.grammar.compiler.operations.PrintOperation;
import eu.rainbowh2020.grammar.compiler.operations.SelectOperation;
import eu.rainbowh2020.grammar.compiler.operations.LocalStreamOperation;
import eu.rainbowh2020.grammar.compiler.operations.WindowOperation;
import eu.rainbowh2020.grammar.compiler.spouts.LocalProvider;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import eu.rainbowh2020.grammar.model.NumberValue;
import eu.rainbowh2020.grammar.model.StringValue;
import eu.rainbowh2020.grammar.model.TimePeriod;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.bolt.JoinBolt;
import org.apache.storm.generated.Bolt;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.junit.Test;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class TestStorm {

    public void debugTopology(StormTopology topology,boolean exit) {

        Map<String, Bolt> bolts = topology.get_bolts();
        bolts.entrySet().forEach(e->{
            String id = e.getKey();
            Bolt v = e.getValue();
            System.out.println("Bolt:"+id);
            v.get_common().get_inputs().keySet().forEach(k->{

                System.out.println("\t"+k);
            });
            System.out.println();
        });
        if(exit)
            System.exit(1);

    }

    @Test
    public void testSum() throws Exception {
        List<Operation> operationList = new LinkedList<>();
        Map<String,Operation> operationsLookup = new HashMap<>();

        KeyValueParams properties = new KeyValueParams();
        properties.put("periodicity",new NumberValue(1000));
        properties.put("type",new StringValue("ones"));
        properties.put("prefix",new StringValue("f"));

        Operation stream1 = new LocalStreamOperation("stream1",properties);
        operationList.add(stream1);
        operationsLookup.put(stream1.id(),stream1);

        Operation sum_f1_by_f2 = new WindowOperation("sum", "f1",
                TimePeriod.create(10, TimeUnit.SECONDS.name()), TimePeriod.create(5, TimeUnit.SECONDS.name()));
        sum_f1_by_f2.addInputSource(stream1);
        operationList.add(sum_f1_by_f2);
        operationsLookup.put(sum_f1_by_f2.id(), sum_f1_by_f2);

        Operation print = new PrintOperation("test");
        print.addInputSource(sum_f1_by_f2);
        operationList.add(print);
        operationsLookup.put(print.id(),print);

        TopologyBuilder topologyBuilder = new TopologyBuilder();
        System.out.println(operationsLookup.keySet().stream().collect(Collectors.joining("\n")));
        for(Operation op: operationList){
            op.apply();
        }
        StormTopology topology = topologyBuilder.createTopology();

        LocalCluster cluster = new LocalCluster();
        Config config = new Config();
        config.setDebug(false);
        config.setNumWorkers(1);
        String topologyName = "test";
        cluster.submitTopology(topologyName,config,topology);
        Thread.sleep(120*1000);
        cluster.killTopology(topologyName);
        cluster.shutdown();
    }

    @Test
    public void testOperations() throws Exception {
        List<Operation> operationList = new LinkedList<>();
        Map<String,Operation> operationsLookup = new HashMap<>();

        KeyValueParams properties = new KeyValueParams();
        properties.put("periodicity",new NumberValue(100));
        properties.put("type",new StringValue("ones"));
        properties.put("prefix",new StringValue("f"));

        Operation stream1 = new LocalStreamOperation("stream1",properties);
        operationList.add(stream1);
        operationsLookup.put(stream1.id(),stream1);


        Operation select_f1 = new SelectOperation("f1");
        select_f1.addInputSource(stream1);
        operationList.add(select_f1);
        operationsLookup.put(select_f1.id(),select_f1);

        System.out.println("S1:"+select_f1.hashCode());

        Operation select_f2 = new SelectOperation("f2");
        select_f2.addInputSource(stream1);
        System.out.println("S2:"+select_f2.hashCode());

        Operation print = new PrintOperation("test");
        print.addInputSource(select_f1);
        operationList.add(print);
        operationsLookup.put(print.id(),print);


        TopologyBuilder topologyBuilder = new TopologyBuilder();
        System.out.println(operationsLookup.keySet().stream().collect(Collectors.joining("\n")));
        for(Operation op: operationList){
            op.apply();
        }
        StormTopology topology = topologyBuilder.createTopology();

        LocalCluster cluster = new LocalCluster();
        Config config = new Config();
        config.setDebug(false);
        config.setNumWorkers(1);
        String topologyName = "test";
        cluster.submitTopology(topologyName,config,topology);
        Thread.sleep(120*1000);
        cluster.killTopology(topologyName);
        cluster.shutdown();
    }

    @Test
    public void testJoin() throws Exception {
        KeyValueParams properties = new KeyValueParams();
        properties.put("periodicity",new NumberValue(1000));
        properties.put("type",new StringValue("ones"));
        properties.put("prefix",new StringValue("f"));
        LocalProvider stream1 = new LocalProvider("stream1",properties);

        properties.put("type",new StringValue("twos"));
        properties.put("prefix",new StringValue("f"));
        LocalProvider stream2 = new LocalProvider("stream2",properties);

        TopologyBuilder topologyBuilder = new TopologyBuilder();

        topologyBuilder.setSpout("stream1", stream1);
        topologyBuilder.setSpout("stream2", stream2);

        BoltDeclarer boltDeclarer;

        //boltDeclarer.localOrShuffleGrouping("stream1");
        //boltDeclarer.localOrShuffleGrouping("stream2");
        SimpleJoinStreams join = new SimpleJoinStreams(new Fields("f1"));
        //JoinBolt joinBolt = new JoinBolt("stream1","f1")
         //       .join("stream2","f1","stream1")
          //      .select("f1")
           //     .withWindow(BaseWindowedBolt.Duration.seconds(2), BaseWindowedBolt.Duration.seconds(1));
                //.withWindow(BaseWindowedBolt.Duration.seconds(1));

        boltDeclarer = topologyBuilder.setBolt("joiner",join,1);

        //boltDeclarer = topologyBuilder.setBolt("sum", new SumBolt("f1")
        //        .withWindow(BaseWindowedBolt.Duration.seconds(2)
        //        )
        //).setNumTasks(1) ;
        boltDeclarer.fieldsGrouping("stream1",new Fields("f1"));
        boltDeclarer.fieldsGrouping("stream2",new Fields("f1"));


        boltDeclarer = topologyBuilder.setBolt("print1", new PrintBolt("print1"));
        boltDeclarer.localOrShuffleGrouping("joiner");
        //boltDeclarer.globalGrouping("stream1",new Fields("f1"));


        StormTopology topology = topologyBuilder.createTopology();
        debugTopology(topology,false);

        LocalCluster cluster = new LocalCluster();
        Config config = new Config();
        config.setDebug(false);
        config.setNumWorkers(1);
        config.put("ustreamsight.logs", true);
        String topologyName = "test";
        cluster.submitTopology(topologyName,config,topology);
        Thread.sleep(120*1000);
        cluster.killTopology(topologyName);
        cluster.shutdown();

    }
    @Test
    public void test1() throws Exception {
        KeyValueParams properties = new KeyValueParams();
        properties.put("periodicity",new NumberValue(1000));
        properties.put("type",new StringValue("constant"));
        LocalProvider stream1 = new LocalProvider("stream1",properties);

        properties.put("type",new StringValue("constant"));
        properties.put("prefix",new StringValue("g"));
        LocalProvider stream2 = new LocalProvider("stream2",properties);
        TopologyBuilder topologyBuilder = new TopologyBuilder();

        topologyBuilder.setSpout("stream1", stream1);
        topologyBuilder.setSpout("stream2", stream2);

        JoinBolt joinBolt = new JoinBolt("stream1","f1")
                .join("stream2","g1","stream1")
                .select("f1,f2,g1,__time")
                .withWindow(new BaseWindowedBolt.Duration(5,TimeUnit.SECONDS),
                        new BaseWindowedBolt.Duration(1,TimeUnit.SECONDS));
               // .withTumblingWindow(new BaseWindowedBolt.Duration(2, TimeUnit.SECONDS));

        BoltDeclarer boltDeclarer = topologyBuilder.setBolt("join1", joinBolt);
        boltDeclarer.fieldsGrouping("stream1",new Fields("f1"))
                .fieldsGrouping("stream2",new Fields("g1"));


        boltDeclarer = topologyBuilder.setBolt("print1", new PrintBolt("print1"));
        boltDeclarer.globalGrouping("join1");
        //boltDeclarer.globalGrouping("stream1",new Fields("f1"));

        //JoinBolt joinBolt = new JoinBolt()
        //boltDeclarer.localOrShuffleGrouping("stream1");
        //boltDeclarer.localOrShuffleGrouping("stream2");


        StormTopology topology = topologyBuilder.createTopology();
        debugTopology(topology,false);

        LocalCluster cluster = new LocalCluster();
        Config config = new Config();
        config.setDebug(false);
        config.setNumWorkers(1);
        String topologyName = "test";
        cluster.submitTopology(topologyName,config,topology);
        Thread.sleep(120*1000);
        cluster.killTopology(topologyName);
        cluster.shutdown();

    }
}
