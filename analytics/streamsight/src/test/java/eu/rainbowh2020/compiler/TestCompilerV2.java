package eu.rainbowh2020.compiler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import eu.rainbowh2020.Domain.NetworkTopology;
import eu.rainbowh2020.grammar.GrammarTranslator;
import eu.rainbowh2020.grammar.Helpers;
import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import eu.rainbowh2020.grammar.compiler.uStreamSightCompiler;
import eu.rainbowh2020.grammar.model.InsightDefinition;
import eu.rainbowh2020.grammar.model.StreamDefinition;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.TopologyBuilder;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static eu.rainbowh2020.grammar.compiler.operations.PrintOperation.PrintOperationName;
import static java.lang.Float.parseFloat;

public class TestCompilerV2 {
    private static final String PYTHON = "python";
    private static final String CSV_FILE_NAME = "output.csv";
    private static final String SCRIPT_FILE_NAME = "Equality.py";
    private static final String ENABLED = "enabled";
    private static final String MODE = "mode";
    private static final Integer NUMBER_OF_MACHINES = 2;
    private static final String PYTHON_COMMAND = PYTHON + " " + SCRIPT_FILE_NAME + " " + CSV_FILE_NAME + " " + NUMBER_OF_MACHINES;

    /**
     * In this scenario we check if our select optimizations are valid
     */
    @Test
    public void scenario_1_select_optimization() throws Exception {
        Config config = new Config();
        config.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 1);
        config.setDebug(false);
        config.setNumWorkers(1);
        config.put("ustreamsight.logs", false);
        config.put("ustreamsight.spout.logs", false);
        run_scenario(true, 20, config, "scenario1.insights");
    }

    /**
     * In this scenario we check if our select optimizations are valid
     */
    @Test
    public void scenario_3_count_window() throws Exception {
        Config config = new Config();
        //Remember that the value must be greater than window + sliding interval
        config.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 30);
        config.setDebug(false);
        config.setNumWorkers(1);
        config.put("ustreamsight.logs", false);
        config.put("ustreamsight.spout.logs", false);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        File file = new File(classLoader.getResource("network-links.yaml").getFile());
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        NetworkTopology networkTopology = om.readValue(file, NetworkTopology.class);
        System.out.println("Number of items: " + networkTopology.getItems().size());
        run_scenario(true, 20, config, "scenario3.insights");
    }

    /**
     * In this scenario we check averagae
     */
    @Test
    public void scenario_4_average() throws Exception {
        Config config = new Config();
        //Remember that the value must be greater than window + sliding interval
        config.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 30);
        config.setDebug(false);
        config.setNumWorkers(1);
        config.put("ustreamsight.logs", false);
        config.put("ustreamsight.spout.logs", false);
        run_scenario(false, 20, config, "scenario4.insights");
    }


    public void run_scenario(boolean executePlan, int duration, Config config, String file) throws Exception {
        GrammarTranslator translator = new GrammarTranslator();
        translator.parse(Helpers.loadFileStreamFromResources(file));

        System.out.println("------------------------------------------------------");
        System.out.println(translator.getStreamDefinitions().values().stream().map(StreamDefinition::pretty).collect(Collectors.joining("\n")));
        System.out.println(translator.getInsightDefinitions().values().stream().map(InsightDefinition::pretty).collect(Collectors.joining("\n")));
        System.out.println("------------------------------------------------------");

        uStreamSightCompiler compiler = new uStreamSightCompiler();
        compiler.load(translator.getStreamDefinitions(), translator.getInsightDefinitions(), PrintOperationName, "", "", "", "");
        if (ENABLED.equals(config.get(MODE))) {
            ScheduledBolt.MODE = "enabled";
            ScheduledWindowedBolt.MODE = "enabled";
            File csvOutputFile = new File(CSV_FILE_NAME);
            try {
                PrintWriter pw = new PrintWriter(csvOutputFile);
                compiler.getOperations().values().stream()
                        .filter(e -> e.getOutputSource() != null)
                        .map(e -> e.id().concat("\t").concat(e.getOutputSource().id()))
                        .forEach(pw::println);

                Process p = Runtime.getRuntime().exec(PYTHON_COMMAND);
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println("stdout: " + line);
                }
            } catch (IOException e) {
                System.out.println("Exception in reading output" + e.toString());
            }

            compiler.getOperations().values().stream().filter(e -> e.getOutputSource() != null).forEach(e -> {
                String forwardBoltId = e.getOutputSource().id();
                IComponent component = e.getComponent();
                if (component instanceof ScheduledBolt) {
                    ((ScheduledBolt) component).setFractions(readFractions(forwardBoltId));
                    ((ScheduledBolt) component).setForwardBoltId(forwardBoltId);
                } else if (component instanceof ScheduledWindowedBolt) {
                    ((ScheduledWindowedBolt) component).setFractions(readFractions(forwardBoltId));
                    ((ScheduledWindowedBolt) component).setForwardBoltId(forwardBoltId);
                }
            });
        }

        TopologyBuilder topologyBuilder = new TopologyBuilder();
        compiler.getOperations().values().forEach(e -> e.declare(topologyBuilder));
        StormTopology topology = topologyBuilder.createTopology();

        System.out.println("------------------------------------------------------");
        compiler.debugPlan(topology);
        System.out.println("------------------------------------------------------");
        if (executePlan) {
            testTopology(topology, config, duration);
        }
    }

    public void testTopology(StormTopology topology, Config config, int durationInSeconds) throws Exception {
        LocalCluster cluster = new LocalCluster();
        String topologyName = "test";
        cluster.submitTopology(topologyName, config, topology);
        Thread.sleep(durationInSeconds * 1000);
        cluster.killTopology(topologyName);
        cluster.shutdown();
    }

    private TreeMap<Double, Integer> readFractions(String pathToFractionsFile) {
        final TreeMap<Double, Integer> fractions = new TreeMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathToFractionsFile + ".txt"));
            final String line = br.readLine();
            int id = 0;
            double sum = 0.0;
            for (String value : line.split(", ")) {
                if (parseFloat(value) != 0) {
                    sum += (int) (parseFloat(value) * 100);
                    fractions.put(sum, id);
                }
                id += 1;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fractions;
    }
}
