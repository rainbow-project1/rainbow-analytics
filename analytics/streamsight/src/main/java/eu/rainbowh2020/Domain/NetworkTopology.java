package eu.rainbowh2020.Domain;

import java.util.ArrayList;

public class NetworkTopology {
    private String apiVersion;
    private ArrayList<Object> items;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public ArrayList<Object> getItems() {
        return items;
    }

    public void setItems(ArrayList<Object> items) {
        this.items = items;
    }

}
