package eu.rainbowh2020.sample;

import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedSumBolt;
import eu.rainbowh2020.sample.input.RandomSpout;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.topology.TopologyBuilder;

public class SampleClusterTopology {
    public static void main(String[] args) throws Exception {
        TopologyBuilder topologyBuilder = new TopologyBuilder();
        topologyBuilder.setSpout("input", new RandomSpout());
        topologyBuilder.setBolt("sum", new WindowedSumBolt("record", "")).localOrShuffleGrouping("input");

        Config conf = new Config();
        conf.setNumWorkers(1);
        conf.setDebug(true);
        //LocalCluster localCluster = new LocalCluster();
        try {
            //localCluster.submitTopology("sample-topology",conf, topologyBuilder.createTopology() );
            StormSubmitter.submitTopology("sample-topology", conf, topologyBuilder.createTopology());
            //Thread.sleep(2000);
        } catch (AlreadyAliveException | AuthorizationException | InvalidTopologyException e) {
            System.out.println(e);
        }
    }
}
