package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.cumulative.AverageBolt;
import eu.rainbowh2020.grammar.compiler.bolts.cumulative.CountBolt;
import eu.rainbowh2020.grammar.compiler.bolts.cumulative.MaxBolt;
import eu.rainbowh2020.grammar.compiler.bolts.cumulative.MinBolt;
import eu.rainbowh2020.grammar.compiler.bolts.cumulative.SumBolt;

public class CumulativeOperation extends Operation {
    private String field;
    private String operation;

    public CumulativeOperation(String field, String operation) {
        this.name = "C";
        this.field = field;
        this.operation = operation;
        this.parameters = operation + "(" + field + ")";
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "count":
                bolt = new CountBolt(this.field, id);
                break;
            case "sum":
                bolt = new SumBolt(this.field, id);
                break;
            case "average":
            case "avg":
                //TODO Possible we can check if sum and count are already defined, so we can make use of them instead of
                // creating a new average operation
                bolt = new AverageBolt(this.field, id);
                break;
            case "maximum":
            case "max":
                bolt = new MaxBolt(this.field, id);
                break;
            case "minimum":
            case "min":
                bolt = new MinBolt(this.field, id);
                break;
            default:
                throw new Exception("Operation: " + operation + " is not supported");
        }
    }
}
