package eu.rainbowh2020.grammar.model;

public abstract class Value {
    public abstract Object getValue();
}
