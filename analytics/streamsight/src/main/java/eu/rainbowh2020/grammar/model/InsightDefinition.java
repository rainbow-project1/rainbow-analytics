package eu.rainbowh2020.grammar.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InsightDefinition implements PrettyPrint {

    private String name;

    private Composition composition;

    public String getName() {
        return name;
    }

    public Composition getComposition() {
        return composition;
    }

    public void setComposition(Composition composition) {
        this.composition = composition;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String pretty() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }

    }
}
