package eu.rainbowh2020.grammar.compiler.bolts.cumulative;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class CountBolt extends ScheduledBolt {
    private String field;
    private int count = 0;

    public CountBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field)) {
            super.setValues(new Values(count += 1));
            super.execute(tuple);
        }
    }

}
