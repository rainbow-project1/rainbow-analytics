package eu.rainbowh2020.grammar.model;

public class MulOp extends Expression {
    String op;
    Expression left;
    Expression right;

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    public String getOp() {
        return op;
    }

    public void setLeft(Expression left) {
        this.left = left;
    }

    public void setRight(Expression right) {
        this.right = right;
    }

    public void setOp(String op) {
        this.op = op;
    }
}
