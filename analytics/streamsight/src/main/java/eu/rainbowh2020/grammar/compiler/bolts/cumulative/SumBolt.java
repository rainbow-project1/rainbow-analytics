package eu.rainbowh2020.grammar.compiler.bolts.cumulative;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class SumBolt extends ScheduledBolt {
    private String field;
    private double sum = 0.0;

    public SumBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field)) {
            super.setValues(new Values(sum += (double) tuple.getValueByField(this.field)));
            super.execute(tuple);
        }
    }

}
