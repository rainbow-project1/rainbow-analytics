package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.SelectBolt;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class SelectOperation extends Operation {
    private String keys[];

    public SelectOperation(String... keys) {
        this.name = "select";
        this.parameters = Arrays.stream(keys).collect(Collectors.joining("_"));
        this.keys = keys;
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        boltDeclarer = topologyBuilder.setBolt(id, (IRichBolt) bolt);
        this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::localOrShuffleGrouping);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    public Set<String> getKeys() {
        return Arrays.stream(keys).collect(Collectors.toSet());
    }

    @Override
    public void apply() {
        bolt = new SelectBolt(this.id(), keys);
        /**  The above does the same job
         for(Operation operation: this.sources){
         String id1 = operation.id();
         boltDeclarer.localOrShuffleGrouping(id1);
         }
         **/
    }

}
