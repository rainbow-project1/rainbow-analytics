package eu.rainbowh2020.grammar.model;

public class FilteredComposition extends Composition {
    private String operator;
    private Expression filteredExpression;
    private Expression expression;

    public String getOperator() {
        return operator;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public Expression getFilteredExpression() {
        return filteredExpression;
    }

    public void setFilteredExpression(Expression filteredExpression) {
        this.filteredExpression = filteredExpression;
    }
}
