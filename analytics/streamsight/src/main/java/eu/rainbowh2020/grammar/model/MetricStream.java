package eu.rainbowh2020.grammar.model;

public class MetricStream extends Operation {
    private String metric;
    private Membership membership;

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }
}
