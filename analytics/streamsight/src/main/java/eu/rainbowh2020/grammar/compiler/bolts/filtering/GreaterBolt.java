package eu.rainbowh2020.grammar.compiler.bolts.filtering;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class GreaterBolt extends ScheduledBolt {
    private String filterId;
    private String inputId;
    private boolean filterSet = false;
    private boolean inputSet = false;
    private double filterValue = 0.0;
    private double inputValue = 0.0;

    public GreaterBolt(String filterId, String inputId, String id) {
        super(id);
        this.filterId = filterId;
        this.inputId = inputId;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            if (tuple.contains(filterId)) {
                filterValue = (double) tuple.getValueByField(filterId);
                filterSet = true;
            } else if (tuple.contains(inputId)) {
                inputValue = (double) tuple.getValueByField(inputId);
                inputSet = true;
            }
            if (filterSet && inputSet) {
                filterSet = false;
                inputSet = false;
                if (inputValue > filterValue) {
                    super.setValues(new Values(inputValue));
                    super.execute(tuple);
                }
            }
        }
    }

}
