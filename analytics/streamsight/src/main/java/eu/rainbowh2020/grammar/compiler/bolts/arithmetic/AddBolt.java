package eu.rainbowh2020.grammar.compiler.bolts.arithmetic;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class AddBolt extends ScheduledBolt {
    private double leftValue = 0.0;
    private double rightValue = 0.0;
    private String leftId;
    private String rightId;
    private boolean leftSet = false;
    private boolean rightSet = false;

    public AddBolt(String leftId, String rightId, String id) {
        super(id);
        this.leftId = leftId;
        this.rightId = rightId;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            if (tuple.contains(leftId)) {
                leftValue = (double) tuple.getValueByField(leftId);
                leftSet = true;
            } else if (tuple.contains(rightId)) {
                rightValue = (double) tuple.getValueByField(rightId);
                rightSet = true;
            }
            if (leftSet && rightSet) {
                leftSet = false;
                rightSet = false;
                super.setValues(new Values(leftValue + rightValue));
                super.execute(tuple);
            }
        }
    }

}
