package eu.rainbowh2020.grammar.compiler.bolts;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;

import java.io.IOException;
import java.time.Instant;
import java.util.Map;
import java.util.stream.Collectors;

public class RainbowStorageBolt implements IRichBolt {
    String id;
    private String url;

    public RainbowStorageBolt(String id, String url) {
        this.id = id;
        this.url = url;
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
    }

    @Override
    public void execute(Tuple input) {
        // TODO create class fields for these
        String ignite_url = "http://" + url + "/analytics/put";
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("text/plain");
        Long timestamp = Instant.now().getEpochSecond();
        String data = "{\"analytics\":[";
        for (String field : input.getFields().toList()) {
            Double val = (Double) input.getValueByField(field);
            String key = this.id;
            if (key == null){
                key = field;
            }
            data += "{\"key\":\"" + key + "\",\"val\":" + val + ",\"timestamp\":" + timestamp + "},";

        }
        data.substring(0, data.length() - 1);
        data += "]}";

        RequestBody body = RequestBody.create(mediaType, data);
        Request request = new Request.Builder()
                .url(ignite_url)
                .method("POST", body)
                .addHeader("Content-Type", "text/plain")
                .build();

        try {
            Response response = client.newCall(request).execute();
            String JSON_SOURCE = response.body().string();
            System.out.println(JSON_SOURCE);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String toString = input.getFields().toList().stream().map(field -> field + ":" + input.getValueByField(field))
                .collect(Collectors.joining(", ", id + ": [", "]"));
        System.out.println(toString);
    }

    @Override
    public void cleanup() {
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}