package eu.rainbowh2020.grammar.model;

public class DoubleValue extends Expression {
    Double value;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
