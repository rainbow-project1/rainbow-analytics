package eu.rainbowh2020.grammar.model;

public class WindowAggregate extends Aggregate {
    private String function;
    private MetricStream stream;
    private TimePeriod window;

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public MetricStream getStream() {
        return stream;
    }

    public void setStream(MetricStream stream) {
        this.stream = stream;
    }

    public TimePeriod getWindow() {
        return window;
    }

    public void setWindow(TimePeriod window) {
        this.window = window;
    }
}
