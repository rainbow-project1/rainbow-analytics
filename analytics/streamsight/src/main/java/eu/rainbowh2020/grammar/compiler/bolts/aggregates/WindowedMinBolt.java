package eu.rainbowh2020.grammar.compiler.bolts.aggregates;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.List;

public class WindowedMinBolt extends ScheduledWindowedBolt {
    private String field;

    public WindowedMinBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        double min = Double.MAX_VALUE;
        List<Tuple> tuples = inputWindow.get();
        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field)) {
                Double value = (double) tuple.getValueByField(this.field);
                min = value < min ? value : min;
            }
        }
        super.setValues(new Values(min));
        super.execute(inputWindow);
    }

}
