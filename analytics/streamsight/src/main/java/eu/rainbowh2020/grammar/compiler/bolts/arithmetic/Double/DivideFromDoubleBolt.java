package eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class DivideFromDoubleBolt extends ScheduledBolt {
    private double value;

    public DivideFromDoubleBolt(double value, String id) {
        super(id);
        this.value = value;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            double value = (double) tuple.getValue(0);
            if (value != 0) {
                super.setValues(new Values(this.value / value));
                super.execute(tuple);
            }
        }
    }

}
