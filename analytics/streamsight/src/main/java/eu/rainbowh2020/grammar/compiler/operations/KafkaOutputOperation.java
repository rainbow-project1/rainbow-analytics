package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.KafkaOutputBolt;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;

public class KafkaOutputOperation extends Operation {
    private String friendlyName;

    public KafkaOutputOperation(String friendlyName) {
        this.name = "kafka";
        this.friendlyName = friendlyName;
    }

    @Override
    public void apply() {
        // Our print operator is an exeption. Since we don't reuse it we override its id with the name
        bolt = new KafkaOutputBolt(friendlyName);
    }

    @Override
    public String id() {
        return this.friendlyName;
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        boltDeclarer = topologyBuilder.setBolt(id, (IRichBolt) bolt);
        this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::localOrShuffleGrouping);
    }
}
