package eu.rainbowh2020.grammar.compiler.bolts;

import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Return a Record with only the selected keys.
 */
public class SelectBolt extends ScheduledBolt {
    private static final Logger logger = LoggerFactory.getLogger(SelectBolt.class);

    private final String keys[];

    public SelectBolt(String id, String... keys) {
        super(id);
        this.keys = keys;
    }

    @Override
    public void execute(Tuple input) {
        Values values = new Values();
        for (String key : keys) {
            if (input.contains(key)) {
                values.add(input.getValueByField(key));
            }
        }
        if (values.isEmpty()) {
            if ((boolean) this.getConf().getOrDefault("ustreamsight.logs", false)) {
                System.out.println(this.getId() + " - " + " No fields matching keys: " + Arrays.stream(keys).collect(Collectors.joining(",")));
            }
        } else {

            if ((boolean) this.getConf().getOrDefault("ustreamsight.logs", false)) {
                System.out.println(this.getId() + ": " + values.toString());
            }
            super.setValues(values);
            super.execute(input);
        }

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(keys));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

}
