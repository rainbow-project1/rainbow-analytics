package eu.rainbowh2020.grammar.compiler.bolts.cumulative;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class MinBolt extends ScheduledBolt {
    private String field;
    private double min = Double.MAX_VALUE;

    public MinBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field)) {
            Double value = (double) tuple.getValueByField(this.field);
            min = value < min ? value : min;
            super.setValues(new Values(min));
            super.execute(tuple);
        }
    }

}
