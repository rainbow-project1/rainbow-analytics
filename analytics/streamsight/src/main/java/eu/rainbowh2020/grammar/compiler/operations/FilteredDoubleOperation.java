package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.EqualDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.GreaterDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.GreaterEqualDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.LessDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.LessEqualDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.Double.NotEqualDoubleBolt;

public class FilteredDoubleOperation extends Operation {
    private String operation;
    private Double value;

    public FilteredDoubleOperation(String operation, Double value) {
        this.name = "F";
        this.operation = operation;
        this.parameters = getOperation() + "(" + value + ")";
        this.value = value;
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "==":
                bolt = new EqualDoubleBolt(value, id);
                break;
            case "!=":
                bolt = new NotEqualDoubleBolt(value, id);
                break;
            case ">=":
                bolt = new GreaterEqualDoubleBolt(value, id);
                break;
            case "<=":
                bolt = new LessEqualDoubleBolt(value, id);
                break;
            case "<":
                bolt = new LessDoubleBolt(value, id);
                break;
            case ">":
                bolt = new GreaterDoubleBolt(value, id);
                break;
        }
    }

    private String getOperation() {
        switch (this.operation) {
            case "==":
                return "equals";
            case "!=":
                return "not_equals";
            case ">=":
                return "greater_equals";
            case "<=":
                return "less_equals";
            case "<":
                return "less";
            case ">":
                return "greater";
            default:
                throw new IllegalArgumentException("Unexpected value: " + this.operation);
        }
    }
}
