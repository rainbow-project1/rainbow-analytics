package eu.rainbowh2020.grammar.compiler.operations.kafkahelpers;

import java.io.IOException;

public interface KafkaRecord {
    public String toJson();

    public void readJson(String jsonInString) throws IOException;
}
