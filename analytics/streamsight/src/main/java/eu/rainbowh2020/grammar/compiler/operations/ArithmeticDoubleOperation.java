package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.AddDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.DivideDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.DivideFromDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.MultiplyDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.SubtractDoubleBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double.SubtractFromDoubleBolt;

public class ArithmeticDoubleOperation extends Operation {
    private String operation;
    private Double value;
    private Character position;

    public ArithmeticDoubleOperation(String operation, Double value, Character position) {
        this.name = "D";
        this.operation = operation;
        this.parameters = getOperation() + "(" + value + "," + position + ")";
        this.value = value;
        this.position = position;
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "+":
                bolt = new AddDoubleBolt(value, id);
                break;
            case "-":
                if (position == 'L') {
                    bolt = new SubtractFromDoubleBolt(value, id);
                } else if (position == 'R') {
                    bolt = new SubtractDoubleBolt(value, id);
                }
                break;
            case "*":
                bolt = new MultiplyDoubleBolt(value, id);
                break;
            case "/":
                if (position == 'L') {
                    bolt = new DivideFromDoubleBolt(value, id);
                } else if (position == 'R') {
                    bolt = new DivideDoubleBolt(value, id);
                }
                break;
        }
    }

    private String getOperation() {
        switch (this.operation) {
            case "+":
                return "add";
            case "-":
                return "subtract";
            case "*":
                return "multiply";
            case "/":
                return "divide";
            default:
                throw new IllegalArgumentException("Unexpected value: " + this.operation);
        }
    }
}
