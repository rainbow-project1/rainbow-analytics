package eu.rainbowh2020.grammar;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Helpers {
    public static InputStream loadFileStreamFromResources(String file){
        return  Helpers.class.getResourceAsStream("/"+file);
    }
    public static InputStream loadFileStream(String file) throws FileNotFoundException {
        return new FileInputStream(file);
    }
}
