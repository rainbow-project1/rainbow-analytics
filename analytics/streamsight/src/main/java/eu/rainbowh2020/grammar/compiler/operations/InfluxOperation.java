package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.InfluxBolt;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;

public class InfluxOperation extends Operation {
    public static final String InfluxOperationName = "influx";
    private IRichBolt bolt;
    private BoltDeclarer boltDeclarer;
    private String friendlyName;
    private String url;

    public InfluxOperation(String friendlyName, String url) {
        this.name = InfluxOperationName;
        this.friendlyName = friendlyName;
        this.url = url;
    }

    @Override
    public void apply() {
        // Our print operator is an exeption. Since we don't reuse it we override its id with the name
        bolt = new InfluxBolt(friendlyName, url);
    }

    @Override
    public IComponent getComponent() {
        return bolt;
    }

    @Override
    public String id() {
        return this.friendlyName;
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        boltDeclarer = topologyBuilder.setBolt(id, bolt);
        this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::localOrShuffleGrouping);
    }
}