package eu.rainbowh2020.grammar.model;

public class IntervalComposition extends Composition {
    private Expression expression;
    private TimePeriod interval;

    public TimePeriod getInterval() {
        return interval;
    }

    public void setInterval(TimePeriod interval) {
        this.interval = interval;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
