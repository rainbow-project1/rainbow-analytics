package eu.rainbowh2020.grammar.compiler.bolts.filtering.Double;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class EqualDoubleBolt extends ScheduledBolt {
    private double value;

    public EqualDoubleBolt(double value, String id) {
        super(id);
        this.value = value;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty() && (double) tuple.getValue(0) == value) {
            super.setValues(new Values(tuple.getValue(0)));
            super.execute(tuple);
        }
    }

}
