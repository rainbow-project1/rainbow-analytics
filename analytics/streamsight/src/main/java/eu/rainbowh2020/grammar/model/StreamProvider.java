package eu.rainbowh2020.grammar.model;

public class StreamProvider {
    private String name;
    private KeyValueParams properties;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KeyValueParams getProperties() {
        return properties;
    }

    public void setProperties(KeyValueParams properties) {
        this.properties = properties;
    }
}
