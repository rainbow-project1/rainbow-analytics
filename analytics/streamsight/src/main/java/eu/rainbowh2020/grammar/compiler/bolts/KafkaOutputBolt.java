package eu.rainbowh2020.grammar.compiler.bolts;

import eu.rainbowh2020.grammar.compiler.operations.kafkahelpers.KafkaPublisher;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.stream.Collectors;

/**
 */
public class KafkaOutputBolt implements IRichBolt {
    private static final Logger logger = LoggerFactory.getLogger(KafkaOutputBolt.class);

    private Map<String,Object> conf;
    private String id;

    private KafkaPublisher publisher;

    public KafkaOutputBolt(String id){
        this.id = id;
    }
    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        this.conf = topoConf;
        // This s
        publisher = new KafkaPublisher("FIXME","FIXME");
    }
    @Override
    public void execute(Tuple input) {
        // TODO Send to Kafka...
        // send to kafka...
        // publisher.publish(//TODO Create a class that extends Kafka Record (JSON) ));
        String toString = input.getFields().toList().stream().map(field -> field + ":" + input.getValueByField(field))
                .collect(Collectors.joining(", ", id + ": [", "]"));
        System.out.println(toString);

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }


    @Override
    public void cleanup() {

    }
}
