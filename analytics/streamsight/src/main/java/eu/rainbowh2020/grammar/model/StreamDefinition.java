package eu.rainbowh2020.grammar.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StreamDefinition implements PrettyPrint{
    private String name;
    private StreamProvider provider;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StreamProvider getProvider() {
        return provider;
    }



    public void setProvider(StreamProvider provider) {
        this.provider = provider;
    }

    @Override
    public String pretty() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }

    }
}
