package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedAverageBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedCountBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedMaxBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedMinBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.WindowedSumBolt;
import eu.rainbowh2020.grammar.model.TimePeriod;
import org.apache.storm.topology.base.BaseWindowedBolt;

public class WindowOperation extends Operation {
    private String field;
    private String operation;
    private TimePeriod window;
    private TimePeriod interval;

    public WindowOperation(String operation, String field, TimePeriod window, TimePeriod interval) {
        this.name = "W";
        this.window = window;
        this.interval = interval;
        this.operation = operation;
        this.field = field;
        this.parameters = operation + "(" + field + "," + window.toMillis() + interval.toMillis() + ")";
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "count":
                bolt = new WindowedCountBolt(this.field, id);
                break;
            case "sum":
                bolt = new WindowedSumBolt(this.field, id);
                break;
            case "average":
            case "avg":
                //TODO Possible we can check if sum and count are already defined, so we can make use of them instead of
                // creating a new average operation
                bolt = new WindowedAverageBolt(this.field, id);
                break;
            case "maximum":
            case "max":
                bolt = new WindowedMaxBolt(this.field, id);
                break;
            case "minimum":
            case "min":
                bolt = new WindowedMinBolt(this.field, id);
                break;
            default:
                throw new Exception("Operation: " + operation + " is not supported");
        }
        bolt = ((ScheduledWindowedBolt) bolt).withWindow(BaseWindowedBolt.Duration.of(window.toMillis()),
                BaseWindowedBolt.Duration.of(interval.toMillis()));
    }

}
