package eu.rainbowh2020.grammar.compiler.spouts;

import eu.rainbowh2020.grammar.model.KeyValueParams;
import eu.rainbowh2020.grammar.model.NumberValue;
import eu.rainbowh2020.grammar.model.StringValue;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LocalProvider implements IRichSpout {
    private final Logger logger = LoggerFactory.getLogger(LocalProvider.class);
    private SpoutOutputCollector collector;
    private int periodicity;
    private String type;
    private String name;
    private String prefix;
    private int fields;
    private Map<String, Object> conf;
    private List<String> places;
    private Random random = new Random();

    public LocalProvider(String name, KeyValueParams properties) {
        this.name = name;
        periodicity = (int) properties.getOrDefault("periodicity", new NumberValue(1000)).getValue();
        type = (String) properties.getOrDefault("type", new StringValue("random")).getValue();
        prefix = (String) properties.getOrDefault("prefix", new StringValue("f")).getValue();
        fields = (int) properties.getOrDefault("fields", new NumberValue(4)).getValue();
        places = Arrays.asList("a", "b", "c", "d");

        logger.trace("Creating local workload with periodicity: "+periodicity+", type: "+type);
    }

    @Override
    public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        this.conf = conf;

    }

    @Override
    public void close() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    private Map<String,Integer> values = new HashMap<>();
    public double getValue(String key,int i){
        switch (type) {
            case "random":
                return Math.random();
            case "ones":
                return 1;
            case "twos":
                return 2;
            case "threes":
                return 3;
            case "incremental":
                Integer old = values.getOrDefault(key, 0);
                values.put(key, old + 1);
                return old;
            case "constant":
                return i;
            default:
                return 0;
        }

    }
    @Override
    public void nextTuple() {

        long time = System.currentTimeMillis()/1000;
        Values values = new Values();
        for(int i=1;i<=fields;i++) {
            values.add(getValue(prefix+i,i));
        }
        values.add(places.get(random.nextInt(places.size())));
        values.add(time);
        List<Integer> tasks = collector.emit(values, time);
        if((boolean)this.conf.getOrDefault("ustreamsight.spout.logs",false)){
            System.out.println(tasks+": "+values.toString());
        }

        try { Thread.sleep(periodicity); } catch (InterruptedException e) { e.printStackTrace(); }
    }

    @Override
    public void ack(Object msgId) {
        if((boolean)this.conf.getOrDefault("ustreamsight.spout.logs",false)) {
            System.out.println("[" + name + "] ack on msgId" + msgId);
        }
    }

    @Override
    public void fail(Object msgId) {

        if((boolean)this.conf.getOrDefault("ustreamsight.spout.logs",false)) {
            System.out.println("[" + name + "] fail on msgId" + msgId);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        List<String> fieldsNames = new LinkedList<>();
        for(int i=1;i<=this.fields;i++) {
            fieldsNames.add(prefix+i);
        }
        fieldsNames.add("place");
        fieldsNames.add("__time");
        declarer.declare(new Fields(fieldsNames));

    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
