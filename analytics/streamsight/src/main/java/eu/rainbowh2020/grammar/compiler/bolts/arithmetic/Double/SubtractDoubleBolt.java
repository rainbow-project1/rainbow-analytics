package eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class SubtractDoubleBolt extends ScheduledBolt {
    private double value;

    public SubtractDoubleBolt(double value, String id) {
        super(id);
        this.value = value;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            super.setValues(new Values((double) tuple.getValue(0) - this.value));
            super.execute(tuple);
        }
    }

}
