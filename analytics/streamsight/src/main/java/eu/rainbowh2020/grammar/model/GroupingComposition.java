package eu.rainbowh2020.grammar.model;

public class GroupingComposition extends Composition {
    private String groupByField;
    private WindowAggregate windowAggregate;
    private TimePeriod interval;

    public void setWindowAggregate(WindowAggregate windowAggregate) {
        this.windowAggregate = windowAggregate;
    }

    public WindowAggregate getWindowAggregate() {
        return windowAggregate;
    }

    public String getGroupByField() {
        return groupByField;
    }

    public void setGroupByField(String groupByField) {
        this.groupByField = groupByField;
    }

    public void setInterval(TimePeriod interval) {
        this.interval = interval;
    }

    public TimePeriod getInterval() {
        return interval;
    }
}
