package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.PrintBolt;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;

public class PrintOperation extends Operation {
    public static final String PrintOperationName = "print";
    private String friendlyName;

    public PrintOperation(String friendlyName) {
        this.name = PrintOperationName;
        this.friendlyName = friendlyName;
    }

    @Override
    public void apply() {
        // Our print operator is an exeption. Since we don't reuse it we override its id with the name
        bolt = new PrintBolt(friendlyName);
    }

    @Override
    public String id() {
        return this.friendlyName;
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        boltDeclarer = topologyBuilder.setBolt(id, (IRichBolt) bolt);
        this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::localOrShuffleGrouping);
    }
}
