package eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class DivideDoubleBolt extends ScheduledBolt {
    private double value;
    private String id;

    public DivideDoubleBolt(double value, String id) {
        super(id);
        if (value == 0) {
            throw new IllegalArgumentException("Cannot divide with 0");
        }
        this.value = value;
        this.id = id;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            super.setValues(new Values((double) tuple.getValue(0) / this.value));
            super.execute(tuple);
        }
    }

}
