package eu.rainbowh2020.grammar.compiler.spouts;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import eu.rainbowh2020.grammar.model.NumberValue;
import eu.rainbowh2020.grammar.model.StringValue;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichSpout;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IgniteProvider implements IRichSpout {
    private final Logger logger = LoggerFactory.getLogger(LocalProvider.class);
    private final String name;
    public List<String> fieldsNames = new ArrayList<String>();
    private SpoutOutputCollector collector;
    private int periodicity;
    private String metricID;
    private String entityID;

    private String podName;
    private String podNamespace;
    private String containerName;
    private ArrayList<ValueTuple> fieldData;

    private String url;
    private Map<String, Object> conf;

    private class ValueTuple implements Serializable {
        String name;
        Double value;

        public ValueTuple(String name, Double value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getValue() {
            return value;
        }

        public void setValue(Double value) {
            this.value = value;
        }
    }

    public IgniteProvider(String name, String url, KeyValueParams properties) throws Exception {
        this.name = name;
        this.url = url;
        periodicity = (int) properties.getOrDefault("periodicity", new NumberValue(1000)).getValue();
        entityID = (String) properties.getOrDefault("entityID", new StringValue("")).getValue();
        metricID = (String) properties.getOrDefault("metricID", new StringValue("")).getValue();

        podName = (String) properties.getOrDefault("podName", new StringValue("")).getValue();
        podNamespace = (String) properties.getOrDefault("podNamespace", new StringValue("")).getValue();
        containerName = (String) properties.getOrDefault("containerName", new StringValue("")).getValue();
        this.fieldData = this.get_data(true);
        this.updateFieldNames();//new LinkedList<String>();

    }

    public void updateFieldNames(){
        ArrayList<ValueTuple> monitoring = this.fieldData;
        for (ValueTuple mon : monitoring) {
            String curMetricId = mon.getName();
            if (!(this.fieldsNames.contains(curMetricId))){
                this.fieldsNames.add(curMetricId);
            }
        }
    }

    @Override
    public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
        this.conf = conf;

    }

    @Override
    public void close() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    public ArrayList make_call()  throws IOException{
        String ignite_url = "http://" + url + "/get";
        System.out.println(ignite_url);
        OkHttpClient client = new OkHttpClient();
        MediaType mediaType = MediaType.parse("text/plain");
        String content = "{   \n    \"latest\": true\n";
        if (!this.metricID.equals("")){
            content+=", \"metricID\": [\"" + this.metricID + "\"]";
        }
        if (!this.entityID.equals("")){
            content+=", \"entityID\": [\"" + this.entityID + "\"]";
        }
        if (!this.podName.equals("")){
            content+=", \"podName\": [\"" + this.podName + "\"]";
        }
        if (!this.podNamespace.equals("")){
            content+=", \"podNamespace\": [\"" + this.podNamespace + "\"]";
        }
        if (!this.containerName.equals("")){
            content+=", \"containerName\": [\"" + this.containerName + "\"]";
        }
        content += ", \"nodes\": [] \n}";
        RequestBody body = RequestBody.create(mediaType, content); // \"metricID\": [\"" + metricId + "\"],\n    \"entityID\": [\"" + entityId + "\"],\n

        Request request = new Request.Builder()
                .url(ignite_url)
                .method("POST", body)
                .addHeader("Content-Type", "text/plain")
                .build();
        Response response = client.newCall(request).execute();
        String JSON_SOURCE = response.body().string();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();

        HashMap<String, List<HashMap<String, Object>>> result;
        ArrayList<HashMap<String, Object>> monitoring;
        //Class<? extends Map> mapClass, Class<?> keyClass, Class<?> valueClass

        result = objectMapper.readValue(JSON_SOURCE, objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, ArrayList.class));
        monitoring = (ArrayList<HashMap<String, Object>>) result.getOrDefault("monitoring", new ArrayList<HashMap<String, Object>>());

        return monitoring;

    }

    public ArrayList<ValueTuple> get_data(Boolean all) throws IOException {
        Double time = new Double(System.currentTimeMillis() / 1000);
        ArrayList<HashMap<String, Object>> monitoring = this.make_call();
        ArrayList<ValueTuple> final_results = new ArrayList();
        for (HashMap obj : monitoring) {
            ArrayList<HashMap<String, Object>> datas = (ArrayList) obj.get("data");
            for (HashMap<String, Object> data: datas){
                String curMetricId = (String) data.get("metricID");
                ArrayList<HashMap> values = (ArrayList) data.get("values");
                if (values != null && !values.isEmpty()){
                    Double value = (Double) values.get(0).get("val");
                    Integer timestamp = (Integer) values.get(0).get("timestamp");
                    if (Math.abs(time-timestamp.doubleValue())<20 || all){
                        final_results.add(new ValueTuple(curMetricId, value));
                    }
                }
            }
        }
        final_results.add(new ValueTuple("__time", time));
        return final_results;
    }

    @Override
    public void nextTuple() {
        Values values = new Values();
        try {
            this.fieldData = this.get_data(false);
        } catch (IOException e) {
            this.fieldData = new ArrayList<ValueTuple>();
            e.printStackTrace();
        }
        if (this.fieldData.size()>1){
            for (ValueTuple value : this.fieldData) {
                values.add(value.getValue());
            }
            List<Integer> tasks = collector.emit(values,System.currentTimeMillis() / 1000);
            if ((boolean) this.conf.getOrDefault("ustreamsight.spout.logs", false)) {
                System.out.println(tasks + ": " + values.toString());
            }
        }

        try {
            Thread.sleep(periodicity);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ack(Object msgId) {
        if ((boolean) this.conf.getOrDefault("ustreamsight.spout.logs", false)) {
            System.out.println("[" + name + "] ack on msgId" + msgId);
        }
    }

    @Override
    public void fail(Object msgId) {

        if ((boolean) this.conf.getOrDefault("ustreamsight.spout.logs", false)) {
            System.out.println("[" + name + "] fail on msgId" + msgId);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(this.fieldsNames));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
