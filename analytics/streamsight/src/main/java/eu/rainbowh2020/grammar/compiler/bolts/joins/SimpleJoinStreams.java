package eu.rainbowh2020.grammar.compiler.bolts.joins;

import org.apache.storm.Config;
import org.apache.storm.generated.GlobalStreamId;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.utils.TimeCacheMap;

import java.util.*;

public class SimpleJoinStreams extends BaseRichBolt {
    OutputCollector collector;
    Fields idFields;
    Fields outFields;
    int numSources;
    TimeCacheMap<String, Map<GlobalStreamId, Tuple>> pending;
    Map<String, GlobalStreamId> fieldLocations;

    public SimpleJoinStreams(Fields outFields) {
        this.outFields = outFields;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(outFields);
    }

    @Override
    public void prepare(Map<String, Object> conf, TopologyContext context, OutputCollector collector) {
            this.collector = collector;
            fieldLocations = new HashMap<>();
            numSources = context.getThisSources().size();
        int timeout = ((Number) conf.get(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS)).intValue();
        pending = new TimeCacheMap<>(timeout, new ExpireCallback());
        Set<String> idFields = null;
        for (GlobalStreamId source : context.getThisSources().keySet()) {
            Fields fields = context.getComponentOutputFields(source.get_componentId(), source.get_streamId());
            Set<String> setFields = new HashSet<>(fields.toList());
            if (idFields == null) {
                idFields = setFields;
            } else {
                idFields.retainAll(setFields);
            }
            for (String outfield : outFields) {
                for (String sourcefield : fields) {
                    if (outfield.equals(sourcefield)) {
                        fieldLocations.put(outfield, source);
                    }
                }
            }
        }
        this.idFields = new Fields(new ArrayList<>(idFields));

        if (fieldLocations.size() != outFields.size()) {
            throw new RuntimeException("Cannot find all outfields among sources");
        }

}

    @Override
    public void execute(Tuple tuple) {
        String all = "all";
        GlobalStreamId streamId = new GlobalStreamId(tuple.getSourceComponent(), tuple.getSourceStreamId());
        if (!pending.containsKey(all)) {
            pending.put(all, new HashMap<>());
        }
        Map<GlobalStreamId, Tuple> parts = pending.get(all);
        if (parts.containsKey(streamId)) {
            throw new RuntimeException("Received same side of single join twice");
        }
        parts.put(streamId, tuple);
        if (parts.size() == numSources) {
            pending.remove(all);
            List<Object> joinResult = new ArrayList<>();
            Map<String,List<Object>> aggregates = new HashMap<>();
            for(Tuple t: parts.values()){
                for (String outField : outFields) {
                    List<Object> list = aggregates.getOrDefault(outField, new LinkedList<>());
                    list.add(t.getValueByField(outField));
                    aggregates.putIfAbsent(outField,list);
                }
            }
            for(String outField: outFields){
                joinResult.add(aggregates.get(outField));
            }
            collector.emit(new ArrayList<>(parts.values()), joinResult);

            for (Tuple part : parts.values()) {
                collector.ack(part);
            }
        }

    }
    private class ExpireCallback implements TimeCacheMap.ExpiredCallback<String, Map<GlobalStreamId, Tuple>> {
        @Override
        public void expire(String id, Map<GlobalStreamId, Tuple> tuples) {
            for (Tuple tuple : tuples.values()) {
                collector.fail(tuple);
            }
        }
    }
}
