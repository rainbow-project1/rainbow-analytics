package eu.rainbowh2020.grammar.compiler.bolts.cumulative;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class MaxBolt extends ScheduledBolt {
    private String field;
    private double max = Double.MIN_VALUE;

    public MaxBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field)) {
            Double value = (double) tuple.getValueByField(this.field);
            max = value > max ? value : max;
            super.setValues(new Values(max));
            super.execute(tuple);
        }
    }

}
