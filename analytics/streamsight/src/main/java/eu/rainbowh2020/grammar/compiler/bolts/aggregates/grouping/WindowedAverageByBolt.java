package eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WindowedAverageByBolt extends ScheduledWindowedBolt {
    private String field;
    private String groupByField;

    public WindowedAverageByBolt(String field, String groupByField, String id) {
        super(id);
        this.field = field;
        this.groupByField = groupByField;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        Map<String, Double> mapSumValues = new HashMap<>();
        Map<String, Integer> mapCountValues = new HashMap<>();
        List<Tuple> tuples = inputWindow.get();

        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field) && tuple.contains(this.groupByField)) {
                addToKey((double) tuple.getValueByField(this.field),
                        (String) tuple.getValueByField(this.groupByField),
                        mapSumValues,
                        mapCountValues);
            }
        }
        String result = mapCountValues.entrySet()
                .stream()
                .map(e -> e.getKey() + ":" + (e.getValue() == 0 ? 0 : mapSumValues.get(e.getKey()) / e.getValue()))
                .collect(Collectors.joining(" "));
        super.setValues(new Values(result));
        super.execute(inputWindow);
    }

    private void addToKey(double value, String key, Map<String,
            Double> mapSumValues, Map<String, Integer> mapCountValues) {
        if (!mapSumValues.containsKey(key)) {
            mapSumValues.put(key, value);
            mapCountValues.put(key, 1);
        } else {
            mapSumValues.put(key, mapSumValues.get(key) + value);
            mapCountValues.put(key, mapCountValues.get(key) + 1);
        }
    }

}
