package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.spouts.IgniteProvider;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.TopologyBuilder;

public class IgniteStreamOperation extends Operation {

    private String streamName;
    private KeyValueParams properties;
    private IgniteProvider component;
    private String url;

    public IgniteStreamOperation(String name, String url, KeyValueParams properties) {
        this.name = "S";
        this.url = url;
        this.parameters = name.concat("_").concat(url);
        this.streamName = name;
        this.properties = properties;
    }

    @Override
    public void apply() throws Exception {
        component = new IgniteProvider(streamName, url, properties);
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        topologyBuilder.setSpout(this.id(), component);
    }

    @Override
    public IComponent getComponent() {
        return component;
    }
}
