package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.spouts.LocalProvider;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.TopologyBuilder;

public class LocalStreamOperation extends Operation {

    private String streamName;
    private KeyValueParams properties;
    private LocalProvider component;

    public LocalStreamOperation(String name, KeyValueParams properties) {
        this.name = "S";
        this.parameters = name;
        this.streamName = name;
        this.properties = properties;
    }

    @Override
    public void apply() {
        component = new LocalProvider(streamName, properties);
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        topologyBuilder.setSpout(this.id(), component);
    }

    @Override
    public IComponent getComponent() {
        return component;
    }
}
