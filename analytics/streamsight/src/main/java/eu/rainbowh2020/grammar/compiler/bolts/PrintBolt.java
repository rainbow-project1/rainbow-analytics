package eu.rainbowh2020.grammar.compiler.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.stream.Collectors;

/**
 *  Return a Record with only the selected keys.
 */
public class PrintBolt implements IRichBolt {
    private static final Logger logger = LoggerFactory.getLogger(PrintBolt.class);

    private Map<String,Object> conf;
    private String id;

    public PrintBolt(String id){
        this.id = id;
    }
    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        this.conf = topoConf;
    }
    @Override
    public void execute(Tuple input) {
        // To Kafka...
        String toString = input.getFields().toList().stream().map(field -> field + ":" + input.getValueByField(field))
                .collect(Collectors.joining(", ", id + ": [", "]"));
        System.out.println(toString);

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }


    @Override
    public void cleanup() {

    }
}
