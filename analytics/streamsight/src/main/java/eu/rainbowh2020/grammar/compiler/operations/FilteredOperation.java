package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.filtering.EqualBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.GreaterBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.GreaterEqualBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.LessBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.LessEqualBolt;
import eu.rainbowh2020.grammar.compiler.bolts.filtering.NotEqualBolt;

public class FilteredOperation extends Operation {
    private String operation;
    private String filterId;
    private String inputId;

    public FilteredOperation(String operation, String inputId, String filterId) {
        this.name = "F";
        this.operation = operation;
        this.inputId = inputId;
        this.filterId = filterId;
        this.parameters = getOperation();
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "==":
                bolt = new EqualBolt(filterId, inputId, id);
                break;
            case "!=":
                bolt = new NotEqualBolt(filterId, inputId, id);
                break;
            case ">=":
                bolt = new GreaterEqualBolt(filterId, inputId, id);
                break;
            case "<=":
                bolt = new LessEqualBolt(filterId, inputId, id);
                break;
            case "<":
                bolt = new LessBolt(filterId, inputId, id);
                break;
            case ">":
                bolt = new GreaterBolt(filterId, inputId, id);
                break;
        }
    }

    private String getOperation() {
        switch (this.operation) {
            case "==":
                return "equals";
            case "!=":
                return "not_equals";
            case ">=":
                return "greater_equals";
            case "<=":
                return "less_equals";
            case "<":
                return "less";
            case ">":
                return "greater";
            default:
                throw new IllegalArgumentException("Unexpected value: " + this.operation);
        }
    }
}
