package eu.rainbowh2020.grammar.model;


import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class TimePeriod implements Serializable {

    private int duration;
    private TimeUnit unit;

    public TimePeriod(){ }

    private TimePeriod(Builder builder){
        this.duration = builder.duration;
        this.unit = builder.unit;
    }
    private TimePeriod(int duration, TimeUnit unit){
        this.duration = duration;
        this.unit = unit;
    }

    public static TimePeriod create(int duration, String unit){
        if(unit.equals("MILLIS") || unit.equals("ms"))
            return new TimePeriod(duration, TimeUnit.MILLISECONDS);
        if(unit.equals("SECONDS") || unit.equals("s"))
            return new TimePeriod(duration, TimeUnit.SECONDS);
        if(unit.equals("MINUTES") || unit.equals("m"))
            return new TimePeriod(duration, TimeUnit.MINUTES);
        if(unit.equals("HOURS") || unit.equals("h"))
            return new TimePeriod(duration, TimeUnit.HOURS);
        return new TimePeriod(duration, TimeUnit.SECONDS);
    }
    public static class Builder {

        private int duration;
        private TimeUnit unit;

        public Builder(){
        }

        public Builder duration(int duration){
            this.duration = duration;
            return this;
        }

        public Builder unit(TimeUnit unit){
            this.unit = unit;
            return this;
        }

        public TimePeriod build(){
            return new TimePeriod(this);

        }
    }

    public TimeUnit getUnit() {
        return unit;
    }

    public int toMillis(){
        return (int) this.unit.toMillis(this.duration);
    }
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "TimePeriod{" +
                "duration=" + duration +
                ", unit=" + unit +
                '}';
    }
}
