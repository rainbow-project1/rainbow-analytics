package eu.rainbowh2020.grammar.compiler.bolts;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;

import java.time.Instant;
import java.util.Map;
import java.util.stream.Collectors;

public class InfluxBolt implements IRichBolt {
    String id;
    private InfluxDBClient influxDB;

    public InfluxBolt(String id, String url) {
        this.id = id;
        influxDB = InfluxDBClientFactory.create(url, "rainbow-eu".toCharArray());
    }

    @Override
    public void prepare(Map<String, Object> map, TopologyContext topologyContext, OutputCollector outputCollector) {
    }

    @Override
    public void execute(Tuple input) {
        input.getFields().toList().stream().forEach(field -> this.send(this.id, field, input));
        String toString = input.getFields().toList().stream().map(field -> field + ":" + input.getValueByField(field))
                .collect(Collectors.joining(", ", id + ": [", "]"));
        System.out.println(toString);
    }

    @Override
    public void cleanup() {
    }

    private void send(String id, String field, Tuple input) {
        String dbName = "streamsight";
        String org = "primary";
        input.getValueByField(field);

        Point point = Point.measurement(id)
                .time(Instant.now(), WritePrecision.NS)
                .addField(field, (Double) input.getValueByField(field));

        try (WriteApi writeApi = influxDB.getWriteApi()) {
            writeApi.writePoint(dbName, org, point);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}