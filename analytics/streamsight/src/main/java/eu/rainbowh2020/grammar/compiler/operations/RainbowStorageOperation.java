package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.RainbowStorageBolt;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.TopologyBuilder;

public class RainbowStorageOperation extends Operation {
    public static final String RainbowStorageOperationName = "RainbowStorage";
    private IRichBolt bolt;
    private BoltDeclarer boltDeclarer;
    private String friendlyName;
    private String url;

    public RainbowStorageOperation(String friendlyName, String url) {
        this.name = RainbowStorageOperationName;
        this.friendlyName = friendlyName;
        this.url = url;
    }

    @Override
    public IComponent getComponent() {
        return bolt;
    }

    @Override
    public String id() {
        return this.friendlyName;
    }

    @Override
    public void apply() {
        // Our print operator is an exeption. Since we don't reuse it we override its id with the name
        bolt = new RainbowStorageBolt(friendlyName, url);
    }

    @Override
    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        boltDeclarer = topologyBuilder.setBolt(id, bolt);
        this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::localOrShuffleGrouping);
    }
}