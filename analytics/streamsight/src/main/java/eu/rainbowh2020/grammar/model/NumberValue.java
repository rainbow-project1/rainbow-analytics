package eu.rainbowh2020.grammar.model;

public class NumberValue extends Value {
    private Number value;
    public NumberValue(){
    }

    public NumberValue(Number value){
        this.value = value;
    }

    @Override
    public Number getValue() {
        return value;
    }

    public void setValue(Number value) {
        this.value = value;
    }
}
