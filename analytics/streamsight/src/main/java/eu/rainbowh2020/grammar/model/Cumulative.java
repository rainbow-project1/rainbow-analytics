package eu.rainbowh2020.grammar.model;

public class Cumulative extends Operation {
    private String function;
    private MetricStream stream;

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public MetricStream getStream() {
        return stream;
    }

    public void setStream(MetricStream stream) {
        this.stream = stream;
    }
}
