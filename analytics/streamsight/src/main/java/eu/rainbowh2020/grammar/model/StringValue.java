package eu.rainbowh2020.grammar.model;

public class StringValue extends Value {
    public StringValue(){}
    public StringValue(String value){this.value = value;}
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String value;
}
