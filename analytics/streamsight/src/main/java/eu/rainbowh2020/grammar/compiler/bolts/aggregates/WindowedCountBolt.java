package eu.rainbowh2020.grammar.compiler.bolts.aggregates;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.List;

public class WindowedCountBolt extends ScheduledWindowedBolt {
    private String field;

    public WindowedCountBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        List<Tuple> tuples = inputWindow.get();
        int count = 0;

        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field)) {
                count += 1;
            }
        }
        super.setValues(new Values(count));
        super.execute(inputWindow);
    }

}
