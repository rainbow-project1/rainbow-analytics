package eu.rainbowh2020.grammar.compiler.bolts;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseWindowedBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class ScheduledWindowedBolt extends BaseWindowedBolt {
    public static String MODE = "disabled";
    private static final String ENABLED = "enabled";
    private OutputCollector collector;
    private Map<String, Object> conf;
    private String id;
    private String forwardBoltId = "";
    private TreeMap<Double, Integer> fractions = new TreeMap<>();
    private List<Integer> taskIds = new ArrayList<>();
    private Values values;

    public ScheduledWindowedBolt(String id) {
        this.id = id;
    }

    public void setFractions(TreeMap<Double, Integer> fractions) {
        this.fractions = fractions;
    }

    public void setForwardBoltId(String forwardBoltId) {
        this.forwardBoltId = forwardBoltId;
    }

    public void setValues(Values values) {
        this.values = values;
    }

    public Map<String, Object> getConf() {
        return conf;
    }

    public String getId() {
        return id;
    }

    public static boolean isSchedulerModeEnabled() {
        return ENABLED.equals(MODE);
    }

    @Override
    public void execute(TupleWindow tupleWindow) {
        List<Tuple> tuples = tupleWindow.get();
        if (ENABLED.equals(MODE) && !"".equals(forwardBoltId)) {
            Random random = new Random();
            double randomValue = random.nextDouble() * 100.0;
            collector.emitDirect(this.taskIds.get(fractions.higherEntry(randomValue).getValue()), tuples, values);
        } else {
            collector.emit(tuples, values);
        }
    }

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext topologyContext, OutputCollector collector) {
        this.collector = collector;
        this.conf = topoConf;
        if (ENABLED.equals(MODE) && !"".equals(forwardBoltId)) {
            this.taskIds.addAll(topologyContext.getComponentTasks(forwardBoltId)
                    .stream()
                    .sorted()
                    .collect(Collectors.toList()));
            Collections.sort(this.taskIds);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(this.id));
    }
}
