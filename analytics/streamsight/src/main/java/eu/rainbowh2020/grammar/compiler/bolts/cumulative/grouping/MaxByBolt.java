package eu.rainbowh2020.grammar.compiler.bolts.cumulative.grouping;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MaxByBolt extends ScheduledBolt {
    private String field;
    private String groupByField;
    private Map<String, Double> groupByValues = new HashMap<>();

    public MaxByBolt(String field, String groupByField, String id) {
        super(id);
        this.field = field;
        this.groupByField = groupByField;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field) && tuple.contains(this.groupByField)) {
            checkValue((double) tuple.getValueByField(this.field),
                    (String) tuple.getValueByField(this.groupByField));
            String result = groupByValues.entrySet().stream().map(e -> e.getKey() + ":" + e.getValue()).collect(Collectors.joining(" "));
            super.setValues(new Values(result));
            super.execute(tuple);
        }
    }

    private void checkValue(double value, String key) {
        if (!groupByValues.containsKey(key)) {
            groupByValues.put(key, value);
        } else if (value > groupByValues.get(key)) {
            groupByValues.put(key, value);
        }
    }

}
