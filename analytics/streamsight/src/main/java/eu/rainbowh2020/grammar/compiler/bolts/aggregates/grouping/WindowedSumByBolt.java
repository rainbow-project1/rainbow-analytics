package eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WindowedSumByBolt extends ScheduledWindowedBolt {
    private String field;
    private String groupByField;

    public WindowedSumByBolt(String field, String groupByField, String id) {
        super(id);
        this.field = field;
        this.groupByField = groupByField;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        Map<String, Double> groupByValues = new HashMap<>();
        List<Tuple> tuples = inputWindow.get();
        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field) && tuple.contains(this.groupByField)) {
                addToKey((double) tuple.getValueByField(this.field),
                        (String) tuple.getValueByField(this.groupByField),
                        groupByValues);
            }
        }
        String result = groupByValues.entrySet().stream().map(e -> e.getKey() + ":" + e.getValue()).collect(Collectors.joining(" "));
        super.setValues(new Values(result));
        super.execute(inputWindow);
    }

    private void addToKey(double value, String key, Map<String, Double> groupByValues) {
        if (!groupByValues.containsKey(key)) {
            groupByValues.put(key, value);
        } else {
            groupByValues.put(key, groupByValues.get(key) + value);
        }
    }

}
