package eu.rainbowh2020.grammar.compiler.operations.kafkahelpers;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class KafkaPublisher implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(KafkaPublisher.class);
    private final Properties props = new Properties();
    private Producer<String, Double> producer;
    private String topic;

    public KafkaPublisher(String host, String topic){
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, host );
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaExampleProducer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 1);
        /**
         * The producer groups together any records that arrive in between request transmissions into a single batched
         * request. Normally this occurs only under load when records arrive faster than they can be sent out.
         * However in some circumstances the client may want to reduce the number of requests even under moderate load.
         * This setting accomplishes this by adding a small amount of artificial delay—that is, rather than immediately
         * sending out a record the producer will wait for up to the given delay to allow other records to be sent so
         * that the sends can be batched together. This can be thought of as analogous to Nagle's algorithm in TCP.
         * This setting gives the upper bound on the delay for batching: once we get batch.size worth of records for a partition it will be sent immediately regardless of this setting, however if we have fewer than this many bytes accumulated for this partition we will 'linger' for the specified time waiting for more records to show up. This setting defaults to 0 (i.e. no delay). Setting linger.ms=5, for example, would have the effect of reducing the number of requests sent but would add up to 5ms of latency to records sent in the absence of load.
         */
        props.put(ProducerConfig.LINGER_MS_CONFIG, 1000);
        props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, 5000);
        this.topic = topic;

    }

    public void openConnection(){
        this.producer = new KafkaProducer<>(props);
    }

    public void publish(KafkaRecord res) throws ExecutionException, InterruptedException {
        ProducerRecord<String, Double> r =
                new ProducerRecord(this.topic, "", res.toJson());
        Future<RecordMetadata> y = this.producer.send(r);
        y.get();
    }

    public void closeConnection(){
        this.producer.close();
    }
}