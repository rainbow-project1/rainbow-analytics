package eu.rainbowh2020.grammar.compiler.bolts.cumulative.grouping;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AverageByBolt extends ScheduledBolt {
    private String field;
    private String groupByField;

    private Map<String, Double> mapSum = new HashMap<>();
    private Map<String, Integer> mapCount = new HashMap<>();

    public AverageByBolt(String field, String groupByField, String id) {
        super(id);
        this.field = field;
        this.groupByField = groupByField;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field) && tuple.contains(this.groupByField)) {
            addToKey((double) tuple.getValueByField(this.field),
                    (String) tuple.getValueByField(this.groupByField));
            String result = mapCount.entrySet()
                    .stream()
                    .map(e -> e.getKey() + ":" + (e.getValue() == 0 ? 0 : mapSum.get(e.getKey()) / e.getValue()))
                    .collect(Collectors.joining(" "));
            super.setValues(new Values(result));
            super.execute(tuple);
        }
    }

    private void addToKey(double value, String key) {
        if (!mapSum.containsKey(key)) {
            mapSum.put(key, value);
            mapCount.put(key, 1);
        } else {
            mapSum.put(key, mapSum.get(key) + value);
            mapCount.put(key, mapCount.get(key) + 1);
        }
    }

}
