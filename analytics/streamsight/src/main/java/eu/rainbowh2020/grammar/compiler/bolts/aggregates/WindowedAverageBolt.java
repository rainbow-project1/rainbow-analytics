package eu.rainbowh2020.grammar.compiler.bolts.aggregates;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.List;

public class WindowedAverageBolt extends ScheduledWindowedBolt {
    private String field;

    public WindowedAverageBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        List<Tuple> tuples = inputWindow.get();
        double sum = 0;
        int count = 0;
        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field)) {
                sum += (double) tuple.getValueByField(this.field);
                count++;
            }
        }
        super.setValues(new Values(count == 0 ? 0 : sum / count));
        super.execute(inputWindow);
    }

}