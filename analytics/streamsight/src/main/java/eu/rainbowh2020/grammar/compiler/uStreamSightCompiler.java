package eu.rainbowh2020.grammar.compiler;

import eu.rainbowh2020.grammar.compiler.operations.ArithmeticDoubleOperation;
import eu.rainbowh2020.grammar.compiler.operations.ArithmeticOperation;
import eu.rainbowh2020.grammar.compiler.operations.CumulativeOperation;
import eu.rainbowh2020.grammar.compiler.operations.FilteredDoubleOperation;
import eu.rainbowh2020.grammar.compiler.operations.FilteredOperation;
import eu.rainbowh2020.grammar.compiler.operations.GroupingOperation;
import eu.rainbowh2020.grammar.compiler.operations.IgniteStreamOperation;
import eu.rainbowh2020.grammar.compiler.operations.InfluxOperation;
import eu.rainbowh2020.grammar.compiler.operations.LocalStreamOperation;
import eu.rainbowh2020.grammar.compiler.operations.Operation;
import eu.rainbowh2020.grammar.compiler.operations.PrintOperation;
import eu.rainbowh2020.grammar.compiler.operations.RainbowStorageOperation;
import eu.rainbowh2020.grammar.compiler.operations.SelectOperation;
import eu.rainbowh2020.grammar.compiler.operations.WindowOperation;
import eu.rainbowh2020.grammar.model.AddOp;
import eu.rainbowh2020.grammar.model.Composition;
import eu.rainbowh2020.grammar.model.Cumulative;
import eu.rainbowh2020.grammar.model.DoubleValue;
import eu.rainbowh2020.grammar.model.Expression;
import eu.rainbowh2020.grammar.model.FilteredComposition;
import eu.rainbowh2020.grammar.model.GroupingComposition;
import eu.rainbowh2020.grammar.model.InsightDefinition;
import eu.rainbowh2020.grammar.model.IntervalComposition;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import eu.rainbowh2020.grammar.model.Membership;
import eu.rainbowh2020.grammar.model.MetricStream;
import eu.rainbowh2020.grammar.model.MulOp;
import eu.rainbowh2020.grammar.model.StreamDefinition;
import eu.rainbowh2020.grammar.model.StreamProvider;
import eu.rainbowh2020.grammar.model.TimePeriod;
import eu.rainbowh2020.grammar.model.WindowAggregate;
import org.apache.storm.generated.Bolt;
import org.apache.storm.generated.StormTopology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static eu.rainbowh2020.grammar.compiler.operations.InfluxOperation.InfluxOperationName;
import static eu.rainbowh2020.grammar.compiler.operations.PrintOperation.PrintOperationName;
import static eu.rainbowh2020.grammar.compiler.operations.RainbowStorageOperation.RainbowStorageOperationName;
import static eu.rainbowh2020.grammar.model.TimePeriod.create;
import static java.util.concurrent.TimeUnit.SECONDS;

public class uStreamSightCompiler {
    private static final Logger logger = LoggerFactory.getLogger(uStreamSightCompiler.class);

    private Map<String, ArrayList<Operation>> inputStreams;
    private Map<String,Operation> cachedInsights;
    private LinkedHashMap<String,Operation> operations;

    public void load(Map<String, StreamDefinition> streamDefinitions,
                     Map<String, InsightDefinition> insightDefinitions,
                     String outputMode, String providerHosts, String providerPort,
                     String consumerHost, String consumerPort) throws Exception {
        operations = new LinkedHashMap<>();
        inputStreams = new HashMap<>();
        cachedInsights = new HashMap<>();

        compileStreams(streamDefinitions, providerHosts, providerPort);
        compileInsights(insightDefinitions, outputMode, consumerHost, consumerPort);

        System.out.println("Operations to be applied:");
        for (Map.Entry<String, Operation> operation : operations.entrySet()) {
            String id = operation.getKey();
            System.out.println("\t" + id);
            operation.getValue().apply();
        }
    }


    private void compileStreams(Map<String, StreamDefinition> streamDefinitions, String providerHosts, String providerPort) {

        for (StreamDefinition streamDefinition : streamDefinitions.values()) {
            ArrayList<Operation> streamOperations = new ArrayList<>();
            StreamProvider provider = streamDefinition.getProvider();
            String providerName = provider.getName();
            KeyValueParams properties = provider.getProperties();
            String streamName = streamDefinition.getName();

            if (providerName.equalsIgnoreCase("local")) {
                Operation streamOperation = new LocalStreamOperation(streamDefinition.getName(), properties);
                String id = streamOperation.id();
                if (operations.containsKey(id)) {
                    logger.info("Operation: " + id + " already exists and will be re-used");
                } else {
                    operations.put(id, streamOperation);
                }
                streamOperations.add(operations.get(id));
                inputStreams.put(streamName, streamOperations);
            }

            if (providerName.equalsIgnoreCase("storageLayer")) {
                System.out.println("--------------------------------------------------");
                System.out.println(properties);
                String[] hosts = providerHosts.split(",");
                for (String host : hosts) {
                    Operation streamOperation = new IgniteStreamOperation(streamDefinition.getName(), host.concat(":").concat(providerPort), properties);
                    String id = streamOperation.id();
                    if (operations.containsKey(id)) {
                        logger.info("Operation: " + id + " already exists and will be re-used");
                    } else {
                        operations.put(id, streamOperation);
                    }
                    streamOperations.add(operations.get(id));
                }
                inputStreams.put(streamName, streamOperations);
            } else {
                logger.info("Currently provider: " + providerName + " is not supported");
            }
        }
    }

    private void compileInsights(Map<String, InsightDefinition> insightDefinitions, String outputMode, String consumerHost, String consumerPort) throws Exception {
        for (InsightDefinition insightDefinition : insightDefinitions.values()) {
            Composition composition = insightDefinition.getComposition();
            String id = "";
            if (composition instanceof IntervalComposition) {
                id = compileIntervalComposition((IntervalComposition) composition);
            } else if (composition instanceof FilteredComposition) {
                id = compileFilteredComposition((FilteredComposition) composition);
            } else if (composition instanceof GroupingComposition) {
                id = compileGroupingComposition((GroupingComposition) composition);
            } else if (composition instanceof  Expression) {
                id = compileExpression((Expression) composition);
            }

            // This is the final operation of an insight
            String insightName = insightDefinition.getName();
            Operation outputOperation;
            if (PrintOperationName.equals(outputMode)) {
                outputOperation = new PrintOperation(insightName);
            } else if (RainbowStorageOperationName.equals(outputMode)) {
                outputOperation = new RainbowStorageOperation(insightName, consumerHost.concat(":").concat(consumerPort));
            } else if (InfluxOperationName.equals(outputMode)) {
                outputOperation = new InfluxOperation(insightName, consumerHost.concat(":").concat(consumerPort));
            } else {
                throw new UnsupportedOperationException("Invalid operation " + outputMode);
            }
            outputOperation.addInputSource(operations.get(id));
            //If we don't want to reuse insight's output we use insightName as the ID of the operation
            // otherwise we could replace insightName with printOperation.id() like:
            //      operations.putIfAbsent(insightName,printOperation);
            // This means that only the first insight gets to output
            operations.putIfAbsent(insightName, outputOperation);
            //We need to a mapping of its name to the actual operation
            cachedInsights.put(insightName,operations.get(id));

        }
    }

    private String compileFilteredComposition(FilteredComposition composition) throws Exception {
        Expression filteredExpression = composition.getFilteredExpression();
        String operation = composition.getOperator();
        String inputId = compileExpression(composition.getExpression());
        if (filteredExpression instanceof DoubleValue) {
            DoubleValue doubleValue = (DoubleValue) filteredExpression;
            FilteredDoubleOperation filteredDoubleOperation = new FilteredDoubleOperation(operation, doubleValue.getValue());
            filteredDoubleOperation.addInputSource(operations.get(inputId));
            operations.get(inputId).setOutputSource(filteredDoubleOperation);
            operations.putIfAbsent(filteredDoubleOperation.id(), filteredDoubleOperation);
            return filteredDoubleOperation.id();
        } else if (filteredExpression instanceof AddOp
                || filteredExpression instanceof MulOp
                || filteredExpression instanceof eu.rainbowh2020.grammar.model.Operation) {
            String filterId = compileExpression(filteredExpression);
            FilteredOperation filteredOperation = new FilteredOperation(operation, inputId, filterId);
            filteredOperation.addInputSource(operations.get(inputId));
            filteredOperation.addInputSource(operations.get(filterId));
            operations.get(inputId).setOutputSource(filteredOperation);
            operations.get(filterId).setOutputSource(filteredOperation);
            operations.putIfAbsent(filteredOperation.id(), filteredOperation);
            return filteredOperation.id();
        } else {
            throw new UnsupportedOperationException("Cannot recognize operation " + filteredExpression);
        }
    }

    private String compileGroupingComposition(GroupingComposition composition) throws Exception {
        WindowAggregate windowAggregate = composition.getWindowAggregate();
        String groupByField = composition.getGroupByField();
        TimePeriod interval = composition.getInterval();
        TimePeriod window = windowAggregate.getWindow();
        String operation = windowAggregate.getFunction();

        MetricStream stream = windowAggregate.getStream();
        String field = stream.getMetric();
        String id = compileMetricStream(stream, groupByField);

        GroupingOperation groupingOperation = new GroupingOperation(operation, field, groupByField, window, interval);

        groupingOperation.addInputSource(operations.get(id));
        operations.get(id).setOutputSource(groupingOperation);
        operations.putIfAbsent(groupingOperation.id(), groupingOperation);
        return groupingOperation.id();
    }

    private String compileIntervalComposition(IntervalComposition composition) throws Exception {
        TimePeriod interval = composition.getInterval();
        return compileExpression(composition.getExpression(), interval);
    }

    private String compileExpression(Expression expression) throws Exception {
        return compileExpression(expression, create(1, SECONDS.toString()));
    }

    private String compileExpression(Expression expression, TimePeriod interval) throws Exception {
        String idLeft;
        String idRight;
        if (expression instanceof AddOp) {
            AddOp addOp = (AddOp) expression;
            String operation = addOp.getOp();
            if (checkIfDoubleChild(addOp.getLeft())) {
                idRight = compileExpression(addOp.getRight(), interval);
                Double value = compileDoubleValue(addOp.getLeft());
                return (compileDoubleOperation(idRight, operation, value, 'L'));
            } else if (checkIfDoubleChild(addOp.getRight())) {
                idLeft = compileExpression(addOp.getLeft(), interval);
                Double value = compileDoubleValue(addOp.getRight());
                return (compileDoubleOperation(idLeft, operation, value, 'R'));
            } else {
                idRight = compileExpression(addOp.getRight(), interval);
                idLeft = compileExpression(addOp.getLeft(), interval);
                return (compileArithmeticOperation(idLeft, idRight, operation));
            }
        } else if (expression instanceof MulOp) {
            MulOp mulOp = (MulOp) expression;
            String operation = mulOp.getOp();
            if (checkIfDoubleChild(mulOp.getLeft())) {
                idRight = compileExpression(mulOp.getRight(), interval);
                Double value = compileDoubleValue(mulOp.getLeft());
                return (compileDoubleOperation(idRight, operation, value, 'L'));
            } else if (checkIfDoubleChild(mulOp.getRight())) {
                idLeft = compileExpression(mulOp.getLeft(), interval);
                Double value = compileDoubleValue(mulOp.getRight());
                return (compileDoubleOperation(idLeft, operation, value, 'R'));
            } else {
                idRight = compileExpression(mulOp.getRight(), interval);
                idLeft = compileExpression(mulOp.getLeft(), interval);
                return (compileArithmeticOperation(idLeft, idRight, operation));
            }
        } else if (expression instanceof eu.rainbowh2020.grammar.model.Operation) {
            return compileOperation((eu.rainbowh2020.grammar.model.Operation) expression, interval);
        } else {
            throw new UnsupportedOperationException("Unknown type of expression");
        }
    }

    private String compileArithmeticOperation(String leftId, String rightId, String operation) {
        ArithmeticOperation arithmeticOperation = new ArithmeticOperation(leftId, rightId, operation);
        arithmeticOperation.addInputSource(operations.get(leftId));
        arithmeticOperation.addInputSource(operations.get(rightId));
        operations.get(leftId).setOutputSource(arithmeticOperation);
        operations.get(rightId).setOutputSource(arithmeticOperation);
        operations.putIfAbsent(arithmeticOperation.id(), arithmeticOperation);
        return arithmeticOperation.id();
    }

    private String compileDoubleOperation(String id, String operation, Double value, Character position) {
        ArithmeticDoubleOperation arithmeticDoubleOperation = new ArithmeticDoubleOperation(operation, value, position);
        arithmeticDoubleOperation.addInputSource(operations.get(id));
        operations.get(id).setOutputSource(arithmeticDoubleOperation);
        operations.putIfAbsent(arithmeticDoubleOperation.id(), arithmeticDoubleOperation);
        return arithmeticDoubleOperation.id();
    }

    private Double compileDoubleValue(Expression expression) {
        return ((DoubleValue) expression).getValue();
    }

    private String compileOperation(eu.rainbowh2020.grammar.model.Operation operation, TimePeriod interval) throws Exception {
        if (operation instanceof MetricStream) {
            return (compileMetricStream((MetricStream) operation, ""));
        } else if (operation instanceof WindowAggregate) {
            return (compileWindowAggregate((WindowAggregate) operation, interval));
        } else if (operation instanceof Cumulative) {
            return (compileCumulative((Cumulative) operation));
        } else {
            throw new UnsupportedOperationException("Unknown operation type");
        }
    }

    private boolean checkIfDoubleChild(Expression expression) {
        return expression instanceof DoubleValue ? true : false;
    }

    private String compileCumulative(Cumulative composition) throws Exception {
        String function = composition.getFunction();
        MetricStream stream = composition.getStream();
        String field = stream.getMetric();
        String id = compileMetricStream(stream, "");
        CumulativeOperation cumulativeOperation = new CumulativeOperation(field, function);
        cumulativeOperation.addInputSource(operations.get(id));
        operations.get(id).setOutputSource(cumulativeOperation);
        operations.putIfAbsent(cumulativeOperation.id(), cumulativeOperation);
        return cumulativeOperation.id();
    }

    private String compileWindowAggregate(WindowAggregate composition, TimePeriod interval) throws Exception {
        String function = composition.getFunction();
        MetricStream stream = composition.getStream();
        String field = stream.getMetric();
        String id = compileMetricStream(stream, "");
        TimePeriod window = composition.getWindow();
        WindowOperation windowOperation = new WindowOperation(function, field, window, interval);
        windowOperation.addInputSource(operations.get(id));
        operations.get(id).setOutputSource(windowOperation);
        operations.putIfAbsent(windowOperation.id(), windowOperation);
        return windowOperation.id();
    }

    /**
     * Return back a unique id of the operation
     * @param composition
     * @return
     */
    private String compileMetricStream(MetricStream composition, String groupByField) throws Exception {
        Membership membership = composition.getMembership();

        //Sorted operations. we need to preserve the order of inputs since the form a unique id
        TreeSet<Operation> subscribeToOperations = new TreeSet<>();
        for(String streamID: membership){
            if(inputStreams.containsKey(streamID)){
                inputStreams.get(streamID).forEach(subscribeToOperations::add);
            }else{
                //We should check if it references an existing insight
                Operation actualOperation = cachedInsights.get(streamID);
                if(actualOperation==null){
                    throw new Exception("Insight: "+streamID+" doesn't exist");
                }else{
                    subscribeToOperations.add(actualOperation);
                }

            }
        }

        // Select operation
        String metric = composition.getMetric();

        SelectOperation selectOperation = "".equals(groupByField) ? new SelectOperation(metric) : new SelectOperation(metric, groupByField);
        Set<Operation> finalInputs = new TreeSet<>();
        for(Operation inputOperation: subscribeToOperations) {
            if(inputOperation instanceof SelectOperation) {
                boolean similar = checkIfSimilar(selectOperation, (SelectOperation) inputOperation);
                if (!similar) {
                    finalInputs.add(inputOperation);
                } else {
                    selectOperation = (SelectOperation) inputOperation;

                }
            }else{
                finalInputs.add(inputOperation);
            }
        }
        //If an input already exists we don't care since the underlying implementation is a set.
        finalInputs.stream().forEachOrdered(selectOperation::addInputSource);

        /**  equivalent to the above
        for(Operation op : finalInputs){
            selectOperation.addInputSource(op);
        }
         **/

        //subscribeToOperations.stream().forEachOrdered(selectOperation::addInputSource);
        // Add our operation in cache if not already exists
        operations.putIfAbsent(selectOperation.id(),selectOperation);
        return selectOperation.id();
    }

    private boolean checkIfSimilar(SelectOperation selectOperation, SelectOperation inputOperation) {
        if(!selectOperation.getName().equals(inputOperation.getName()))
            return false;
        Set<String> a =  selectOperation.getKeys();
        Set<String> b =  inputOperation.getKeys();
        if(!a.containsAll(b) || !b.containsAll(a))
            return false;

        return true;
    }

    public void debugPlan(StormTopology topology) {

        Map<String, Bolt> bolts = topology.get_bolts();
        bolts.entrySet().forEach(e->{
            String id = e.getKey();
            Bolt v = e.getValue();
            System.out.println("Operation:"+id);
            v.get_common().get_inputs().keySet().forEach(k->{
                System.out.println("\t"+k);
            });
            System.out.println();
        });

    }

    public LinkedHashMap<String, Operation> getOperations() {
        return operations;
    }
}
