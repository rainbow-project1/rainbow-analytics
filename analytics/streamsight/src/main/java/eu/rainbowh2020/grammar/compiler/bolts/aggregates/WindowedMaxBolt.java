package eu.rainbowh2020.grammar.compiler.bolts.aggregates;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.apache.storm.windowing.TupleWindow;

import java.util.List;

public class WindowedMaxBolt extends ScheduledWindowedBolt {
    private String field;

    public WindowedMaxBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(TupleWindow inputWindow) {
        double max = Double.MIN_VALUE;
        List<Tuple> tuples = inputWindow.get();

        for (Tuple tuple : tuples) {
            if (tuple.contains(this.field)) {
                Double value = (double) tuple.getValueByField(this.field);
                max = value > max ? value : max;
            }
        }
        super.setValues(new Values(max));
        super.execute(inputWindow);
    }

}
