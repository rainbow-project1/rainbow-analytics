package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.topology.BoltDeclarer;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.IWindowedBolt;
import org.apache.storm.topology.TopologyBuilder;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public abstract class Operation implements Comparable<Operation> {
    protected String name;
    String parameters = "";
    protected TreeSet<Operation> inputSources = new TreeSet();
    protected Operation outputSource;
    protected BoltDeclarer boltDeclarer;
    protected IComponent bolt;

    protected Operation() {
    }

    public void addInputSource(Operation operation) {
        this.inputSources.add(operation);
    }

    public void setOutputSource(Operation operation) {
        this.outputSource = operation;
    }

    public Operation getOutputSource() {
        return outputSource;
    }

    public abstract void apply() throws Exception;

    public String getName() {
        return name;
    }

    public IComponent getComponent() {
        return bolt;
    }

    public Set<Operation> getInputSources() {
        return inputSources;
    }

    public String id() {
        String id = this.name + "_" + parameters
                + "_" + inputSources.stream().map(Operation::id).sorted().collect(Collectors.joining(",", "[", "]"));
        return id;
    }

    public void declare(TopologyBuilder topologyBuilder) {
        String id = this.id();
        if (bolt instanceof IRichBolt) {
            boltDeclarer = topologyBuilder.setBolt(id, (IRichBolt) bolt);
        } else if (bolt instanceof IWindowedBolt) {
            boltDeclarer = topologyBuilder.setBolt(id, (IWindowedBolt) bolt);
        }
        if (!ScheduledBolt.isSchedulerModeEnabled()) {
            //Semantically this may be incorrect, since it doesn't reduce records to a single task and count them
            this.inputSources.stream().map(Operation::id).forEach(boltDeclarer::globalGrouping);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Operation)) return false;
        Operation operation = (Operation) o;
        return Objects.equals(getName(), operation.getName()) &&
                Objects.equals(parameters, operation.parameters) &&
                Objects.equals(getInputSources(), operation.getInputSources());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), parameters, getInputSources());
    }

    @Override
    public int compareTo(Operation operation) {
        return this.id().compareTo(operation.id());
    }

    @Override
    public String toString() {
        return id();
    }
}
