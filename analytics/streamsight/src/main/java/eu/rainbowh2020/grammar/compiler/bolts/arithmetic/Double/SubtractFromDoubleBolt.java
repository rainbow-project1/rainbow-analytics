package eu.rainbowh2020.grammar.compiler.bolts.arithmetic.Double;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class SubtractFromDoubleBolt extends ScheduledBolt {
    private double value;

    public SubtractFromDoubleBolt(double value, String id) {
        super(id);
        this.value = value;
    }

    @Override
    public void execute(Tuple tuple) {
        if (!tuple.getValues().isEmpty()) {
            super.setValues(new Values(this.value - (double) tuple.getValue(0)));
            super.execute(tuple);
        }
    }

}
