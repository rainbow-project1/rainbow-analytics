package eu.rainbowh2020.grammar;

import eu.rainbowh2020.grammar.model.AddOp;
import eu.rainbowh2020.grammar.model.Aggregate;
import eu.rainbowh2020.grammar.model.Composition;
import eu.rainbowh2020.grammar.model.Cumulative;
import eu.rainbowh2020.grammar.model.DoubleValue;
import eu.rainbowh2020.grammar.model.Expression;
import eu.rainbowh2020.grammar.model.FilteredComposition;
import eu.rainbowh2020.grammar.model.GroupingComposition;
import eu.rainbowh2020.grammar.model.InsightDefinition;
import eu.rainbowh2020.grammar.model.IntervalComposition;
import eu.rainbowh2020.grammar.model.KeyValuePair;
import eu.rainbowh2020.grammar.model.KeyValueParams;
import eu.rainbowh2020.grammar.model.Membership;
import eu.rainbowh2020.grammar.model.MetricStream;
import eu.rainbowh2020.grammar.model.MulOp;
import eu.rainbowh2020.grammar.model.NumberValue;
import eu.rainbowh2020.grammar.model.Operation;
import eu.rainbowh2020.grammar.model.StreamDefinition;
import eu.rainbowh2020.grammar.model.StreamProvider;
import eu.rainbowh2020.grammar.model.StringValue;
import eu.rainbowh2020.grammar.model.TimePeriod;
import eu.rainbowh2020.grammar.model.Value;
import eu.rainbowh2020.grammar.model.WindowAggregate;
import eu.rainbowh2020.uStreamSightLexer;
import eu.rainbowh2020.uStreamSightParser;
import eu.rainbowh2020.uStreamSightParser.AddopExprContext;
import eu.rainbowh2020.uStreamSightParser.DOUBLEContext;
import eu.rainbowh2020.uStreamSightParser.FilterExprContext;
import eu.rainbowh2020.uStreamSightParser.FilteredCompositionContext;
import eu.rainbowh2020.uStreamSightParser.GroupExprContext;
import eu.rainbowh2020.uStreamSightParser.GroupingCompositionContext;
import eu.rainbowh2020.uStreamSightParser.InterExprContext;
import eu.rainbowh2020.uStreamSightParser.IntervalCompositionContext;
import eu.rainbowh2020.uStreamSightParser.MulopExprContext;
import eu.rainbowh2020.uStreamSightParser.OpExprContext;
import eu.rainbowh2020.uStreamSightParser.OperationContext;
import eu.rainbowh2020.uStreamSightParserBaseVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static eu.rainbowh2020.uStreamSightParser.AggregateContext;
import static eu.rainbowh2020.uStreamSightParser.AggregateExprContext;
import static eu.rainbowh2020.uStreamSightParser.BasecaseExprContext;
import static eu.rainbowh2020.uStreamSightParser.CompositionContext;
import static eu.rainbowh2020.uStreamSightParser.CumulativeContext;
import static eu.rainbowh2020.uStreamSightParser.CumulativeExprContext;
import static eu.rainbowh2020.uStreamSightParser.ExpressionContext;
import static eu.rainbowh2020.uStreamSightParser.FunctionContext;
import static eu.rainbowh2020.uStreamSightParser.GroupingContext;
import static eu.rainbowh2020.uStreamSightParser.InsightDefinitionContext;
import static eu.rainbowh2020.uStreamSightParser.InsightDefinitionExprContext;
import static eu.rainbowh2020.uStreamSightParser.IntervalContext;
import static eu.rainbowh2020.uStreamSightParser.KeyValuePairContext;
import static eu.rainbowh2020.uStreamSightParser.KeyValueParamsContext;
import static eu.rainbowh2020.uStreamSightParser.MemberContext;
import static eu.rainbowh2020.uStreamSightParser.MembershipContext;
import static eu.rainbowh2020.uStreamSightParser.MetricContext;
import static eu.rainbowh2020.uStreamSightParser.MetricStreamContext;
import static eu.rainbowh2020.uStreamSightParser.MetricStreamExprContext;
import static eu.rainbowh2020.uStreamSightParser.StatementContext;
import static eu.rainbowh2020.uStreamSightParser.StatementsContext;
import static eu.rainbowh2020.uStreamSightParser.StreamDefinitionContext;
import static eu.rainbowh2020.uStreamSightParser.StreamDefinitionExprContext;
import static eu.rainbowh2020.uStreamSightParser.StreamIdContext;
import static eu.rainbowh2020.uStreamSightParser.StreamProviderContext;
import static eu.rainbowh2020.uStreamSightParser.TimeperiodContext;
import static eu.rainbowh2020.uStreamSightParser.ValueContext;
import static eu.rainbowh2020.uStreamSightParser.WindowContext;
import static eu.rainbowh2020.uStreamSightParser.WindowedAggrExprContext;
import static eu.rainbowh2020.uStreamSightParser.WindowedFunctionContext;
import static java.util.concurrent.TimeUnit.SECONDS;

public class GrammarTranslator extends uStreamSightParserBaseVisitor{

    private static final Logger logger = Logger.getLogger(GrammarTranslator.class);

    // Intermediate models.
    private Map<String, StreamDefinition> streamDefinitions = new HashMap<>();
    private Map<String, InsightDefinition> insightDefinitions = new TreeMap<>();

    public Object parse(InputStream input) throws Exception {
        CharStream charStream = CharStreams.fromStream(input);
        return parse(charStream);
    }
    public Object parse(String input) throws Exception {
        CharStream charStream = CharStreams.fromString(input);
        return parse(charStream);
    }
    private Object parse(CharStream charStream) throws Exception{
        uStreamSightLexer lexer = new uStreamSightLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        uStreamSightParser parser = new uStreamSightParser(tokens);

        //StreamSightASTTranslator streamSightASTListener = new StreamSightASTTranslator(exceptionHandler);
        //parser.addErrorListener(new SyntaxErrorHandler(exceptionHandler));
        //String msg = exceptionHandler.getMsg();
        //if(msg!=null)
         //   throw new Exception(msg);

        ParseTree tree = parser.statements();
        System.out.println(tree.toStringTree(parser));

        this.visit(tree);
        //msg = exceptionHandler.getMsg();
        //if(msg!=null)
         //   throw new Exception(msg);

        // parser.insights().enterRule(streamSightASTListener);
        //return grammarParser.getResult();
        return null;
    }

    public Map<String, StreamDefinition> getStreamDefinitions() {
        return streamDefinitions;
    }

    public Map<String, InsightDefinition> getInsightDefinitions() {
        return insightDefinitions;
    }

    @Override
    public Object visitStatements(StatementsContext ctx) {
        for(StatementContext statementContext: ctx.statement()){
            if(statementContext instanceof StreamDefinitionExprContext){
                StreamDefinition streamDefinition = visitStreamDefinition(((StreamDefinitionExprContext) statementContext).streamDefinition());
                this.streamDefinitions.put(streamDefinition.getName(),streamDefinition);
            }else if (statementContext instanceof  InsightDefinitionExprContext){
                InsightDefinition insightDefinition = visitInsightDefinition((((InsightDefinitionExprContext) statementContext).insightDefinition()));
                if(this.insightDefinitions.containsKey(insightDefinition.getName())){
                    logger.info(insightDefinition.getName()+" already declared!");
                }else {
                    this.insightDefinitions.put(insightDefinition.getName(), insightDefinition);
                }
            }else{
                logger.info("Unsupported Statement");
            }

        }
        return null;
    }

    @Override
    public StreamDefinition visitStreamDefinition(StreamDefinitionContext ctx) {
        StreamDefinition streamDefinition = new StreamDefinition();

        String id = visitStreamId(ctx.streamId());
        streamDefinition.setName(id);

        StreamProvider provider = visitStreamProvider(ctx.streamProvider());
        streamDefinition.setProvider(provider);

        return streamDefinition;
    }

    @Override
    public String visitStreamId(StreamIdContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public StreamProvider visitStreamProvider(StreamProviderContext ctx) {
        StreamProvider provider = new StreamProvider();
        String name = ctx.ID().getText();
        provider.setName(name);

        KeyValueParams keyValueParams = visitKeyValueParams(ctx.keyValueParams());
        provider.setProperties(keyValueParams);

        return provider;
    }

    @Override
    public KeyValueParams visitKeyValueParams(KeyValueParamsContext ctx) {
        KeyValueParams params = new KeyValueParams();
        if(ctx==null)
            return params;
        for(KeyValuePairContext pair :ctx.keyValuePair()){
            KeyValuePair keyValuePair = visitKeyValuePair(pair);
            params.put(keyValuePair.getKey(),keyValuePair.getValue());
        }
        return params;
    }

    @Override
    public KeyValuePair visitKeyValuePair(KeyValuePairContext ctx) {
        KeyValuePair pair = new KeyValuePair();
        String key = ctx.ID().getText();
        pair.setKey(key);
        Value value = visitValue(ctx.value());
        pair.setValue(value);
        return pair;
    }

    @Override
    public Value visitValue(ValueContext ctx) {
        if( ctx.INTEGER() !=null) {
           String number = ctx.INTEGER().getText();
           NumberValue value = new NumberValue();
           value.setValue(Integer.parseInt(number));
           return value;
       } else if( ctx.FLOAT() !=null){
           String number = ctx.FLOAT().getText();
           NumberValue value = new NumberValue();
           value.setValue(Float.parseFloat(number));
           return value;
       }else if(ctx.STRING()!=null){
            StringValue value = new StringValue();
           value.setValue(ctx.STRING().getText().replaceAll("\"",""));
           return value;
       }else{
            logger.info("Value not supported");
           return null;
       }

    }

    @Override
    public InsightDefinition visitInsightDefinition(InsightDefinitionContext ctx) {
        InsightDefinition insightDefinition = new InsightDefinition();
        String name = ctx.insightId().ID().getText();
        insightDefinition.setName(name);

        CompositionContext compositionCtx = ctx.composition();
        Composition composition = null;
        if (compositionCtx instanceof BasecaseExprContext) {
            composition = visitBasecaseExpr((BasecaseExprContext) compositionCtx);
        } else if (compositionCtx instanceof InterExprContext) {
            composition = visitInterExpr((InterExprContext) compositionCtx);
        } else if (compositionCtx instanceof FilterExprContext) {
            composition = visitFilterExpr((FilterExprContext) compositionCtx);
        } else if (compositionCtx instanceof GroupExprContext) {
            composition = visitGroupExpr((GroupExprContext) compositionCtx);
        }
        insightDefinition.setComposition(composition);
        return insightDefinition;
    }

    @Override
    public IntervalComposition visitInterExpr(InterExprContext interExprContext) {
        IntervalCompositionContext ctx = interExprContext.intervalComposition();
        IntervalComposition intervalComposition = new IntervalComposition();
        intervalComposition.setInterval(visitInterval(ctx.interval()));
        intervalComposition.setExpression(visitExpression(ctx.expression()));
        return intervalComposition;
    }

    @Override
    public FilteredComposition visitFilterExpr(FilterExprContext filterExprContext) {
        FilteredCompositionContext ctx = filterExprContext.filteredComposition();
        FilteredComposition filteredComposition = new FilteredComposition();
        filteredComposition.setOperator(ctx.OPERATOR().getText());
        filteredComposition.setExpression(visitExpression(ctx.expression(0)));
        filteredComposition.setFilteredExpression(visitExpression(ctx.expression(1)));
        return filteredComposition;
    }

    @Override
    public GroupingComposition visitGroupExpr(GroupExprContext groupExprContext) {
        GroupingCompositionContext ctx = groupExprContext.groupingComposition();
        GroupingComposition groupingComposition = new GroupingComposition();
        groupingComposition.setGroupByField(visitGrouping(ctx.grouping()));
        groupingComposition.setWindowAggregate(visitWindowedAggrExpr((WindowedAggrExprContext) ctx.aggregate()));
        groupingComposition.setInterval(visitInterval(ctx.interval()));
        return groupingComposition;
    }

    @Override
    public Expression visitBasecaseExpr(BasecaseExprContext ctx) {
        return visitExpression(ctx.expression());
    }

    public Expression visitExpression(ExpressionContext ctx) {
        Expression expression = null;
        if (ctx instanceof MulopExprContext) {
            expression = visitMulopExpr((MulopExprContext) ctx);
        } else if (ctx instanceof AddopExprContext) {
            expression = visitAddopExpr((AddopExprContext) ctx);
        } else if (ctx instanceof OpExprContext) {
            expression = visitOpExpr(((OpExprContext) ctx));
        } else if (ctx instanceof DOUBLEContext) {
            expression = visitDOUBLE((DOUBLEContext) ctx);
        }
        return expression;
    }

    @Override
    public Operation visitOpExpr(OpExprContext opExprContext) {
        return (visitOperation(opExprContext.operation()));
    }

    public Operation visitOperation(OperationContext ctx) {
        Operation operation = null;
        if (ctx instanceof MetricStreamExprContext) {
            operation = visitMetricStreamExpr((MetricStreamExprContext) ctx);
        } else if (ctx instanceof AggregateExprContext) {
            operation = visitAggregateExpr((AggregateExprContext) ctx);
        } else if (ctx instanceof CumulativeExprContext) {
            operation = visitCumulativeExpr((CumulativeExprContext) ctx);
        }
        return operation;
    }

    @Override
    public MulOp visitMulopExpr(MulopExprContext ctx) {
        MulOp mulOp = new MulOp();
        mulOp.setOp(ctx.MULOP().getText());
        mulOp.setLeft((Expression) visit(ctx.expression(0)));
        mulOp.setRight((Expression) visit(ctx.expression(1)));
        return mulOp;
    }

    @Override
    public AddOp visitAddopExpr(AddopExprContext ctx) {
        AddOp addOp = new AddOp();
        addOp.setOp(ctx.ADDOP().getText());
        addOp.setLeft((Expression) visit(ctx.expression(0)));
        addOp.setRight((Expression) visit(ctx.expression(1)));
        return addOp;
    }

    @Override
    public DoubleValue visitDOUBLE(DOUBLEContext ctx) {
        DoubleValue doubleValue = new DoubleValue();
        doubleValue.setValue(Double.valueOf(ctx.FLOAT().getText()));
        return doubleValue;
    }

    @Override
    public Cumulative visitCumulativeExpr(CumulativeExprContext ctx) {
        return visitCumulative(ctx.cumulative());
    }

    @Override
    public Cumulative visitCumulative(CumulativeContext ctx) {
        Cumulative cumulative = new Cumulative();

        String function = visitFunction(ctx.function());
        cumulative.setFunction(function);

        MetricStream metricStream = visitMetricStream(ctx.metricStream());
        cumulative.setStream(metricStream);

        return cumulative;
    }

    @Override
    public MetricStream visitMetricStreamExpr(MetricStreamExprContext ctx) {
        return visitMetricStream(ctx.metricStream());
    }

    @Override
    public MetricStream visitMetricStream(MetricStreamContext ctx) {
        MetricStream metricStream = new MetricStream();

        String metric = visitMetric(ctx.metric());
        metricStream.setMetric(metric);

        Membership membership = visitMembership(ctx.membership());
        metricStream.setMembership(membership);

        return metricStream;
    }
    @Override
    public String visitMetric(MetricContext ctx) {
        return ctx.STRING().getText().replaceAll("\"","");
    }

    @Override
    public Membership visitMembership(MembershipContext ctx) {
        Membership membership = new Membership();
        for(MemberContext memberCtx : ctx.member()){
            String member = visitMember(memberCtx);
            membership.add(member);
        }
        return membership;
    }

    @Override
    public String visitMember(MemberContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public Aggregate visitAggregateExpr(AggregateExprContext ctx) {
        Aggregate aggregate = null;
        AggregateContext aggregateCtx = ctx.aggregate();

        if(aggregateCtx instanceof WindowedAggrExprContext){
              aggregate = visitWindowedAggrExpr((WindowedAggrExprContext) aggregateCtx);
        }

        return aggregate;
    }

    @Override
    public WindowAggregate visitWindowedAggrExpr(WindowedAggrExprContext ctx) {
        WindowAggregate windowAggregate = new WindowAggregate();

        String aggregateFunction = visitWindowedFunction(ctx.windowedFunction());
        windowAggregate.setFunction(aggregateFunction);

        MetricStream metricStream = visitMetricStream(ctx.metricStream());
        windowAggregate.setStream(metricStream);

        windowAggregate.setWindow(visitWindow(ctx.window()));
        return windowAggregate;
    }

    @Override
    public String visitFunction(FunctionContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public String visitWindowedFunction(WindowedFunctionContext ctx) {
        return ctx.ID().getText();
    }

    @Override
    public TimePeriod visitWindow(WindowContext ctx) {
        return  visitTimeperiod(ctx.timeperiod());
    }

    @Override
    public TimePeriod visitInterval(IntervalContext ctx) {
        if (ctx == null) {
            return TimePeriod.create(1,  SECONDS.toString());
        } else {
            return visitTimeperiod(ctx.timeperiod());
        }
    }

    @Override
    public String visitGrouping(GroupingContext ctx) {
        if (ctx != null) {
            return ctx.STRING().getText().replaceAll("\"", "");
        } else {
            return "";
        }
    }

    @Override
    public TimePeriod visitTimeperiod(TimeperiodContext ctx) {
        int duration = Integer.parseInt(ctx.INTEGER().getText());
        String period = ctx.TIME_PERIOD().getText();
        return TimePeriod.create(duration,period);
    }
}
