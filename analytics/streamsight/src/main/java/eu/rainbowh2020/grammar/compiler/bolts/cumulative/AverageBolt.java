package eu.rainbowh2020.grammar.compiler.bolts.cumulative;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class AverageBolt extends ScheduledBolt {
    private String field;
    private int count = 0;
    private double sum = 0.0;

    public AverageBolt(String field, String id) {
        super(id);
        this.field = field;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field)) {
            sum += (double) tuple.getValueByField(this.field);
            count++;
            super.setValues(new Values(count == 0 ? 0 : sum / count));
            super.execute(tuple);
        }
    }

}
