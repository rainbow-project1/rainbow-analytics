package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping.WindowedAverageByBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping.WindowedCountByBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping.WindowedMaxByBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping.WindowedMinByBolt;
import eu.rainbowh2020.grammar.compiler.bolts.aggregates.grouping.WindowedSumByBolt;
import eu.rainbowh2020.grammar.model.TimePeriod;
import org.apache.storm.topology.base.BaseWindowedBolt;

public class GroupingOperation extends Operation {
    private String field;
    private String groupByField;
    private String operation;
    private TimePeriod window;
    private TimePeriod interval;

    public GroupingOperation(String operation, String field, String groupByField, TimePeriod window, TimePeriod interval) {
        this.name = "G";
        this.window = window;
        this.interval = interval;
        this.field = field;
        this.groupByField = groupByField;
        this.operation = operation;
        this.parameters = operation + "(" + field + "," + groupByField + "," + window.toMillis() + "," + interval.toMillis() + ")";
    }

    @Override
    public void apply() throws Exception {
        String id = this.id();
        switch (this.operation) {
            case "count":
                bolt = new WindowedCountByBolt(this.field, this.groupByField, id);
                break;
            case "sum":
                bolt = new WindowedSumByBolt(this.field, this.groupByField, id);
                break;
            case "average":
            case "avg":
                //TODO Possible we can check if sum and count are already defined, so we can make use of them instead of
                // creating a new average operation
                bolt = new WindowedAverageByBolt(this.field, this.groupByField, id);
                break;
            case "maximum":
            case "max":
                bolt = new WindowedMaxByBolt(this.field, this.groupByField, id);
                break;
            case "minimum":
            case "min":
                bolt = new WindowedMinByBolt(this.field, this.groupByField, id);
                break;
            default:
                throw new Exception("Operation: " + operation + " is not supported");
        }
        bolt = ((ScheduledWindowedBolt) bolt).withWindow(BaseWindowedBolt.Duration.of(window.toMillis()),
                BaseWindowedBolt.Duration.of(interval.toMillis()));
    }
}
