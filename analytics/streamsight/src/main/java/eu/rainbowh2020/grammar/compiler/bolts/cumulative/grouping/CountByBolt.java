package eu.rainbowh2020.grammar.compiler.bolts.cumulative.grouping;

import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CountByBolt extends ScheduledBolt {
    private String field;
    private String groupByField;
    private Map<String, Integer> groupByValues = new HashMap<>();

    public CountByBolt(String field, String groupByField, String id) {
        super(id);
        this.field = field;
        this.groupByField = groupByField;
    }

    @Override
    public void execute(Tuple tuple) {
        if (tuple.contains(this.field) && tuple.contains(this.groupByField)) {
            addToKey((String) tuple.getValueByField(this.groupByField));
            String result = groupByValues.entrySet().stream().map(e -> e.getKey() + ":" + e.getValue()).collect(Collectors.joining(" "));
            super.setValues(new Values(result));
            super.execute(tuple);
        }
    }

    private void addToKey(String key) {
        if (!groupByValues.containsKey(key)) {
            groupByValues.put(key, 1);
        } else {
            groupByValues.put(key, groupByValues.get(key) + 1);
        }
    }

}
