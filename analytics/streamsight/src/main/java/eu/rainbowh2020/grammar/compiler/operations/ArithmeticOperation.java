package eu.rainbowh2020.grammar.compiler.operations;

import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.AddBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.DivideBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.MultiplyBolt;
import eu.rainbowh2020.grammar.compiler.bolts.arithmetic.SubtractBolt;

public class ArithmeticOperation extends Operation {
    private String operation;
    private String leftId;
    private String rightId;

    public ArithmeticOperation(String leftId, String rightId, String operation) {
        this.name = "A";
        this.operation = operation;
        this.leftId = leftId;
        this.rightId = rightId;
        this.parameters = getOperation();
    }

    @Override
    public void apply() {
        String id = this.id();
        switch (this.operation) {
            case "+":
                bolt = new AddBolt(leftId, rightId, id);
                break;
            case "-":
                bolt = new SubtractBolt(leftId, rightId, id);
                break;
            case "*":
                bolt = new MultiplyBolt(leftId, rightId, id);
                break;
            case "/":
                bolt = new DivideBolt(leftId, rightId, id);
                break;
        }
    }

    private String getOperation() {
        switch (this.operation) {
            case "+":
                return "add";
            case "-":
                return "subtract";
            case "*":
                return "multiply";
            case "/":
                return "divide";
            default:
                throw new IllegalArgumentException("Unexpected value: " + this.operation);
        }
    }
}
