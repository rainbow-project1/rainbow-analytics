package eu.rainbowh2020;

import eu.rainbowh2020.grammar.GrammarTranslator;
import eu.rainbowh2020.grammar.Helpers;
import eu.rainbowh2020.grammar.compiler.bolts.ScheduledBolt;
import eu.rainbowh2020.grammar.compiler.bolts.ScheduledWindowedBolt;
import eu.rainbowh2020.grammar.compiler.operations.Operation;
import eu.rainbowh2020.grammar.compiler.uStreamSightCompiler;
import org.apache.log4j.Logger;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.topology.IComponent;
import org.apache.storm.topology.TopologyBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;

import static java.lang.Float.parseFloat;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class);
    private static final String PYTHON = "python";
    private static final String CSV_FILE_NAME = "output.csv";
    private static final String SCRIPT_FILE_NAME = "Equality.py";
    private static final String MODE = "mode";
    private static final String ENABLED = "enabled";
    private static final String PYTHON_COMMAND = PYTHON + " " + SCRIPT_FILE_NAME + " " + CSV_FILE_NAME + " ";

    public static void main(String[] args) throws Exception {
        if (args.length >= 2 && args[0].contains("syntax-validation")) {
            checkSyntax(args[1]);
            System.exit(0);
        }
        // TODO in the future you can set these parameters through a yaml configuration file
        Config conf = new Config();
        List<String> seeds = Collections.singletonList(System.getenv("NIMBUS_SEEDS"));
        conf.put(Config.NIMBUS_SEEDS, seeds);
        conf.put(Config.NIMBUS_THRIFT_PORT, Integer.parseInt(System.getenv("NIMBUS_THRIFT_PORT")));
        conf.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, 15);
        Integer num_of_workers;
        String num_of_workers_string = System.getenv("NUM_OF_WORKERS");
        try{
            num_of_workers = Integer.parseInt(num_of_workers_string);
        }catch (Exception ex) {
            num_of_workers = 1;
        }
        conf.setNumWorkers(num_of_workers);
        conf.setDebug(false);
        if (args.length != 5 && args.length !=7) { // not local execution or not remote execution
            throw new IllegalArgumentException("Illegal number of arguments: " + args.length);
            // TODO configure that
//            conf.put(MODE, args[1]);
//            conf.setNumWorkers(Integer.parseInt(args[2]));
        }
        conf.put("ustreamsight.logs", false);
        conf.put("ustreamsight.spout.logs", false);

        // Read the insights:
        // We assume insights are named insights.insights
        String insightsFile = args[6];
        GrammarTranslator translator = new GrammarTranslator();
        logger.info("Reading insights from: " + insightsFile);
        translator.parse(Helpers.loadFileStream(insightsFile));

        uStreamSightCompiler compiler = new uStreamSightCompiler();
        compiler.load(translator.getStreamDefinitions(), translator.getInsightDefinitions(), args[1], args[2], args[3], args[4], args[5]);

        if (ENABLED.equals(conf.get(MODE))) {
            ScheduledBolt.MODE = "enabled";
            ScheduledWindowedBolt.MODE = "enabled";
            File csvOutputFile = new File(CSV_FILE_NAME);
            try {
                PrintWriter pw = new PrintWriter(csvOutputFile);
                compiler.getOperations().values().stream()
                        .filter(e -> e.getOutputSource() != null)
                        .map(e -> e.id().concat("\t").concat(e.getOutputSource().id()))
                        .forEach(pw::println);

                Process p = Runtime.getRuntime().exec(PYTHON_COMMAND + args[2]);
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    System.out.println("stdout: " + line);
                }
            } catch (IOException e) {
                System.out.println("Exception in reading output" + e.toString());
            }

            compiler.getOperations().values().stream().filter(e -> e.getOutputSource() != null).forEach(e -> {
                String forwardBoltId = e.getOutputSource().id();
                IComponent component = e.getComponent();
                if (component instanceof ScheduledBolt) {
                    ((ScheduledBolt) component).setFractions(readFractions(forwardBoltId));
                    ((ScheduledBolt) component).setForwardBoltId(forwardBoltId);
                } else if (component instanceof ScheduledWindowedBolt) {
                    ((ScheduledWindowedBolt) component).setFractions(readFractions(forwardBoltId));
                    ((ScheduledWindowedBolt) component).setForwardBoltId(forwardBoltId);
                }
            });
        }
        //System.out.println("------------------------------------------------------");
        // compiler.debugPlan(topology);
        //System.out.println("------------------------------------------------------");

        System.out.println("Operations to be applied:");
        for (Map.Entry<String, Operation> operation : compiler.getOperations().entrySet()) {
            String id = operation.getKey();
            System.out.println("\t" + id);
            operation.getValue().apply();
        }

        TopologyBuilder topologyBuilder = new TopologyBuilder();
        compiler.getOperations().values().forEach(e -> e.declare(topologyBuilder));
        StormTopology topology = topologyBuilder.createTopology();
        // LocalCluster localCluster = new LocalCluster();
        try {
            // args0 is the topology name
            StormSubmitter.submitTopology(args[0], conf, topology);
            // localCluster.submitTopology(args[0], conf, topology);
            Thread.sleep(2000);
        } catch (AlreadyAliveException | AuthorizationException | InvalidTopologyException e) {
            System.out.println(e);
        }
    }

    private static TreeMap<Double, Integer> readFractions(String pathToFractionsFile) {
        final TreeMap<Double, Integer> fractions = new TreeMap<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(pathToFractionsFile + ".txt"));
            final String line = br.readLine();
            int id = 0;
            double sum = 0.0;
            for (String value : line.split(", ")) {
                if (parseFloat(value) != 0) {
                    sum += (int) (parseFloat(value) * 100);
                    fractions.put(sum, id);
                }
                id += 1;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fractions;
    }

    private static String convertToCSV(Operation operation) {
        return operation.id().concat(",").concat(operation.getOutputSource().id());
    }

    private static void checkSyntax(String filename) throws Exception {
        // Read the insights:
        GrammarTranslator translator = new GrammarTranslator();
        logger.info("Reading insights from: " + filename);
        translator.parse(Helpers.loadFileStream(filename));
    }
}
