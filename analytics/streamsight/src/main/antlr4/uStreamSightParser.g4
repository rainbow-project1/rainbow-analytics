parser grammar uStreamSightParser;
@header {
    package eu.rainbowh2020;
}
options{
    tokenVocab=uStreamSightLexer;
}

statements              :  statement (SEMI_COLON statement)* SEMI_COLON? EOF
                        ;

statement               : streamDefinition #streamDefinitionExpr
                        | insightDefinition #insightDefinitionExpr
                        ;

streamDefinition         : streamId COLON STREAM FROM streamProvider
                        ;

streamId                : ID
                        ;

streamProvider          : ID LPAR keyValueParams? RPAR
                        ;

keyValueParams          : keyValuePair ( COMMA keyValuePair)*
                        ;

keyValuePair            : ID EQUAL value
                        ;

value                   : STRING
                        | INTEGER
                        | FLOAT
                        ;

insightDefinition       : insightId EQUAL COMPUTE composition ;//(FROM membership)? ;

insightId               : ID
                        ;

composition             : intervalComposition #interExpr
                        | filteredComposition #filterExpr
                        | groupingComposition #groupExpr
                        | expression #basecaseExpr
                        ;

intervalComposition     : expression EVERY interval;

filteredComposition     : expression WHEN OPERATOR expression;

groupingComposition     : aggregate BY grouping (EVERY interval)?
                        ;

expression              : expression MULOP expression # mulopExpr
                        | expression ADDOP expression # addopExpr
                        | operation #opExpr
                        | FLOAT #DOUBLE
                        ;

operation               : aggregate    #aggregateExpr
                        | metricStream #metricStreamExpr
                        | cumulative #cumulativeExpr
                        ;

metric                  : STRING
;

membership              : LPAR member (COMMA member)* RPAR;

member                  : ID;

aggregate               : windowedFunction LPAR metricStream COMMA window RPAR #windowedAggrExpr
                        ;

cumulative              : function LPAR metricStream RPAR
                        ;

metricStream            : metric (FROM membership)?
;

function                :   ID;

windowedFunction        :   ID;

window                  : timeperiod
                        ;
interval                : timeperiod
                        ;
grouping                : STRING
                        ;
timeperiod              : INTEGER TIME_PERIOD;

