// Generated from /home/zgeorg03/git-projects/Streaming-Analytics/uStreamSight/src/main/antlr4/uStreamSightLexer.g4 by ANTLR 4.8

    package eu.rainbowh2020;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class uStreamSightLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMPUTE=1, STREAM=2, FROM=3, TIME_PERIOD=4, ID=5, STREAM_PROVIDER=6, SEMI_COLON=7, 
		COLON=8, EQUAL=9, LPAR=10, RPAR=11, LBR=12, RBR=13, COMMA=14, STRING=15, 
		INTEGER=16, FLOAT=17, WS=18, BlockComment=19, LineComment=20;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"COMPUTE", "STREAM", "FROM", "TIME_PERIOD", "ID", "STREAM_PROVIDER", 
			"SEMI_COLON", "COLON", "EQUAL", "LPAR", "RPAR", "LBR", "RBR", "COMMA", 
			"STRING", "INTEGER", "FLOAT", "WS", "BlockComment", "LineComment"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, "';'", "':'", "'='", "'('", 
			"')'", "'['", "']'", "','"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMPUTE", "STREAM", "FROM", "TIME_PERIOD", "ID", "STREAM_PROVIDER", 
			"SEMI_COLON", "COLON", "EQUAL", "LPAR", "RPAR", "LBR", "RBR", "COMMA", 
			"STRING", "INTEGER", "FLOAT", "WS", "BlockComment", "LineComment"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public uStreamSightLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "uStreamSightLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 18:
			BlockComment_action((RuleContext)_localctx, actionIndex);
			break;
		case 19:
			LineComment_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void BlockComment_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			 // System.out.println("BC> " + getText());
			        skip();
			break;
		}
	}
	private void LineComment_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			 //System.out.println("LC> " + getText());
			        skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\26\u00cf\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\3\2\5\2:\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\5\3H\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4R\n\4\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5r\n\5\3\6\6\6u\n\6\r\6\16"+
		"\6v\3\6\7\6z\n\6\f\6\16\6}\13\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3"+
		"\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\7\20\u0093\n\20\f\20"+
		"\16\20\u0096\13\20\3\20\3\20\3\21\5\21\u009b\n\21\3\21\6\21\u009e\n\21"+
		"\r\21\16\21\u009f\3\22\5\22\u00a3\n\22\3\22\6\22\u00a6\n\22\r\22\16\22"+
		"\u00a7\3\22\3\22\6\22\u00ac\n\22\r\22\16\22\u00ad\5\22\u00b0\n\22\3\23"+
		"\6\23\u00b3\n\23\r\23\16\23\u00b4\3\23\3\23\3\24\3\24\3\24\3\24\7\24\u00bd"+
		"\n\24\f\24\16\24\u00c0\13\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\7\25\u00c9"+
		"\n\25\f\25\16\25\u00cc\13\25\3\25\3\25\4\u0094\u00be\2\26\3\3\5\4\7\5"+
		"\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23"+
		"%\24\'\25)\26\3\2\b\5\2C\\aac|\6\2\62;C\\aac|\3\2\62;\4\2--//\5\2\13\f"+
		"\16\17\"\"\4\2\f\f\17\17\2\u00e4\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2"+
		"\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2"+
		"\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2"+
		"\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2"+
		"\39\3\2\2\2\5G\3\2\2\2\7Q\3\2\2\2\tq\3\2\2\2\13t\3\2\2\2\r~\3\2\2\2\17"+
		"\u0080\3\2\2\2\21\u0082\3\2\2\2\23\u0084\3\2\2\2\25\u0086\3\2\2\2\27\u0088"+
		"\3\2\2\2\31\u008a\3\2\2\2\33\u008c\3\2\2\2\35\u008e\3\2\2\2\37\u0090\3"+
		"\2\2\2!\u009a\3\2\2\2#\u00a2\3\2\2\2%\u00b2\3\2\2\2\'\u00b8\3\2\2\2)\u00c6"+
		"\3\2\2\2+,\7E\2\2,-\7Q\2\2-.\7O\2\2./\7R\2\2/\60\7W\2\2\60\61\7V\2\2\61"+
		":\7G\2\2\62\63\7e\2\2\63\64\7q\2\2\64\65\7o\2\2\65\66\7r\2\2\66\67\7w"+
		"\2\2\678\7v\2\28:\7g\2\29+\3\2\2\29\62\3\2\2\2:\4\3\2\2\2;<\7U\2\2<=\7"+
		"V\2\2=>\7T\2\2>?\7G\2\2?@\7C\2\2@H\7O\2\2AB\7u\2\2BC\7v\2\2CD\7t\2\2D"+
		"E\7g\2\2EF\7c\2\2FH\7o\2\2G;\3\2\2\2GA\3\2\2\2H\6\3\2\2\2IJ\7H\2\2JK\7"+
		"T\2\2KL\7Q\2\2LR\7O\2\2MN\7h\2\2NO\7t\2\2OP\7q\2\2PR\7o\2\2QI\3\2\2\2"+
		"QM\3\2\2\2R\b\3\2\2\2ST\7O\2\2TU\7K\2\2UV\7N\2\2VW\7N\2\2WX\7K\2\2Xr\7"+
		"U\2\2YZ\7o\2\2Zr\7u\2\2[\\\7U\2\2\\]\7G\2\2]^\7E\2\2^_\7Q\2\2_`\7P\2\2"+
		"`a\7F\2\2ar\7U\2\2br\7u\2\2cd\7O\2\2de\7K\2\2ef\7P\2\2fg\7W\2\2gh\7V\2"+
		"\2hi\7G\2\2ir\7U\2\2jr\7o\2\2kl\7J\2\2lm\7Q\2\2mn\7W\2\2no\7T\2\2or\7"+
		"U\2\2pr\7j\2\2qS\3\2\2\2qY\3\2\2\2q[\3\2\2\2qb\3\2\2\2qc\3\2\2\2qj\3\2"+
		"\2\2qk\3\2\2\2qp\3\2\2\2r\n\3\2\2\2su\t\2\2\2ts\3\2\2\2uv\3\2\2\2vt\3"+
		"\2\2\2vw\3\2\2\2w{\3\2\2\2xz\t\3\2\2yx\3\2\2\2z}\3\2\2\2{y\3\2\2\2{|\3"+
		"\2\2\2|\f\3\2\2\2}{\3\2\2\2~\177\5\13\6\2\177\16\3\2\2\2\u0080\u0081\7"+
		"=\2\2\u0081\20\3\2\2\2\u0082\u0083\7<\2\2\u0083\22\3\2\2\2\u0084\u0085"+
		"\7?\2\2\u0085\24\3\2\2\2\u0086\u0087\7*\2\2\u0087\26\3\2\2\2\u0088\u0089"+
		"\7+\2\2\u0089\30\3\2\2\2\u008a\u008b\7]\2\2\u008b\32\3\2\2\2\u008c\u008d"+
		"\7_\2\2\u008d\34\3\2\2\2\u008e\u008f\7.\2\2\u008f\36\3\2\2\2\u0090\u0094"+
		"\7$\2\2\u0091\u0093\13\2\2\2\u0092\u0091\3\2\2\2\u0093\u0096\3\2\2\2\u0094"+
		"\u0095\3\2\2\2\u0094\u0092\3\2\2\2\u0095\u0097\3\2\2\2\u0096\u0094\3\2"+
		"\2\2\u0097\u0098\7$\2\2\u0098 \3\2\2\2\u0099\u009b\7/\2\2\u009a\u0099"+
		"\3\2\2\2\u009a\u009b\3\2\2\2\u009b\u009d\3\2\2\2\u009c\u009e\t\4\2\2\u009d"+
		"\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u009d\3\2\2\2\u009f\u00a0\3\2"+
		"\2\2\u00a0\"\3\2\2\2\u00a1\u00a3\t\5\2\2\u00a2\u00a1\3\2\2\2\u00a2\u00a3"+
		"\3\2\2\2\u00a3\u00a5\3\2\2\2\u00a4\u00a6\t\4\2\2\u00a5\u00a4\3\2\2\2\u00a6"+
		"\u00a7\3\2\2\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00af\3\2"+
		"\2\2\u00a9\u00ab\7\60\2\2\u00aa\u00ac\t\4\2\2\u00ab\u00aa\3\2\2\2\u00ac"+
		"\u00ad\3\2\2\2\u00ad\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00b0\3\2"+
		"\2\2\u00af\u00a9\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0$\3\2\2\2\u00b1\u00b3"+
		"\t\6\2\2\u00b2\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b4"+
		"\u00b5\3\2\2\2\u00b5\u00b6\3\2\2\2\u00b6\u00b7\b\23\2\2\u00b7&\3\2\2\2"+
		"\u00b8\u00b9\7\61\2\2\u00b9\u00ba\7,\2\2\u00ba\u00be\3\2\2\2\u00bb\u00bd"+
		"\13\2\2\2\u00bc\u00bb\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bf\3\2\2\2"+
		"\u00be\u00bc\3\2\2\2\u00bf\u00c1\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1\u00c2"+
		"\7,\2\2\u00c2\u00c3\7\61\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c5\b\24\3\2"+
		"\u00c5(\3\2\2\2\u00c6\u00ca\7%\2\2\u00c7\u00c9\n\7\2\2\u00c8\u00c7\3\2"+
		"\2\2\u00c9\u00cc\3\2\2\2\u00ca\u00c8\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb"+
		"\u00cd\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cd\u00ce\b\25\4\2\u00ce*\3\2\2\2"+
		"\23\29GQqv{\u0094\u009a\u009f\u00a2\u00a7\u00ad\u00af\u00b4\u00be\u00ca"+
		"\5\b\2\2\3\24\2\3\25\3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}