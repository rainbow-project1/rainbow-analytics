// Generated from /home/zgeorg03/git-projects/Streaming-Analytics/uStreamSight/src/main/antlr4/uStreamSightParser.g4 by ANTLR 4.8

    package eu.rainbowh2020;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link uStreamSightParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface uStreamSightParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(uStreamSightParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Visit a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(uStreamSightParser.ValueContext ctx);
}