// Generated from /home/zgeorg03/git-projects/Streaming-Analytics/uStreamSight/src/main/antlr4/uStreamSightParser.g4 by ANTLR 4.8

    package eu.rainbowh2020;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link uStreamSightParser}.
 */
public interface uStreamSightParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 */
	void enterStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#statements}.
	 * @param ctx the parse tree
	 */
	void exitStatements(uStreamSightParser.StatementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(uStreamSightParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(uStreamSightParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 */
	void enterStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#streamDefinition}.
	 * @param ctx the parse tree
	 */
	void exitStreamDefinition(uStreamSightParser.StreamDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 */
	void enterStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#streamProvider}.
	 * @param ctx the parse tree
	 */
	void exitStreamProvider(uStreamSightParser.StreamProviderContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 */
	void enterKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#keyValueParams}.
	 * @param ctx the parse tree
	 */
	void exitKeyValueParams(uStreamSightParser.KeyValueParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 */
	void enterKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#keyValuePair}.
	 * @param ctx the parse tree
	 */
	void exitKeyValuePair(uStreamSightParser.KeyValuePairContext ctx);
	/**
	 * Enter a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(uStreamSightParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link uStreamSightParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(uStreamSightParser.ValueContext ctx);
}