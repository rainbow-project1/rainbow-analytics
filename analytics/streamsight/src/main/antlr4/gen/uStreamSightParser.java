// Generated from /home/zgeorg03/git-projects/Streaming-Analytics/uStreamSight/src/main/antlr4/uStreamSightParser.g4 by ANTLR 4.8

    package eu.rainbowh2020;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class uStreamSightParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		COMPUTE=1, STREAM=2, FROM=3, ID=4, STREAM_PROVIDER=5, SEMI_COLON=6, COLON=7, 
		EQUAL=8, LPAR=9, RPAR=10, COMMA=11, STRING=12, WS=13, NUMBER=14;
	public static final int
		RULE_statements = 0, RULE_statement = 1, RULE_streamDefinition = 2, RULE_streamProvider = 3, 
		RULE_keyValueParams = 4, RULE_keyValuePair = 5, RULE_value = 6;
	private static String[] makeRuleNames() {
		return new String[] {
			"statements", "statement", "streamDefinition", "streamProvider", "keyValueParams", 
			"keyValuePair", "value"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, "';'", "':'", "'='", "'('", "')'", 
			"','"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "COMPUTE", "STREAM", "FROM", "ID", "STREAM_PROVIDER", "SEMI_COLON", 
			"COLON", "EQUAL", "LPAR", "RPAR", "COMMA", "STRING", "WS", "NUMBER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "uStreamSightParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public uStreamSightParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StatementsContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode EOF() { return getToken(uStreamSightParser.EOF, 0); }
		public List<TerminalNode> SEMI_COLON() { return getTokens(uStreamSightParser.SEMI_COLON); }
		public TerminalNode SEMI_COLON(int i) {
			return getToken(uStreamSightParser.SEMI_COLON, i);
		}
		public StatementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStatements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStatements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStatements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementsContext statements() throws RecognitionException {
		StatementsContext _localctx = new StatementsContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statements);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(14);
			statement();
			setState(19);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(15);
					match(SEMI_COLON);
					setState(16);
					statement();
					}
					} 
				}
				setState(21);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			setState(23);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI_COLON) {
				{
				setState(22);
				match(SEMI_COLON);
				}
			}

			setState(25);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StreamDefinitionContext streamDefinition() {
			return getRuleContext(StreamDefinitionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27);
			streamDefinition();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StreamDefinitionContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public TerminalNode COLON() { return getToken(uStreamSightParser.COLON, 0); }
		public TerminalNode STREAM() { return getToken(uStreamSightParser.STREAM, 0); }
		public TerminalNode FROM() { return getToken(uStreamSightParser.FROM, 0); }
		public StreamProviderContext streamProvider() {
			return getRuleContext(StreamProviderContext.class,0);
		}
		public StreamDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_streamDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StreamDefinitionContext streamDefinition() throws RecognitionException {
		StreamDefinitionContext _localctx = new StreamDefinitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_streamDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(29);
			match(ID);
			setState(30);
			match(COLON);
			setState(31);
			match(STREAM);
			setState(32);
			match(FROM);
			setState(33);
			streamProvider();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StreamProviderContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public TerminalNode LPAR() { return getToken(uStreamSightParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(uStreamSightParser.RPAR, 0); }
		public KeyValueParamsContext keyValueParams() {
			return getRuleContext(KeyValueParamsContext.class,0);
		}
		public StreamProviderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_streamProvider; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterStreamProvider(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitStreamProvider(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitStreamProvider(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StreamProviderContext streamProvider() throws RecognitionException {
		StreamProviderContext _localctx = new StreamProviderContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_streamProvider);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(35);
			match(ID);
			setState(36);
			match(LPAR);
			setState(38);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ID) {
				{
				setState(37);
				keyValueParams();
				}
			}

			setState(40);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyValueParamsContext extends ParserRuleContext {
		public List<KeyValuePairContext> keyValuePair() {
			return getRuleContexts(KeyValuePairContext.class);
		}
		public KeyValuePairContext keyValuePair(int i) {
			return getRuleContext(KeyValuePairContext.class,i);
		}
		public TerminalNode COMMA() { return getToken(uStreamSightParser.COMMA, 0); }
		public KeyValueParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyValueParams; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterKeyValueParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitKeyValueParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitKeyValueParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyValueParamsContext keyValueParams() throws RecognitionException {
		KeyValueParamsContext _localctx = new KeyValueParamsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_keyValueParams);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42);
			keyValuePair();
			{
			setState(43);
			match(COMMA);
			setState(44);
			keyValuePair();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyValuePairContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(uStreamSightParser.ID, 0); }
		public TerminalNode EQUAL() { return getToken(uStreamSightParser.EQUAL, 0); }
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public KeyValuePairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyValuePair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterKeyValuePair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitKeyValuePair(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitKeyValuePair(this);
			else return visitor.visitChildren(this);
		}
	}

	public final KeyValuePairContext keyValuePair() throws RecognitionException {
		KeyValuePairContext _localctx = new KeyValuePairContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_keyValuePair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(ID);
			setState(47);
			match(EQUAL);
			setState(48);
			value();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(uStreamSightParser.STRING, 0); }
		public TerminalNode NUMBER() { return getToken(uStreamSightParser.NUMBER, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof uStreamSightParserListener ) ((uStreamSightParserListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof uStreamSightParserVisitor ) return ((uStreamSightParserVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			_la = _input.LA(1);
			if ( !(_la==STRING || _la==NUMBER) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20\67\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\7\2\24\n\2\f"+
		"\2\16\2\27\13\2\3\2\5\2\32\n\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\5\3\5\3\5\5\5)\n\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b"+
		"\3\b\2\2\t\2\4\6\b\n\f\16\2\3\4\2\16\16\20\20\2\62\2\20\3\2\2\2\4\35\3"+
		"\2\2\2\6\37\3\2\2\2\b%\3\2\2\2\n,\3\2\2\2\f\60\3\2\2\2\16\64\3\2\2\2\20"+
		"\25\5\4\3\2\21\22\7\b\2\2\22\24\5\4\3\2\23\21\3\2\2\2\24\27\3\2\2\2\25"+
		"\23\3\2\2\2\25\26\3\2\2\2\26\31\3\2\2\2\27\25\3\2\2\2\30\32\7\b\2\2\31"+
		"\30\3\2\2\2\31\32\3\2\2\2\32\33\3\2\2\2\33\34\7\2\2\3\34\3\3\2\2\2\35"+
		"\36\5\6\4\2\36\5\3\2\2\2\37 \7\6\2\2 !\7\t\2\2!\"\7\4\2\2\"#\7\5\2\2#"+
		"$\5\b\5\2$\7\3\2\2\2%&\7\6\2\2&(\7\13\2\2\')\5\n\6\2(\'\3\2\2\2()\3\2"+
		"\2\2)*\3\2\2\2*+\7\f\2\2+\t\3\2\2\2,-\5\f\7\2-.\7\r\2\2./\5\f\7\2/\13"+
		"\3\2\2\2\60\61\7\6\2\2\61\62\7\n\2\2\62\63\5\16\b\2\63\r\3\2\2\2\64\65"+
		"\t\2\2\2\65\17\3\2\2\2\5\25\31(";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}