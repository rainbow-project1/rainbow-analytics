lexer grammar uStreamSightLexer;
@header {
    package eu.rainbowh2020;
}

COMPUTE: 'COMPUTE' | 'compute';
STREAM: 'STREAM' | 'stream';
FROM: 'FROM' | 'from';
BY: 'BY' | 'by';
EVERY: 'EVERY' | 'every';
WHEN: 'WHEN' | 'when';

TIME_PERIOD             : 'MILLIS' |'ms'| 'SECONDS' | 's' | 'MINUTES' | 'm' | 'HOURS' | 'h';

ID : [a-zA-Z_]+[a-zA-Z_0-9]*;
STREAM_PROVIDER:    ID;

SEMI_COLON: ';' ;
COLON: ':' ;
EQUAL: '=';
LPAR : '(';
RPAR : ')';
LBR: '[';
RBR: ']';

COMMA: ',';

ADDOP                   : ('+' | '-');

MULOP                   : ('*' | '/' | '%');

OPERATOR                : ('>' | '<' | '==' | '!=' | '>=' | '<=');

STRING: '"' .*? '"' ;
INTEGER                 :  '-'? [0-9]+ ;
FLOAT                   : ('+' | '-') ?  [0-9] + ('.' [0-9] +)? ;

// Whitespace
WS  :  [ \t\r\n\u000C]+ -> skip ;

BlockComment
    : '/*' .*? '*/'
      { // System.out.println("BC> " + getText());
        skip();}
    ;


LineComment
    :   '#' ~('\n'|'\r')*
        { //System.out.println("LC> " + getText());
        skip();}
    ;

