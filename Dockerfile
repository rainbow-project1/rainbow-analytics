FROM storm:2.1.0

# install python
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y python3 python3-pip
RUN python3 --version

# install maven
# RUN wget https://mirrors.estointernet.in/apache/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.tar.gz
# RUN tar -xvf apache-maven-3.8.1-bin.tar.gz
# RUN mv apache-maven-3.8.1 /usr/share/
# ENV PATH=/usr/share/apache-maven-3.8.1/bin:$PATH


RUN mkdir /src
RUN mkdir /analytics
ADD ./rainbow-analytic-enabler/rest-api/src /src/
ADD ./analytics /analytics/
WORKDIR /src
RUN pip3 install -r /src/requirements.txt

ENTRYPOINT ["python3", "/src/main.py"]
