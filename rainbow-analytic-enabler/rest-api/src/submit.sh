#!/bin/sh
MAIN_CLASS="eu.rainbowh2020.Main"
DEPLOYMENT_NAME=$1
TOPOLOGY="streamsight-topology-${DEPLOYMENT_NAME}"
STREAMSIGHT_FILE="${DEPLOYMENT_NAME}.insights"
STREAMSIGHT_JAR=rainbow-analytics.jar

#cp $(pwd)/$STREAMSIGHT_FILE /insights.insights

storm jar $(pwd)/$STREAMSIGHT_JAR $MAIN_CLASS $TOPOLOGY $OUTPUT_MODE $PROVIDER_HOSTS $PROVIDER_PORT $CONSUMER_HOST $CONSUMER_PORT $(pwd)/$STREAMSIGHT_FILE
