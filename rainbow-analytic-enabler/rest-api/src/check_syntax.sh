#!/bin/sh
DEPLOYMENT_ID=$1
STREAMSIGHT_JAR=rainbow-analytics-syntax-validation.jar
java -jar $(pwd)/$STREAMSIGHT_JAR syntax-validation $(pwd)/$DEPLOYMENT_ID.insights
