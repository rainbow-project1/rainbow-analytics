class WrongFormatException(Exception):
    """
    Raised when the user passes queries that do not have a valid syntax.
    """
    pass


class EmptyQueryException(Exception):
    """
    Raised when the user tries to make a post / put api call but does not include in their body the queries.
    """
    pass

class UiNameAndPortMissing(Exception):
    """
    Raised when no ui service name and port was declared
    """
    pass
