""" This is the Rainbow's REST API, its purpose is to receive and save / change
StreamSight queries """
import json, subprocess, requests, os
from flask_cors import CORS
from flask import Flask, request
from exceptions import WrongFormatException, EmptyQueryException, UiNameAndPortMissing

# Create the application instance
app = Flask(__name__)
CORS(app)
app.config["DEBUG"] = True
core_path = "/api/insights"
path = core_path + "/<deployment_id>"
###########################################################

if not "ui_name" in os.environ or not "ui_port" in os.environ:
    raise UiNameAndPortMissing
ui_name = os.environ["ui_name"]
ui_port = os.environ["ui_port"]

"""
This	method	checks the syntax validation of the queries, if they are not valid it will return an exception,
otherwise it will create a file (if it does not exist) and then it'll write the queries in it. 
"""


def write_insights_to_file(body, deployment_id):
    if "Queries" not in body:
        raise EmptyQueryException

    file = open(deployment_id + ".insights", 'w+')
    for query in body["Queries"]:
        file.write(query + "\n")

    file.close()

    # check syntax validation of queries
    bash_command = "sh check_syntax.sh " + deployment_id
    process = subprocess.Popen(bash_command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output_str = output.decode("utf-8")
    if "Unsupported Statement" in output_str:
        os.remove(deployment_id + ".insights")
        return WrongFormatException()

    return 0


"""
This method submits the topology to storm
"""


def submit_job(deployment_id):
    os.system("sh submit.sh " + deployment_id)


"""
This is default method to guide the users that they need to add a deployment-id in their queries
"""


@app.route(core_path, methods=['GET', 'POST', 'PUT', 'DELETE'])
def route_request():
    return json.dumps({'status': 'failure', 'message': "Please include '/<deployment_id>' in the request"}), 400, {
        'ContentType': 'application/json'}


"""
This method	returns	the	current	status of an analytics job along with the	
submitted queries as a reference based on the given	deployment-id.
"""


@app.route(path, methods=['GET'])
def get_query(deployment_id):
    storm_ui_get_topology = requests.get('http://' + ui_name + ':' + ui_port + '/api/v1/topology/summary')

    # if storm does not respond
    if storm_ui_get_topology.status_code != 200:
        return json.dumps({'status': 'failure', 'message': 'storm is not currently responding'}), 500, {
            'ContentType': 'application/json'}

    deployment_name = 'streamsight-topology-' + deployment_id

    # if user requested to get all active jobs
    if deployment_id.lower() == "all":
        return storm_ui_get_topology.json()

    # if job was never submitted or killed
    if not os.path.exists(deployment_id + ".insights"):
        return json.dumps({'status': 'failure',
                           'message': 'The given analytic job with name ' + deployment_id + ' does not exist or has been killed.'}), 400, {
                   'ContentType': 'application/json'}

    # if job is not available in storm's api
    if not deployment_name in storm_ui_get_topology.text:
        file = open(deployment_id + ".insights", "r")
        queries = file.read()
        return json.dumps({'status': 'failure',
                           'message': 'The given job was not submitted successfully, please check your submitted queries if they are semantically correct.',
                           'Queries': [queries]}), 400, {
                   'ContentType': 'application/json'}

    storm_ui_get_topology = json.loads(storm_ui_get_topology.text)
    # successful scenario
    for topology in storm_ui_get_topology["topologies"]:
        if topology["name"] == deployment_name:
            file = open(deployment_id + ".insights", "r")
            queries = file.read()
            return json.dumps({'status': topology["status"], 'Queries': [queries]}), 200, {
                'ContentType': 'application/json'}


"""
With this method, entities submit or update queries.
It also validates their syntax and returns success if the job has been 
submitted, otherwise it returns an error message.
"""


@app.route(path, methods=['POST', 'PUT'])
def post_query(deployment_id):
    # TODO: to check if the body includes scheduler configuration
    body = request.get_json()

    # if job already exists, kill it
    if os.path.exists(deployment_id + ".insights"):
        delete_query(deployment_id)

    # validate syntax and write to file
    result = write_insights_to_file(body, deployment_id)
    if isinstance(result, WrongFormatException):
        return json.dumps({'status': 'failure', "message": "One of the given queries have wrong syntax"}), 400, {
            'ContentType': 'application/json'}

    # start the topology and submit job
    submit_job(deployment_id)
    return json.dumps({'status': 'success'}), 200, {'ContentType': 'application/json'}


"""
This method deletes submitted analytics job by referring to the job with its deployment-id.
"""


@app.route(path, methods=['DELETE'])
def delete_query(deployment_id):

    # if user requested to delete all active jobs
    if deployment_id.lower() == "all":
        os.system("kill -9 $(ps -aef | grep 'sh submit.sh' | grep -v grep | awk '{print $2}')")
        os.system("rm /src/*.insights")
    else:
        # if process exists, kill it
        os.system("kill -9 $(ps -aef | grep 'sh submit.sh " + deployment_id + "' | grep -v grep | awk '{print $2}')")

    storm_ui_get_topology = requests.get('http://' + ui_name + ':' + ui_port + '/api/v1/topology/summary')

    # if storm does not respond
    if storm_ui_get_topology.status_code != 200:
        return json.dumps({'status': 'failure', 'message': 'storm is not currently responding'}), 500, {
            'ContentType': 'application/json'}

    deployment_name = 'streamsight-topology-' + deployment_id

    # if job was never submitted or already killed
    if not os.path.exists(deployment_id + ".insights") and deployment_id.lower() != "all":
        return json.dumps({'status': 'failure',
                           'message': 'The given analytic job with name ' + deployment_id + ' does not exist or has already been killed.'}), 400, {
                   'ContentType': 'application/json'}

    storm_ui_get_topology = json.loads(storm_ui_get_topology.text)
    # successful scenario - found topology
    for topology in storm_ui_get_topology["topologies"]:
        if topology["name"] == deployment_name or deployment_id.lower() == "all":
            storm_ui_kill_topology = requests.post(
                'http://' + ui_name + ':' + ui_port + '/api/v1/topology/' + topology['id'] + '/kill/0')
            if deployment_id.lower() != "all":
                os.remove(deployment_id + ".insights")
                if storm_ui_kill_topology.status_code == 200:
                    return json.dumps({"status": "success"}), 200, {'ContentType': 'application/json'}
                else:
                    return json.dumps({'status': 'failure', 'message': 'storm is not currently responding'}), 500, {
                        'ContentType': 'application/json'}
    if deployment_id.lower() == "all":
        return json.dumps({"status": "success", "message": "All jobs have been killed successfully"}), 200, {'ContentType': 'application/json'}
    return json.dumps({'status': 'failure',
            'message': 'The given analytic job with name ' + deployment_id + ' does not exist or has already been killed.'}), 400, {
           'ContentType': 'application/json'}


"""
This method will return the name of the scheduler that the user chose for a specific analytic job.
"""


@app.route(path, methods=['GET'])
def get_scheduler(deployment_id):
    # TODO
    pass


app.run('0.0.0.0')
